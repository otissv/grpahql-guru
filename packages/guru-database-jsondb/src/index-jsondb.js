import _connect from './connect-jsondb'
import jsonDBQuery from './query-jsondb'
import jsonDBMutation from './mutation-jsondb'

export const connect = _connect
export const JsonDBQuery = jsonDBQuery
export const JsonDBMutation = jsonDBMutation
