import autobind from 'class-autobind'
import uuid from 'uuid/v4'
import { errors } from 'guru-server-utils'

export default class JsonDBMutation {
  constructor() {
    autobind(this)
  }

  create(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    const TABLE = this.table || this.collection
    const id = uuid()

    return new Promise((resolve, reject) => {
      const data = jsondb
        .get(TABLE)
        .push({ ...args, id })
        .write()
        .filter(item => item.id === id)

      resolve(data[0])
    }).catch(error =>
      reject(
        errors.resolverMutation({
          error,
          message: `${TABLE.toLowerCase()}Create failed.`,
        })
      )
    )
  }

  remove(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    const id = args.id
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      const data = jsondb
        .get(TABLE)
        .remove({ id: args.id })
        .write()
        .filter(item => item.id === id)

      resolve(data[0])
    }).catch(error =>
      reject(
        errors.resolverMutation({
          error,
          message: `${TABLE.toLowerCase()}Remove failed.`,
        })
      )
    )
  }

  update(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    const id = args.id
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      const data = jsondb
        .get(TABLE)
        .find({ id: args.id })
        .assign({ ...args })
        .write()

      resolve(data)
    }).catch(error =>
      reject(
        errors.resolverMutation({
          error,
          message: `${TABLE.toLowerCase()}Update failed.`,
        })
      )
    )
  }

  // createMany
  // deleteMany
  // removeById
  // updateMany
}
