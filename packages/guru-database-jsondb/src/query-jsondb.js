import autobind from 'class-autobind'
import { errors } from 'guru-server-utils'

export default class JsonDBQuery {
  constructor() {
    autobind(this)
  }

  resolve() {
    return Array.isArray(...arguments[0])
      ? this.findManyById(...arguments[0])
      : this.findById(...arguments[0])
  }

  findMany(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      const data = jsondb.get(TABLE).value()
      resolve(data)
    }).catch(error =>
      Promise.reject(
        errors.resolverQuery({
          error,
          message: `${TABLE.toLowerCase()}FindMany failed.`,
        })
      )
    )
  }

  findById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      const data = jsondb
        .get(TABLE)
        .find({ id: obj.id })
        .value()

      resolve(data)
    }).catch(error =>
      Promise.reject(
        errors.resolverQuery({
          error,
          message: `${TABLE.toLowerCase()}FindById failed.`,
        })
      )
    )
  }

  findManyById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'jsondb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const jsondb = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection
    const ids = obj.id.map(id => ({ id }))

    return new Promise((resolve, reject) => {
      const table = jsondb.get(TABLE).value()

      const data = ids.reduce((previous, item) => {
        const insert = table.filter(record => record.id === item.id)

        return [...previous, ...insert]
      }, [])

      resolve(data)
    }).catch(error =>
      Promise.reject(
        errors.resolverQuery({
          error,
          message: `${TABLE.toLowerCase()}FindManyById failed.`,
        })
      )
    )
  }
}
