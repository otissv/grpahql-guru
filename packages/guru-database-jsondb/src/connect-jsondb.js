import low from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'

export default function connect({ defaults = null, storage }) {
  // path to json database file

  const dbPath =
    storage.substr(0, 1) === '/'
      ? `${process.cwd()}${storage.substr(0, 1)}`
      : `${process.cwd()}/${storage}`

  // initialize database;
  const adapter = new FileSync(dbPath)
  const db = low(adapter)

  if (!db) noDB()

  // get database state
  const state = db.getState()

  // if database is empty add default tables
  if (Object.keys(state).length === 0) {
    db.defaults(defaults).write()
  }

  return db
}
