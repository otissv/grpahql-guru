import { utils } from 'guru-database-mongodb'
import { errors } from 'guru-server-utils'
const { queryObjectId } = utils

export default class MongodbCollection {
  resolve() {}

  findMany(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collections()
          .then(collections => {
            resolve(collections.map(collection => ({ name: collection.s.name })))
          })
          .catch(error => reject(errors.resolverQuery({ error })))
      })
    })
  }

  findManyDocuments(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(collection)
          .find({}, projection)
          .toArray((error, docs) => {
            if (error) {
              reject(errors.resolverQuery({ error }))
            } else {
              resolve({
                name: collection,
                data: docs,
              })
            }
          })
      })
    })
  }

  findDocumentById(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, id } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(collection)
          .find(queryObjectId(id), projection)
          .toArray((error, docs) => {
            if (error) {
              reject(errors.resolverQuery({ error }))
            } else {
              resolve({
                name: collection,
                data: docs[0],
              })
            }
          })
      })
    })
  }
}
