import mongodb from 'mongodb'
import { utils } from 'guru-database-mongodb'
import { errors } from 'guru-server-utils'
const { ObjectId } = mongodb
const { queryObjectId } = utils

export default class MongodbCollection {
  create(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { name, options } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .createCollection(name, options)
          .then(result => {
            resolve({ name: result.s.name })
          })
          .catch(error =>
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbCollectionCreate  failed.',
              })
            )
          )
      })
    })
  }

  drop(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { name, options } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(args.name)
          .drop()
          .then(result => {
            return result
          })
          .catch(error =>
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbCollectionDrop  failed.',
              })
            )
          )
      })
    })
  }

  removeDocuments(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { name, options, id } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(name)
          .remove({ id })
          .then(result => {
            return result
          })
          .catch(error =>
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbCollectionRemoveDocuments  failed.',
              })
            )
          )
      })
    })
  }

  rename(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { name, dropTarget, target } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(name)
          .rename(target, dropTarget)
          .then(result => {
            resolve({ name: result.s.name })
          })
          .catch(error =>
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbCollectionRename  failed.',
              })
            )
          )
      })
    })
  }

  createDocument(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, data } = args

    return new Promise((resolve, reject) =>
      mongodb.then(db => {
        return db
          .collection(collection)
          .insert(data)
          .then(response => {
            resolve({
              name: collection,
              data: {
                ...data,
                id: response.insertedIds[0],
              },
              RESULTS: {
                result: 'ok',
                created: response.result.n,
              },
            })
          })
          .catch(error =>
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbCollectionCreateDocument  failed.',
              })
            )
          )
      })
    )
  }

  removeDocument(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, id } = args

    return mongodb.then(db => {
      return db
        .collection(collection)
        .remove({ id: ObjectId(id) })
        .then(response => {
          return {
            id,
            result: response.result.n > 0 ? 'ok' : 'failed',
            removed: response.result.n,
          }
        })
        .catch(error =>
          errors.resolverMutation({
            error,
            message: 'mongodbCollectionRemoveDocument  failed.',
          })
        )
    })
  }

  updateDocument(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, id, data } = args

    return mongodb.then(db => {
      return db
        .collection(collection)
        .update({ id: ObjectId(id) }, { $set: data })
        .then(response => {
          const error = response.result.n === 0 ? { message: 'No data supplied.' } : null

          return {
            id,

            RESULTS: {
              result: response.result.n > 0 ? 'ok' : 'failed',
              updated: response.result.n,
              error,
            },
          }
        })
      reject(
        errors.resolverMutation({
          error,
          message: 'mongodbCollectionUpdateDocument  failed.',
        })
      )
    })
  }
}
