import { errors } from 'guru-server-utils'

export default class MongodbIndex {
  create(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, index, options } = args

    const indexObj = {
      [index.key]: parseInt(index.value, 10),
    }

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(collection, options)
          .createIndex(indexObj)
          .then(result => {
            const resultSplit = result.split('_')

            resolve({
              name: resultSplit[0],
              value: resultSplit[1],
            })
          })
          .catch(error => {
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbIndexCreate  failed.',
              })
            )
          })
      })
    })
  }

  drop(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection, index } = args
    const indexStr = `${index.key}_${index.value}`

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(args.collection)
          .dropIndex(indexStr)
          .then(result => {
            resolve(result)
          })
          .catch(error => {
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbIndexDrop  failed.',
              })
            )
          })
      })
    })
  }

  dropAll(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    const { collection } = args

    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(args.collection)
          .dropIndexes()
          .then(result => resolve(result))
          .catch(error => {
            reject(
              errors.resolverMutation({
                error,
                message: 'mongodbIndexDropAll  failed.',
              })
            )
          })
      })
    })
  }
}
