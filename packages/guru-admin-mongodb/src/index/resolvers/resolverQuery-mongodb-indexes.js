import { errors } from 'guru-server-utils'

export default class MongodbIndex {
  resolve({ query }) {
    return query.index
  }

  findByCollection(query, args, context, info) {
    const { databases } = context
    const dbName = this.database || 'mongodb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const mongodb = databases[dbName]
    return mongodb.then(db => {
      return new Promise((resolve, reject) => {
        return db
          .collection(args.collection)
          .indexes()
          .then(indexes => {
            resolve(
              indexes.map(index => ({
                key: JSON.stringify(index.key),
                name: index.name,
              }))
            )
          })
        reject(errors.resolverQuery({ error }))
      })
    })
  }
}
