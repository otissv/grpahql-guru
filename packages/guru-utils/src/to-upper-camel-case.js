const camel = require('to-camel-case')

module.exports = function upperCamelCase(str) {
  return `${str[0].toUpperCase()}${camel(str.substr(1, str.length - 1))}`
}
