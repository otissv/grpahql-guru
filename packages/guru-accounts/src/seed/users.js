import roles from './roles'
import times from 'lodash/fp/times'

const randomIntToFrom = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
const randomDate = () => {}
const randomRole = () => roles.data[randomIntToFrom(0, roles.data.length - 1)]
const randomPovider = () => {
  const providers = [
    'auth0',
    'auth0api',
    'azuer',
    'bitbucket',
    'facebook',
    'github',
    'gitlab',
    'google',
    'instagram',
    'jwt',
    'twitter',
    'windowslive',
  ]
  return providers[randomIntToFrom(0, providers.length - 1)]
}

export default {
  database: 'mongodb',
  data: times(i => ({
    _id: i,
    lastLoggedInWith: 'facebook',
    lastLogin: randomDate(),
    password: `pass${i}`,
    roles: randomRole(),
    token: `token_${i}`,
    username: `user_${i}`,
    auth0Id: randomPovider(),
    auth0apiId: randomPovider(),
    azuerId: randomPovider(),
    bitbucketId: randomPovider(),
    facebookId: randomPovider(),
    githubId: randomPovider(),
    gitlabId: randomPovider(),
    googleId: randomPovider(),
    instagramId: randomPovider(),
    jwtId: randomPovider(),
    twitterId: randomPovider(),
    windowsliveId: randomPovider(),
  }))(100),
}
