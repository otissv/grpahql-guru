const roles = [
  {
    name: 'admin',
    description: 'Admin role',
  },
  {
    name: 'author',
    description: 'Author role',
  },
  {
    name: 'commenter',
    description: 'Commenter role',
  },
  {
    name: 'editor',
    description: 'Editor role',
  },
  {
    name: 'guest',
    description: 'Guest role',
  },
  {
    name: 'manager',
    description: 'Manger role',
  },
]

export default {
  database: 'mongodb',
  data: roles.map((role, i) => ({ ...role, _id: i })),
}
