import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({ collection: 'roles' })
export default class Role {}
