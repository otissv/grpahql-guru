import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'roles' })
export default class Role {}
