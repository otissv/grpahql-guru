import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({ collection: 'permissions' })
export default class Permission {}
