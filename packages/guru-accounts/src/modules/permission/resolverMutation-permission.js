import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'permissions' })
export default class Permission {}
