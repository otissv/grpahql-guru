import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'userProfiles' })
export default class UserProfile {}
