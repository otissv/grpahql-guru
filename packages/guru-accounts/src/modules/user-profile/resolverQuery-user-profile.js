import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({ collection: 'userProfiles' })
export default class UserProfile {}
