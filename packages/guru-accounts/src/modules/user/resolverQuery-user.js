import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({ collection: 'users' })
export default class User {}
