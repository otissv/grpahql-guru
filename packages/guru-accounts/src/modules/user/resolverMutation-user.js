import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'users' })
export default class User {}
