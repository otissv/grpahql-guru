import passport from 'passport'
import { Strategy as AzureOAuthStrategy } from 'passport-azure-oauth'
import { findOrCreateOauthUser } from './utils-auth'

const azureOptions = {
  clientID: process.env.AZURE_CLIENT_ID,
  clientSecret: process.env.AZURE_CLIENT_SECRET,
  redirectURL: process.env.AZURE_REDIRECT_URL,
  tenantId: process.env.AZURE_TENANT_ID,
  resource: process.env.AZURE_RESOURCE,
  proxy: {
    host: process.env.AZURE_PROXY_HOST,
    port: process.env.AZURE_PROXY_PORT,
    protocol: process.env.AZURE_PROXY_PROTOCOL,
  },
}

const hasConfig = Object.values(azureOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new AzureOAuthStrategy(azureOptions, async function(
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'azure',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }

  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/azure/callback',
      passport.authenticate('azure', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.azure,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/azure', passport.authenticate('azure'))
  }
  return app
}
