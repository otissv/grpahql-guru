import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import { Strategy as FacebookStrategy } from 'passport-facebook'

const facebookOptions = {
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL,
}

const hasConfig = Object.values(facebookOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new FacebookStrategy(facebookOptions, async function(
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'facebook',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/facebook/callback',
      passport.authenticate('facebook', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.facebook,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/facebook', passport.authenticate('facebook'))
  }
  return app
}
