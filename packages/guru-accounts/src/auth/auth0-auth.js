import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import Auth0Strategy from 'passport-auth0'

const auth0Options = {
  domain: process.env.AUTH0_DOMAIN,
  clientID: process.env.AUTH0_CLIENT_ID,
  clientSecret: process.env.AUTH0_CLIENT_SECRET,
  callbackURL: process.env.AUTH0_CALLBACK_URL,
}

const hasConfig = Object.values(auth0Options).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new Auth0Strategy(auth0Options, async function(
        accessToken,
        refreshToken,
        extraParams,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'auth0',
            args: { auth0Id: { $elemMatch: { $eq: profile.id } } },
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/auth0/callback',
      passport.authenticate('auth0', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.auth0,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/auth0', passport.authenticate('auth0'))
  }

  return app
}
