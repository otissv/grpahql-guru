import passport from 'passport'
import { Strategy as TwitterStrategy } from 'passport-twitter'
import { findOrCreateOauthUser } from './utils-auth'

const twitterOptions = {
  consumerKey: process.env.TWITTER_CONSUMER_KEY,
  consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
  callbackUrl: process.env.TWITTER_CALLBACK_URL,
}

const hasConfig = Object.values(twitterOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new TwitterStrategy(twitterOptions, async function(
        req,
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'twitter',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, redirect, options = {}, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/twitter/callback',
      passport.authenticate('twitter', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.twitter,
      }),
      (request, response) => {
        return redirect ? response.redirect(301, redirect) : response.json(user)
      }
    )
    app.use('/auth/twitter', passport.authenticate('twitter'))
  }
  return app
}
