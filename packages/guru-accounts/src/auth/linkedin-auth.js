import passport from 'passport'
import { Strategy as LinkedInStrategy } from 'passport-linkedin'
import { findOrCreateOauthUser } from './utils-auth'

const linkedinOptions = {
  consumerKey: process.env.LINKEDIN_CONSUMER_KEY,
  consumerSecret: process.env.LINKEDIN_CONSUMER_SECRET,
  callbackUrl: process.env.LINKEDIN_CALLBACK_URL,
}

const hasConfig = Object.values(linkedinOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new LinkedInStrategy(linkedinOptions, async function(
        req,
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'linkedin',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, redirect, options = {}, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/linkedin/callback',
      passport.authenticate('linkedin', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.linkedin,
      }),
      (request, response) => {
        return redirect ? response.redirect(301, redirect) : response.json(user)
      }
    )
    app.use('/auth/linkedin', passport.authenticate('linkedin'))
  }
  return app
}
