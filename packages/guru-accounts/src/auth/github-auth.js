import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import { Strategy as GitHubStrategy } from 'passport-github2'

const githubOptions = {
  clientID: process.env.GITHUB_CLIENT_ID,
  clientSecret: process.env.GITHUB_CLIENT_SECRET,
  callbackURL: process.env.GITHUB_CALLBACK_URL,
}

const hasConfig = Object.values(githubOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new GitHubStrategy(githubOptions, async function(accessToken, refreshToken, profile, done) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'github',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/github/callback',
      passport.authenticate('github', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.github,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )

    app.use('/auth/github', passport.authenticate('github'))
  }
  return app
}
