import init from './init-auth'
import logout from './logout-auth'
import { middleware as auth0Middleware, routes as auth0Routes } from './auth0-auth'
import { middleware as azureMiddleware, routes as azureRoutes } from './azure-auth'
import { middleware as bearerMiddleware, routes as bearerRoutes } from './bearer-auth'
import { middleware as bitbucketMiddleware, routes as bitbucketRoutes } from './bitbucket-auth'
import { middleware as facebookMiddleware, routes as facebookRoutes } from './facebook-auth'
import { middleware as githubMiddleware, routes as githubRoutes } from './github-auth'
import { middleware as gitlabMiddleware, routes as gitlabRoutes } from './gitlab-auth'
import { middleware as googleMiddleware, routes as googleRoutes } from './google-auth'
import { middleware as instagramMiddleware, routes as instagramRoutes } from './instagram-auth'
import { middleware as linkedinMiddleware, routes as linkedinRoutes } from './linkedin-auth'
import { middleware as localMiddleware, routes as localRoutes } from './local-auth'
import { middleware as twitterMiddleware, routes as twitterRoutes } from './twitter-auth'
import {
  middleware as windowsliveMiddleware,
  routes as windowsliveRoutes,
} from './windowslive-auth'

export const middleware = {
  auth0: auth0Middleware,
  azure: azureMiddleware,
  bearer: bearerMiddleware,
  bitbucket: bitbucketMiddleware,
  facebook: facebookMiddleware,
  github: githubMiddleware,
  gitlab: gitlabMiddleware,
  google: googleMiddleware,
  instagram: instagramMiddleware,
  linkedin: linkedinMiddleware,
  local: localMiddleware,
  twitter: twitterMiddleware,
  windowslive: windowsliveMiddleware,
  init,
}

export const routes = {
  auth0: auth0Routes,
  azure: azureRoutes,
  bearer: bearerRoutes,
  bitbucket: bitbucketRoutes,
  facebook: facebookRoutes,
  github: githubRoutes,
  gitlab: gitlabRoutes,
  google: googleRoutes,
  instagram: instagramRoutes,
  linkedin: linkedinRoutes,
  local: localRoutes,
  twitter: twitterRoutes,
  windowslive: windowsliveRoutes,
  logout,
}
