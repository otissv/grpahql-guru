import jwt from 'jsonwebtoken'
import util from 'util'
import fs from 'fs'
import env from 'get-env'
import bcrypt from 'bcryptjs'
import { cleanObj, errors } from 'guru-server-utils'

const dev = env !== 'prod' || env !== 'production'
const promisify = util.promisify
const CERT = `${process.cwd()}/.certs/combined.pem`
const ALGORITHM = 'RS256'

const readFileAsync = promisify(fs.readFile)
const signAsync = promisify(jwt.sign)
const verifyAsync = promisify(jwt.verify)

function getCert() {
  return readFileAsync(CERT, 'utf8')
}

const SECRET = process.env.GURU_APP_SECRET

export async function findUser(args, context, cb) {
  try {
    const User = new context.connectors.user()
    const user = await User.findOne(null, { input: args }, context)
    // TODO: add last provider used to login
    if (cb && user) return cb(user)

    if (user) {
      return user
    }
  } catch (error) {
    console.error(error)
    return error
  }
}

export async function createUser({ provider, context, cb, args }) {
  try {
    const User = new context.connectors.user()
    let user

    if (args.password) {
      args.password = await generateHash(args.password)
    }

    user = await User.findOne(null, { input: { username: args.username } }, context).then(
      async result => {
        try {
          if (result) {
            const isValid = await isValidateHash(args.password, result.password)

            return isValid
              ? {
                  id: result.id,
                  token: result.token,
                  roles: result.roles,
                  user: args.username,
                  name: provider.name,
                }
              : null
          } else {
          }
        } catch (error) {
          console.error(error)
          return error
        }
      }
    )

    if (!user) {
      const input = {
        ...args,
        provider,
        id: 'app',
        token: await jwtSignAsync({ payload: args }),
      }

      user = await User.create(null, { input }, context).then(result => {
        if (!result) return

        return {
          id: result.id,
          token: result.token,
          roles: result.roles,
          username: args.username,
          name: provider.name,
        }
      })
    }

    if (cb && user) return cb(user)

    if (user) {
      const { id, token, username } = user
      return { id, token, username }
    }
  } catch (error) {
    console.error(error)
    return error
  }
}

export async function findOrCreateOauthUser({ profile, provider, context, cb, args }) {
  const { connectors } = context
  const id = `${provider}Id`
  const _args = args || { [id]: profile.id }
  const UserProfile = new connectors.userProfile()
  const User = new connectors.user()

  try {
    let user = await User.findOne(null, { input: _args }, context)
      .then(result => {
        if (!result) return

        if (cb) {
          return cb(result)
        }

        if (profile.id === result[id])
          return {
            id: result.id,
            token: result.token,
            roles: result.roles,
            username: profile.username,
            name: profile.name,
          }
      })
      .then(async doc => {
        // Update user details
        let data
        if (doc) {
          const input = {
            id: doc.id,
            lastLoggedInWith: provider,
            token: doc.token,
          }

          return User.update(null, { input }, context).then(result => {
            if (result.error) return Promise.reject(error)

            return {
              id: doc.id,
              token: doc.token,
              roles: doc.roles,
              username: profile.username,
              name: profile.name,
            }
          })
        } else {
          const input = {
            [id]: profile.id,
            id: 'app',
            lastLoggedInWith: provider,
            token: await jwtSignAsync({ payload: SECRET }),
          }
          return User.create(null, { input }, context)
        }
      })
      .then(user => {
        if (!user.RESULTS)
          return {
            id: user.id,
            token: user.token,
            roles: user.roles,
            username: profile.username,
            name: profile.name,
          }
        // Create user profile
        const email =
          Array.isArray(profile.email) || !profile.email ? profile.email : [profile.email]

        const input = cleanObj({
          [id]: profile.id,
          displayName: profile.displayName,
          email,
          gender: profile.gender,
          id: 'app',
          name: profile.name,
          token: user.token,
          user: user.id.toString(),
          username: profile.username,
        })

        return UserProfile.create(null, { input }, context).then(result => {
          if (result.error) return Promise.reject(error)

          return {
            id: user.id,
            token: user.token,
            roles: result.roles,
            username: profile.username,
            name: profile.name,
          }
        })
      })

    if (user) {
      const { id, token, username, name } = user
      return { id, token, username, name }
    }
  } catch (error) {
    if (error) console.error(error)
    return { error: 'FAILED_LOGIN', message: 'Failed to log in user.' }
  }
}

export async function findAndModifyUser({ id, token }, context) {
  const User = new context.connectors.user()
  try {
    const input = {
      query: { id, token },
      update: { token: '' },
      sort: { id: 1 },
      unset: true,
    }
    const user = await User.findAndModify(null, { input }, context)

    if (user) {
      return {
        id,
        token,
        roles: user.roles,
        username: user.username,
      }
    }
  } catch (error) {
    console.error(error)
    return error
  }
}

export async function generateHash() {
  try {
    return await getCert().then(cert => bcrypt.hashSync(cert, bcrypt.genSaltSync(10), null))
  } catch (error) {
    console.error(error)
  }
}

export async function isValidateHash(str, hash) {
  try {
    return await getCert().then(cert => bcrypt.compareSync(cert, hash))
  } catch (error) {
    console.error(error)
  }
}

export async function jwtSignAsync({ payload, options = {}, failureRedirect }) {
  try {
    const opts = {
      algorithm: ALGORITHM,
      ...options,
    }

    return await getCert().then(cert => signAsync(payload, cert, opts))
  } catch (error) {
    console.error(error)
  }
}

export async function jwtVerifyAsync({ token, options }) {
  try {
    const opts = {
      algorithm: ALGORITHM,
      ...options,
    }

    return await getCert().then(cert => verifyAsync(token, cert, opts))
  } catch (error) {
    return error
    console.error(error)
  }
}
