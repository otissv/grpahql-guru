import passport from 'passport'
import { Strategy as GoogleStrategy } from 'passport-google-oauth20'
import { findOrCreateOauthUser } from './utils-auth'

const googleOptions = {
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK_URL,
  scope: process.env.GOOGLE_SCOPE,
  passReqToCallback: true,
}

const hasConfig = Object.values(googleOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new GoogleStrategy(googleOptions, async function(
        req,
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'google',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, redirect, options = {}, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/google/callback',
      passport.authenticate('google', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.google,
      }),
      (request, response) => (redirect ? response.redirect(301, redirect) : response.json(user))
    )
    app.use('/auth/google', passport.authenticate('google'))
  }
  return app
}
