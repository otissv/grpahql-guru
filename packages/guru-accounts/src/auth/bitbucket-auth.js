import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import { Strategy as BitbucketStrategy } from 'passport-bitbucket-oauth2'

const bitbucketOptions = {
  clientID: process.env.BITBUCKET_CLIENT_ID,
  clientSecret: process.env.BITBUCKET_CLIENT_SECRET,
  callbackURL: process.env.BITBUCKET_CALLBACK_URL,
}

const hasConfig = Object.values(bitbucketOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new BitbucketStrategy(bitbucketOptions, async function(
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'bitbucket',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }

  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/bitbucket/callback',
      passport.authenticate('bitbucket', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.bitbucket,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )

    app.use('/auth/bitbucket', passport.authenticate('bitbucket'))
  }
  return app
}
