import passport from 'passport'

export default function authInit({ app }) {
  passport.serializeUser(function(user, done) {
    done(null, user)
  })

  passport.deserializeUser(function(user, done) {
    done(null, user)
  })

  app.use(passport.initialize())
  app.use(passport.session())

  app.get('/auth/unauthorized', (request, response) =>
    response.status('401').json({ error: 'Unauthorized' })
  )

  return app
}
