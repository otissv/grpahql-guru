import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import { Strategy as GitLabStrategy } from 'passport-gitlab2'

const gitlabOptions = {
  clientID: process.env.GITLAB_CLIENT_ID,
  clientSecret: process.env.GITLAB_CLIENT_SECRET,
  callbackURL: process.env.GITLAB_CALLBACK_URL,
}

const hasConfig = Object.values(gitlabOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new GitLabStrategy(gitlabOptions, async function(accessToken, refreshToken, profile, done) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'gitlab',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/gitlab/callback',
      passport.authenticate('gitlab', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.gitlab,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/gitlab', passport.authenticate('gitlab'))
  }
  return app
}
