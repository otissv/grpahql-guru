import passport from 'passport'
import { findUser } from './utils-auth'
import { Strategy as BearerStrategy } from 'passport-http-bearer'

export function middleware({ app, context }) {
  const locals = context.locals

  passport.use(
    new BearerStrategy(async function(token, done) {
      if (!token) return

      try {
        const user = await findUser({ token }, context)

        done(null, user)
      } catch (error) {
        console.error(error)
        return error
      }
    })
  )

  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  app.use(
    '/auth/bearer/callback',
    passport.authenticate('bearer', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options.bearer,
    })
  )
  app.use(
    '/auth/bearer',
    passport.authenticate('bearer'),
    (request, response) =>
      redirect ? response.redirect(301, redirect) : response.json(request.user)
  )

  return app
}
