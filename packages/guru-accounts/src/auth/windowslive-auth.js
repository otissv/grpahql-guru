import passport from 'passport'
import { Strategy as WindowsLiveStrategy } from 'passport-windowslive'
import { findOrCreateOauthUser } from './utils-auth'

const windowsliveOptions = {
  clientID: process.env.WINDOWSIVE_CLIENT_ID,
  clientSecret: process.env.WINDOWSIVE_CLIENT_SECRET,
  callbackURL: process.env.WINDOWSIVE_CALLBACK_URL,
}

const hasConfig = Object.values(windowsliveOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new WindowsLiveStrategy(windowsliveOptions, async function(
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'windowslive',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/windowslive/callback',
      passport.authenticate('windowslive', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.instagram,
      }),
      (request, response) => (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/windowslive', passport.authenticate('windowslive'))
  }
  return app
}
