import passport from 'passport'
import { createUser } from './utils-auth'
import { Strategy as LocalStrategy } from 'passport-local'

export function middleware({ app, context }) {
  passport.use(
    new LocalStrategy(async function(username, password, done) {
      if (!username || !password) return

      function cb(user) {
        return password === user.password ? user : null
      }

      try {
        const user = await createUser({
          provider: 'local',
          args: { username, password, roles },
          context,
        })

        done(null, user)
      } catch (error) {
        console.error(error)
        return error
      }
    })
  )

  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  app.use(
    '/auth/local/callback',
    passport.authenticate('local', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options.local,
    })
  )

  app.use('/auth/local', passport.authenticate('local'), (request, response) => {
    req.logout()
    redirect ? response.redirect(301, redirect) : response.json(request.user)
  })

  return app
}
