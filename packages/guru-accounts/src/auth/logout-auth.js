import { parseQueryString } from 'guru-server-utils'
import { findAndModifyUser } from './utils-auth'

export default function logout({ app, context, redirect, failedRedirect }) {
  app.use('/auth/logout', async (request, response) => {
    try {
      const body =
        request.body || request.url.indexOf('?') >= 0 ? await parseQueryString(request.url) : null

      if (!body) {
        return response.json({
          result: 'failed',
          data: 'User logged out failed.',
        })
      }

      const user = await findAndModifyUser(body, context)

      if (request.error) {
        const message = 'User logged out failed.'

        if (failedRedirect) {
          request.message = message
          return response.redirect(301, failedRedirect)
        } else {
          return response.json({
            result: 'failed',
            data: message,
          })
        }
      } else {
        request.logout()
        const message = 'User logged out was successful.'
        if (redirect) {
          request.message = message
          return response.redirect(301, redirect)
        } else {
          return response.json({
            result: 'ok',
            data: message,
          })
        }
      }
      return app
    } catch (error) {
      console.error(error)
      return error
    }
  })
}
