import passport from 'passport'
import { findOrCreateOauthUser } from './utils-auth'
import { Strategy as InstagramStrategy } from 'passport-instagram'

const instagramOptions = {
  clientID: process.env.INSTAGRAM_CLIENT_ID,
  clientSecret: process.env.INSTAGRAM_CLIENT_SECRET,
  callbackURL: process.env.INSTAGRAM_CALLBACK_URL,
}

const hasConfig = Object.values(instagramOptions).every(val => Boolean(val))

export function middleware({ app, context }) {
  if (hasConfig) {
    passport.use(
      new InstagramStrategy(instagramOptions, async function(
        accessToken,
        refreshToken,
        profile,
        done
      ) {
        try {
          const user = await findOrCreateOauthUser({
            profile,
            context,
            provider: 'instagram',
          })

          done(null, user)
        } catch (error) {
          console.error(error)
          return error
        }
      })
    )
  }
  return app
}

export function routes({ app, context, options = {}, redirect, failureRedirect }) {
  if (hasConfig) {
    app.use(
      '/auth/instagram/callback',
      passport.authenticate('instagram', {
        failureRedirect: failureRedirect || '/auth/unauthorized',
        ...options.instagram,
      }),
      (request, response) =>
        redirect ? response.redirect(301, redirect) : response.json(request.user)
    )
    app.use('/auth/instagram', passport.authenticate('instagram'))
  }
  return app
}
