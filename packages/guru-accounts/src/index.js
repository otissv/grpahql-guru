import init from './auth/init-auth'
import logout from './auth/logout-auth'
import { middleware as auth0Middleware, routes as auth0Routes } from './auth/auth0-auth'
import { middleware as azureMiddleware, routes as azureRoutes } from './auth/azure-auth'
import { middleware as bearerMiddleware, routes as bearerRoutes } from './auth/bearer-auth'
import { middleware as bitbucketMiddleware, routes as bitbucketRoutes } from './auth/bitbucket-auth'
import { middleware as facebookMiddleware, routes as facebookRoutes } from './auth/facebook-auth'
import { middleware as githubMiddleware, routes as githubRoutes } from './auth/github-auth'
import { middleware as gitlabMiddleware, routes as gitlabRoutes } from './auth/gitlab-auth'
import { middleware as googleMiddleware, routes as googleRoutes } from './auth/google-auth'
import { middleware as instagramMiddleware, routes as instagramRoutes } from './auth/instagram-auth'
import { middleware as linkedinMiddleware, routes as linkedinRoutes } from './auth/linkedin-auth'
import { middleware as localMiddleware, routes as localRoutes } from './auth/local-auth'
import { middleware as twitterMiddleware, routes as twitterRoutes } from './auth/twitter-auth'
import {
  middleware as windowsliveMiddleware,
  routes as windowsliveRoutes,
} from './auth/windowslive-auth'

export const middleware = {
  auth0: auth0Middleware,
  azure: azureMiddleware,
  bearer: bearerMiddleware,
  bitbucket: bitbucketMiddleware,
  facebook: facebookMiddleware,
  github: githubMiddleware,
  gitlab: gitlabMiddleware,
  google: googleMiddleware,
  instagram: instagramMiddleware,
  linkedin: linkedinMiddleware,
  local: localMiddleware,
  twitter: twitterMiddleware,
  windowslive: windowsliveMiddleware,
  init,
}

export const routes = {
  auth0: auth0Routes,
  azure: azureRoutes,
  bearer: bearerRoutes,
  bitbucket: bitbucketRoutes,
  facebook: facebookRoutes,
  github: githubRoutes,
  gitlab: gitlabRoutes,
  google: googleRoutes,
  instagram: instagramRoutes,
  linkedin: linkedinRoutes,
  local: localRoutes,
  twitter: twitterRoutes,
  windowslive: windowsliveRoutes,
  logout,
}

export async function authorize({ context, info }) {
  try {
    const { connectors: { permission }, user } = context

    if (user) {
      const operation = info.fieldName

      // Find permission's roles
      const permissions = await new permission().findMany(null, null, {
        ...context,
        options: {
          projection: { id: 0 },
        },
      })

      if (!permissions || permissions.length === 0) return true

      // Get operation  permission's roles
      const operationsPermission = permissions
        .filter(p => p.operation === operation)
        .reduce((p, c) => [...p, ...c.roles.map(r => r.role.toUpperCase())], [])

      // Check operation against user roles
      const filtered = user.roles.filter(r => operationsPermission.indexOf(r) >= 0)

      if (filtered.length > 0) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
    // Check operation against user roles
  } catch (error) {
    console.error(error)
    return error
  }
}
