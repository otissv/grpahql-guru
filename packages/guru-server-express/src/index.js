/*
* Application Server
*/
import cluster from 'cluster'
import fs from 'fs'
import https from 'https'
import http from 'http'
import express from 'express'
import shared from '../../guru-server-shared'
import routes from './routes'
import middleware from './middleware/index-middleware'
import os from 'os'

const main = shared.main
const dev = process.env.NODE_ENV !== 'prod' && process.env.NODE_ENV !== 'production'

const PORT = process.env.PORT || '9200'
const DOMAIN = process.env.DOMAIN || 'localhost'

function serverStartedMessage() {
  const host = dev ? `http://${DOMAIN}:${PORT}` : domain

  // Network Address
  let externalAddress
  const networkInterfaces = os.networkInterfaces()

  if (networkInterfaces.en0 === true) {
    externalAddress = networkInterfaces.en0[0].address
  } else if (networkInterfaces.wlp4s0) {
    externalAddress = networkInterfaces.wlp4s0[0].address
  }

  const env = dev ? 'development' : 'production'

  return process.stdout.write(`
Guru Express server in ${env} mode.
  - IDE address           = ${host}
  - GraphQL endpoint:     = ${host}/graphql
${externalAddress ? '  - External IDE address  = ' + 'http://' + externalAddress + ':' + PORT : ''}
${
    externalAddress
      ? '  - External IDE endpoint = ' + 'http://' + externalAddress + ':' + PORT + '/graphql'
      : ''
  }

`)
}

export default (async function server(opts = {}) {
  try {
    const app = express()
    const numCPUs = os.cpus().length
    const options = { ...opts }

    // Remove non apollo options;
    delete options.port

    // Load server
    await main({ app, routes, middleware, options })

    //Start servers
    if (!dev && cluster.isMaster) {
      serverStartedMessage()

      // Fork workers.
      for (let i = 0; i < numCPUs; i++) {
        cluster.fork()
      }

      cluster.on('exit', (worker, code, signal) => {
        console.error(`worker ${worker.process.pid} died`)
      })
    } else {
      debugger
      app.listen(PORT, serverStartedMessage())
    }
  } catch (error) {
    console.error(error)
  }
})
