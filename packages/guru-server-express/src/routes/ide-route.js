/*
* GraphQL rourte
*/

'use strict'

import { graphqlExpress } from 'graphql-server-express'
import { makeExecutableSchema } from 'graphql-tools'
import { parse } from 'graphql'
import shared from '../../../guru-server-shared'

const { schema, resolvers } = shared.ide

export default function ideRoute({ app, context }) {
  const logger = { log: e => console.error(e) }

  app
    .route('/ide/schema')
    .get((request, response) => response.json(parse(schema)))

  app.use(
    '/ide',
    graphqlExpress(request => {
      return {
        schema: makeExecutableSchema({
          typeDefs: schema,
          resolvers,
          logger,
        }),
        context: {
          ...context,
          request,
          debug: true,
        },
      }
    })
  )
}
