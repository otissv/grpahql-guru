/*
* GraphQL rourte
*/

'use strict'

import { graphqlExpress, graphiqlExpress } from 'graphql-server-express'
import { makeExecutableSchema } from 'graphql-tools'
import { parse } from 'graphql'
import Cache from './cache-route'
import fs from 'fs'
import { mergeStrings } from 'gql-merge'
import chalk from 'chalk'

export default function graphqlRoute({
  app,
  context,
  schema,
  resolvers,
  options,
}) {
  const logger = { log: e => console.error(e) }

  function parsePersistedQuery({ request, response, next, dir, whitelist }) {
    if (request.body.persisted) {
      const query = JSON.parse(request.body.query)

      const variables = request.body.variables || '{}'
      const body = query.map(item => {
        if (item.id) {
          const file = `${dir}${item.id}-query.json`
          if (fs.existsSync(file)) {
            const queryDoc = fs.readFileSync(file, 'utf8')
            const itemVariables = item.variables ? item.variables : {}
            const vars = {
              ...JSON.parse(variables),
              ...itemVariables,
            }

            return {
              query: JSON.parse(queryDoc).query,
              operationName: item.operationName || null,
              variables: JSON.stringify(vars),
            }
          } else {
            process.stdout.write(
              chalk.red('Error: Persisted query file not found\n')
            )

            return response.status(400).json({
              errors: 'Invalid request',
            })
          }
        } else {
          if (whitelist) {
            process.stdout.write(
              chalk.red('Error: The request is not a persisted query\n')
            )

            return response.status(400).json({
              errors: 'Invalid request',
            })
          } else {
            return item
          }
        }
      })

      request.body = body

      next()
    } else {
      if (whitelist) {
        process.stdout.write(
          chalk.red('The request is not a persisted query\n')
        )

        response.status('400').json({
          errors: 'Invalid request',
        })
      } else {
        next()
      }
    }
  }

  // function validateQueryOperation (request, response, next) {
  //   const queryAst = parse(request.body.query);

  //   queryAst.definitions.forEach(definition => {
  //     const operation = definition.operation;
  //     if (operation === 'mutation' || operation === 'query') {
  //       definition.selectionSet.selections.forEach(selection => {
  //         const result = schema.definition.operations[
  //           capitalize(operation)
  //         ].filter(item => item.name === selection.name.value);

  //         if (result.length === 0) {
  //
  //           response.status('400').json({
  //             errors: 'Invalid request'
  //           });
  //         } else {
  //           next();
  //         }
  //       });
  //     } else {
  //       next();
  //     }
  //   });
  // }

  // app.use('/graphql', validateQueryOperation);

  app.use(
    '/graphql',
    (request, response, next) => {
      parsePersistedQuery({
        request,
        response,
        next,
        dir: `${process.cwd()}/src/queries/`,
        whitelist: false,
      })
    },
    graphqlExpress(request => {
      return {
        schema: makeExecutableSchema({
          typeDefs: schema.ast,
          resolvers,
          logger,
        }),
        context: {
          ...context,
          cache: new Cache(),
          debug: options.debug,
          request,
        },
        ...options,
      }
    })
  )

  app.use(
    '/graphiql',
    graphiqlExpress({
      endpointURL: '/graphql',
    })
  )
}
