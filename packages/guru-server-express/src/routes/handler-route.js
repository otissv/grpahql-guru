/*
* Schema routes
*/
'use strict'
const routes = require(`${process.cwd()}/renderer/routes`)

export default function handlerRoute({ app, handle, context, render }) {
  routes.forEach(route => {
    app.route(route.href).get((req, res) => {
      handle({ ...req, url: route.page }, res)
    })
  })

  app.route('*').get((req, res) => {
    handle(req, res)
  })
}
