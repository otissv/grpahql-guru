/*
* Schema routes
*/
'use strict'

export default function schemaRoute({ app, schema }) {
  app.route('/schema').get((request, response) => {
    return response.json({ schema })
  })

  Object.keys(schema).forEach(key => {
    app.route(`/schema/${key}`).get((request, response) => {
      return response.json({ ast: schema[key] })
    })
  })
}
