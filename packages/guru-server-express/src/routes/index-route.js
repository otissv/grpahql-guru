import graphqlRoute from './graphql-route'
import ideRoute from './ide-route'
import schemaRoute from './schema-route'
import handlerRoute from './handler-route'

export default function coreRoutes(params) {
  graphqlRoute(params)
  ideRoute(params)
  schemaRoute(params)
  handlerRoute(params)
}
