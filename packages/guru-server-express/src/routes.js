/*
* Application routes
*/

import coreRoutes from './routes/index-route'

function routes(params) {
  let _params = { ...params }
  delete _params.routes
  params.routes(_params)
  coreRoutes(params)
}

export default routes
