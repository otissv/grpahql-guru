/*
* Application middleware
 */

import logger from './logger-middleware'
import body from './body-middleware'
import staticFiles from './staticFiles-middleware'
import session from './session-middleware'
import security from './security-middleware'

export default function middleware({ middlewareLoader }) {
  const tmpArgs = { ...arguments[0] }
  delete tmpArgs.middlewareLoader

  logger(tmpArgs)
  body(tmpArgs)
  session(tmpArgs)
  staticFiles(tmpArgs)
  security(tmpArgs)
  middlewareLoader(tmpArgs)
}
