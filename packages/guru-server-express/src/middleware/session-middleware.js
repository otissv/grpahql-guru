/*
*Application session
*/

import cookieParser from 'cookie-parser'
import csrf from 'csurf'
import expressSession from 'express-session'

export default function sessions({ app }) {
  const secret = process.env.SECRET

  app.use(
    expressSession({
      saveUninitialized: true, // saved new sessions
      resave: false, // do not automatically write to the session store
      // store: sessionStore,
      secret,
      cookie: { httpOnly: true, maxAge: 2419200000 }, // configure when sessions expires
    })
  )

  if (app.get('env') === 'production') {
    app.set('trust proxy', 1) // trust first proxy
    expressSession.cookie.secure = true // serve secure cookies
  }

  app.use(cookieParser(secret))
  // CSRF
  // app.use(
  //   csrf({
  //     cookie: {
  //       key: '_csrf',
  //       path: '/'
  //     }
  //   })
  // );

  // app.use((req, res, next) => {
  //   const token = req.csrfToken();

  //   res.cookie('XSRF-TOKEN', token);
  //   next();
  // });
}
