/*
* Application security
*/

import helmet from 'helmet'
import cors from 'cors'

export default function security({ app }) {
  app.disable('x-powered-by')

  // add localhost to white list
  const appWhitelist = app.locals.whitelist ? app.locals.whitelist : []
  const localHosts = Array.apply(null, { length: 9999 }).map(
    Number.call,
    Number => `http://localhost:${Number}`
  )

  let whitelist = [...appWhitelist, ...localHosts]

  const corsOptions = {
    origin: function(origin, callback) {
      var originIsWhitelisted = whitelist.indexOf(origin) !== -1
      callback(null, originIsWhitelisted)
    },
  }

  app.use(cors(corsOptions))

  // pre-flight
  app.options('*', cors(corsOptions))

  // Content Security Policy

  app.use(
    helmet.contentSecurityPolicy({
      directives: {
        defaultSrc: ["'self'"],
        scriptSrc: [
          "'self'",
          "'unsafe-inline'",
          '*.google-analytics.com',
          'http://unpkg.com',
          'http://cdn.jsdelivr.net',
        ],
        styleSrc: ["'self'", "'unsafe-inline'", 'fonts.googleapis.com', 'http://unpkg.com'],
        imgSrc: ["'self'", '*.google-analytics.com', 'data:'],
        connectSrc: ["'self'"],
        fontSrc: ["'self'", 'data:', 'googleapis.com', 'fonts.gstatic.com'],
        objectSrc: ["'self'"],
        mediaSrc: ["'self'"],
        frameSrc: ["'self'"],
      },
    })
  )

  app.use(helmet.xssFilter())
  app.use(helmet.frameguard())
  app.use(
    helmet.hsts({
      maxAge: 7776000000,
      includeSubdomains: true,
    })
  )
  app.use(helmet.noSniff())
  app.use(helmet.ieNoOpen())
  app.use(helmet.hidePoweredBy())
  app.use(helmet.dnsPrefetchControl())
  app.use(helmet.referrerPolicy({ policy: 'same-origin' }))
  // app.use(expectCt({ maxAge: 123 }));
  // app.use(require('express-enforces-ssl'));
  // app.use(helmet.hpkp({
  //   maxAge: ninetyDaysInMilliseconds,
  //   sha256s: ['AbCdEf123=', 'ZyXwVu456='],
  //   includeSubdomains: true,         // optional
  //   reportUri: 'http://example.com'  // optional
  //   reportOnly: false,               // optional
  //
  //   // Set the header based on a condition.
  //   // This is optional.
  //   setIf: function (req, res) {
  //     return req.secure
  //   }
  // }))
}
