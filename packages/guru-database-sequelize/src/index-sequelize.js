import Sequelize from 'sequelize'
import autobind from 'class-autobind'
import { errors } from 'guru-server-utils'

function Model({ db, fields, table }) {
  const types = {
    array: Sequelize.ARRAY(Sequelize.TEXT),
    boolean: Sequelize.BOOLEAN,
    number: Sequelize.FLOAT,
    string: Sequelize.STRING,
  }

  const Model = db.define(
    table,
    Object.keys(fields).reduce((previous, field) => {
      const current = fields[field]

      return {
        ...previous,
        [field]: {
          type: types[current.type],
          allowNull: !current.required,
        },
      }
    })
  )

  return Promise.resolve(Model)
}

export function connect({ username, database, password, port, dialect, host, pool, storage }) {
  const db = new Sequelize(database, username, password, {
    host,
    dialect,
    pool,
    storage,
  })

  db
    .authenticate()
    .then(() => {
      process.stdout.write('Connection has been established successfully.')
    })
    .catch(error => {
      console.error('Unable to connect to the database:', error)

      return Promise.reject(
        databaseConnectionError({
          error,
          message: 'Unable to connect to the database:',
        })
      )
    })

  return db
}

export class SequelizeQuery {
  constructor() {
    autobind(this)
  }

  resolve() {
    return Array.isArray(...arguments[0])
      ? this.findManyById(...arguments[0])
      : this.findById(...arguments[0])
  }

  findMany(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql =>
          sql.findMany({
            attributes: Object.keys(fields),
          })
        )
        .then(response => response.map(item => item.dataValues))
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindMany failed.`,
            })
          )
        )
    })
  }

  findById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql =>
          sql.findOne({
            attributes: Object.keys(fields),
            where: { id: obj.id },
          })
        )
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindById failed.`,
            })
          )
        )
    })
  }

  findManyById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql =>
          sql.findMany({
            attributes: Object.keys(fields),
            where: { id: obj.id },
          })
        )
        .then(response => response.map(item => item.dataValues))
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindManyById failed.`,
            })
          )
        )
    })
  }
}

export class SequelizeMutation {
  constructor() {
    autobind(this)
  }

  create(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql => sql.create(args))
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}Create failed.`,
            })
          )
        )
    })
  }

  remove(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql =>
          sql.destroy({
            where: { id: args.id },
          })
        )
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}Remove failed.`,
            })
          )
        )
    })
  }

  update(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql =>
          sql.update(args, {
            where: { id: args.id },
          })
        )
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}Update failed.`,
            })
          )
        )
    })
  }

  createMany(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'sequelize'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const db = databases[dbName]
    const TABLE = this.table || this.collection
    const fields = json[TABLE].schema

    return new Promise((resolve, reject) => {
      Model({
        db,
        fields,
        table: TABLE,
      })
        .then(sql => sql.bulkCreate(args))
        .then(response => response.map(item => item.dataValues))
        .then(data => resolve(data))
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}CreateMany failed.`,
            })
          )
        )
    })
  }

  // deleteMany
  // removeById
  // updateMany
}
