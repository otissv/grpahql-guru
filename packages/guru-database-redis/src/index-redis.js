/*
* redis connection
*/

import Promise from 'bluebird'
import redisP from 'promise-redis'

export function connection({ port, uri }) {
  const redis = redisP(resolver => new Promise(resolver))
  let client

  client = redis.createClient({ port, uri })

  // Event handlers
  client.on('connect', () => {
    process.stdout.write(`Redis connected to ${uri}:${port}`)
  })

  client.on('end', () => {
    process.stdout.write('Redis disconnected')
    client.quit()
  })

  client.on('error', function(error) {
    process.stdout.write('Error ' + error)
  })

  // Return instance of redis client
  return client
}

export function connect(options) {
  return connection(options)
}
