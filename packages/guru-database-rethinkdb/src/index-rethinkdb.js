import rethinkdbdash from 'rethinkdbdash'
import autobind from 'class-autobind'
import { objectId } from './rethinkdb-utils'
import { errors } from 'guru-server-utils'

export function connect(options) {
  return rethinkdbdash(options)
}

export class RethinkDBQuery {
  constructor() {
    autobind(this)
  }

  resolve() {
    return Array.isArray(...arguments[0])
      ? this.findManyById(...arguments[0])
      : this.findById(...arguments[0])
  }

  findMany(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .run()
        .then(response => {
          resolve(response)
        })
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindMany failed.`,
            })
          )
        )
    })
  }

  findById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .get(obj.id)
        .run()
        .then(response => {
          resolve(response)
        })
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindById failed.`,
            })
          )
        )
    })
  }

  findManyById(query, args, context, info) {
    const { databases, options, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    let obj = args || query
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .getAll(...obj.map(i => i.id))
        .run()
        .then(response => {
          resolve(response)
        })
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}FindManyById failed.`,
            })
          )
        )
    })
  }
}

export class RethinkDBMutation {
  constructor() {
    autobind(this)
  }

  create(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .insert(args)
        .run()
        .then(response => {
          resolve(response)
        })
        .catch(error =>
          reject(
            errors.resolverQuery({
              error,
              message: `${TABLE.toLowerCase()}Create failed.`,
            })
          )
        )
    })
  }

  remove(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    const id = args.id
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .getAll(id)
        .delete()
        .run()
        .then(response => {
          const __result = response.deleted === 1 ? 'success' : 'failed'

          resolve({ __result })
        })
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}Remove failed.`,
            })
          )
        )
    })
  }

  update(query, args, context, info) {
    const { databases, models } = context
    const dbName = this.database || 'rethinkdb'

    if (!databases || !databases[dbName])
      return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

    const r = databases[dbName]
    const id = args.id
    const TABLE = this.table || this.collection

    return new Promise((resolve, reject) => {
      r
        .table(TABLE)
        .get(id)
        .update(args)
        .run()
        .then(response => {
          resolve()
        })
        .catch(error =>
          reject(
            errors.resolverMutation({
              error,
              message: `${TABLE.toLowerCase()}Update failed.`,
            })
          )
        )
    })
  }

  // createMany
  // deleteMany
  // removeById
  // updateMany
}
