import { globJsModulePathsAsync, globPluginsPathsAsync } from 'guru-server-utils'
import camel from 'to-camel-case'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

export default (async function modelLoaderAsync() {
  try {
    const configPath = await globJsModulePathsAsync({
      fileName: 'model',
    })
    const pluginsPath = await globPluginsPathsAsync({
      fileName: 'model.js',
      plugins,
    })

    return [...configPath, ...pluginsPath].reduce((previousObj, path) => {
      const model = require(path).default
      return {
        ...previousObj,
        [camel(model.name)]: model,
      }
    }, {})
  } catch (error) {
    console.error(error)
  }
})
