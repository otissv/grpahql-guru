import { capitalize, getMethods } from 'guru-server-utils'

export default async function schemaOperationsAsync(loadedResolverModules) {
  let operations = {
    mutation: '',
    query: '',
  }

  return loadedResolverModules.reduce(
    (previous, resolver) => {
      const resolverName = resolver.name
      const moduleClass = new resolver.module()
      const methods = getMethods(moduleClass)
      operations[resolver.type] = methods.reduce((previousOperations, method) => {
        const methodName = capitalize(method)

        const type = methodName.match(/Many?/)
          ? `[${capitalize(resolverName)}]`
          : `${capitalize(resolverName)}`

        if (methodName.match(/Drop|DropMany|Nested/)) {
          return `${previousOperations}
${resolverName}${methodName} : ${type}
`
        }

        const update =
          resolver.type === 'mutation' || methodName.match(/Drop|DropMany/) !== null
            ? ` # ${methodName} update arguments
update: Input${capitalize(resolverName)}`
            : ''

        const option =
          methodName === 'Aggregate'
            ? 'Aggregate'
            : resolver.type === 'query' ? 'Query' : methodName

        return `${previousOperations}

 
${resolverName}${methodName} (
  # ${methodName} a ${resolverName} arguments
  query: InputMongodb${capitalize(resolverName)}
  # ${methodName} a ${resolverName} options
  options: InputMongodb${capitalize(resolverName)}Options
  ${update}
): ${type}
`
      }, '')

      return {
        mutation: `${previous.mutation}
      ${operations.mutation}
`,
        query: `${previous.query}
${operations.query}
`,
      }
    },
    {
      ...operations,
    }
  )
}
