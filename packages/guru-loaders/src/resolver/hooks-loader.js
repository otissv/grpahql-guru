/*
* Can access loader
*/

'use strict'
import chalk from 'chalk'
import loadResolverAsync from './load-resolver-loader'
import camel from 'to-camel-case'
import t from 'exectimer'
import {
  box,
  capitalize,
  errors,
  globJsModulePathsAsync,
  mergeGlobPathsAsync,
} from 'guru-server-utils'
const rule = '-----------------------------------------------'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

export async function hooksLoaderAsync() {
  const hook = await globJsModulePathsAsync({ fileName: 'resolverHook' })
  const mutationHook = await globJsModulePathsAsync({
    fileName: 'resolverHookMutation',
  })
  const queryHook = await globJsModulePathsAsync({
    fileName: 'resolverHookQuery',
  })

  const paths = [...hook, ...mutationHook, ...queryHook]
  return loadResolverAsync(paths)
}

export default (async function hooksAsync({ hookListAsync, locals, moduleName, moduleType, type }) {
  try {
    if (type == null) {
      throw new Error('hooks: Action type cannot be null or undefined')
    }

    const hookList = await hookListAsync
    const Hook = hookList.filter(
      hook => hook.name === moduleName && hook.type === `hook${capitalize(moduleType)}`
    )
    const GlobalHooks = hookList.filter(hook => hook.name === 'global')
    const hookModule = Hook.length > 0 && new Hook[0].module()
    const globalModule = GlobalHooks.length > 0 && new GlobalHooks[0].module()

    let globalAccess
    let accessMethod
    let beforeEach
    let globalBefore
    let beforeMethod
    let afterEach
    let globalAfter
    let afterMethod

    if (hookModule) {
      accessMethod = hookModule.access
      afterEach = hookModule.afterEach
      afterMethod = hookModule[camel(`after_${type}`)]
      beforeEach = hookModule.beforeEach
      beforeMethod = hookModule[camel(`before_${type}`)]
    }

    if (globalModule) {
      globalAccess = globalModule.access
      globalAfter =
        moduleType === 'query' ? globalModule.afterEachQuery : globalModule.afterEachMutation
      globalBefore =
        moduleType === 'query' ? globalModule.beforeEachQuery : globalModule.beforeEachMutation
    }

    const operationName = camel(`${moduleName}_${type}`)
    const capitalizeMod = capitalize(moduleName)

    const operationHookName = box(type)
      .map(str => camel(str))
      .fold(str => capitalize(str))

    return async function middleware({ fn }) {
      const initParams = { ...arguments[0] }

      function debug({ name, timer, operation = '' }) {
        const isDebug = process.env.DEBUG

        if (isDebug === 'true') {
          process.stdout.write(
            `GRAPHQL ${chalk.green(name)} - ${operation} ${moduleType} ${(
              timer.duration() * 0.000001
            ).toFixed(3)} ms\n`
          )
        }
      }

      const operationTick = new t.Tick('total')
      operationTick.start()

      try {
        return await new Promise(async (resolve, reject) => {
          // Global access operation hook

          delete initParams.fn

          if (globalAccess) {
            console.error(rule)
            const operationTick = new t.Tick('globalAccess')
            operationTick.start()

            const access = await globalAccess(initParams)
            operationTick.stop()

            debug({
              operation: operationName,
              name: 'Global hook access',
              timer: t.timers.globalAccess,
            })

            return typeof access === 'boolean'
              ? resolve(initParams)
              : reject(
                  errors.accessDenied({
                    error: `Global access hook error, ${operationName} denied.`,
                  })
                )
          } else {
            return resolve(initParams)
          }
        })
          .then(async params => {
            // Method access operation hook

            if (accessMethod) {
              const operationTick = new t.Tick('methodAccess')
              operationTick.start()

              const access = await accessMethod(params)
              operationTick.stop()

              debug({
                operation: operationName,
                name: `${capitalizeMod} hook access`,
                timer: t.timers.methodAccess,
              })

              return typeof access === 'boolean'
                ? params
                : Promise.reject(
                    errors.accessDenied({
                      error: `${capitalizeMod} access hook error, ${camel(operationName)} denied.`,
                    })
                  )
            } else {
              return params
            }
          })
          .then(async params => {
            // Global beforeEach operation hook

            if (globalBefore) {
              const operationTick = new t.Tick('globalBefore')
              operationTick.start()

              const before = await globalBefore(params)
              operationTick.stop()

              debug({
                operation: operationName,
                name: `Global hook before`,
                timer: t.timers.globalBefore,
              })

              return before
                ? params
                : Promise.reject(
                    errors.lifecycleHook({
                      error: `Global hook error, beforeEach${
                        moduleType === 'query' ? 'Query' : 'Mutation'
                      } does not return arguments.`,
                    })
                  )
            } else {
              return params
            }
          })
          .then(async params => {
            // Module beforeEach operation hook

            if (beforeEach) {
              const operationTick = new t.Tick('beforeEach')
              operationTick.start()

              const before = await beforeEach(params)
              operationTick.stop()

              debug({
                operation: operationName,
                name: `${capitalizeMod} hook before each`,
                timer: t.timers.beforeEach,
              })

              return before
                ? params
                : Promise.reject(
                    errors.lifecycleHook({
                      error: `${capitalizeMod} hook error, beforeEach does not return arguments.`,
                    })
                  )
            } else {
              return params
            }
          })
          .then(async params => {
            // Method before operation hook

            if (beforeMethod) {
              const operationTick = new t.Tick('beforeMethod')
              operationTick.start()

              const before = await beforeMethod(params)
              operationTick.stop()

              debug({
                operation: operationName,
                name: `${capitalizeMod} hook before ${camel(type)}`,
                timer: t.timers.beforeMethod,
              })

              return before
                ? params
                : Promise.reject(
                    errors.lifecycleHook({
                      error: `${capitalizeMod} hook error, before${operationHookName} does not return arguments.`,
                    })
                  )
            } else {
              return params
            }
          })
          .then(async params => {
            // Operation

            const operationTick = new t.Tick('operation')
            operationTick.start()

            const data = await fn(params)
            operationTick.stop()

            debug({
              operation: operationName,
              name: 'Operation',
              timer: t.timers.operation,
            })
            // Operation
            return {
              params,
              data,
            }
          })
          .then(async ({ data, params }) => {
            // Global afterEach operation hook

            if (globalAfter) {
              const operationTick = new t.Tick('globalAfter')
              operationTick.start()

              const after = await globalAfter({ data, params })
              operationTick.stop()

              debug({
                operation: operationName,
                name: `Global hook after`,
                timer: t.timers.globalAfter,
              })

              return after
                ? { data, params }
                : Promise.reject(
                    errors.accessDenied({
                      error: `Global hook error, afterEach${
                        moduleType === 'query' ? 'Query' : 'Mutation'
                      } does not return any data.`,
                    })
                  )
            } else {
              return { data, params }
            }
          })
          .then(async ({ data, params }) => {
            // Method afterEach operation hook

            if (afterEach) {
              const operationTick = new t.Tick('afterEach')
              operationTick.start()

              const after = await afterEach({ data, params })
              operationTick.stop()

              debug({
                operation: operationName,
                name: `${capitalizeMod} hook after each`,
                timer: t.timers.globalAfter,
              })

              return after
                ? { data, params }
                : Promise.reject(
                    errors.lifecycleHook({
                      error: `${capitalizeMod} hook error, afterEach does not return data.`,
                    })
                  )
            } else {
              return { data, params }
            }
          })
          .then(async ({ data, params }) => {
            // Method after operation hook

            if (afterMethod) {
              const operationTick = new t.Tick('afterMethod')
              operationTick.start()

              const after = await afterMethod({ data, params })
              operationTick.stop()

              debug({
                operation: operationName,
                name: `${capitalizeMod} hook after ${camel(type)}`,
                timer: t.timers.afterMethod,
              })

              return after
                ? data
                : Promise.reject(
                    errors.lifecycleHook({
                      error: `${capitalizeMod}  hook error, after${operationHookName} does not return data.`,
                    })
                  )
            } else {
              return data
            }
          })
          .then(data => {
            operationTick.stop()

            debug({
              name: `Total ${operationName}`,
              timer: t.timers.total,
            })
            return data
          })
          .catch(error => error)
      } catch (error) {
        return Promise.reject(error)
      }
    }
  } catch (error) {
    return Promise.reject(error)
  }
})
