/*
* Resolvers loader
*/

'use strict'

import Bluebird from 'bluebird'
import hooksAsync, { hooksLoaderAsync } from './hooks-loader'
import connectorReducerAsync from './connectors-reducer-loader'
import { extendedResolverQuery } from '../graphql-extend/graphql-extend-resolver'
import snake from 'to-snake-case'
import camel from 'to-camel-case'
import upperCamel from 'guru-utils/lib/to-upper-camel-case'
import get from 'lodash/fp/get'
import objectId from 'bson-objectid'
import isObj from 'isobj'
import {
  capitalize,
  getMethods,
  mergePluginsPathsAsync,
  mergeGlobPathsAsync,
  pipeAsync,
} from 'guru-server-utils'
import loadResolverAsync from './load-resolver-loader'
import schemaOperationsAsync from './schema-operations-loader'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

async function getModule({ context, moduleName, methodName }) {
  try {
    return async fnParams => {
      const Module = new context.connectors[moduleName]()
      return await Module[methodName](
        fnParams.obj,
        fnParams.args,
        fnParams.context,
        fnParams.info
      ).catch(error => Promise.reject(error))
    }
  } catch (error) {
    return error
  }
}

function mutationArgs({ context, inputArgs, inputName, prefix }) {
  const input = context.schema.definition.inputs.filter(
    item => item.name === `Input${upperCamel(inputName)}`
  )[0]

  const inputKeyName = camel(inputName)

  if (input == null) return []

  return input.fields.reduce((previousInput, item) => {
    const value = get(item.name, inputArgs)

    const typeName = item.type.name
    if (!value) return previousInput

    const nestedFieldName = typeName.substr('InputNested'.length, typeName.length)

    if (typeName.substr(0, 'InputNested'.length) === 'InputNested') {
      return {
        ...previousInput,
        [inputKeyName]: {
          ...previousInput[inputKeyName],
          [item.name]: value.id,
        },

        ...mutationArgs({
          context,
          inputArgs: value,
          inputName: nestedFieldName,
        }),
      }
    }

    return {
      ...previousInput,
      [inputKeyName]: {
        ...previousInput[inputKeyName],
        [item.name]: value,
      },
    }
  }, {})
}

async function runMutation({ args, context, hooks, info, methodName, moduleName }) {
  function addNestedId(obj) {
    var objToDotNotation = function(obj) {
      Object.keys(obj).forEach(function(key) {
        if (isObj(obj[key])) {
          return objToDotNotation(obj[key])
        }
        obj.id = obj.id ? obj.id : objectId().toString()
      })
      return obj
    }
    return objToDotNotation(obj)
  }

  let mutationResults

  try {
    if (methodName === 'save') {
      const input = addNestedId({ ...args.input })

      const mutationTypes = mutationArgs({
        context,
        inputArgs: input,
        inputName: moduleName,
      })

      mutationResults = await Promise.all(
        Object.keys(mutationTypes).map(async (mutationTypeName, index) => {
          try {
            const mutationTypeMethodName = index === 0 ? methodName : 'nested'
            const data = mutationTypes[mutationTypeName]
            const fn = await getModule({
              context,
              moduleName: mutationTypeName,
              methodName: mutationTypeMethodName,
            })

            if (mutationTypeMethodName === 'nested') {
              return await fn({
                obj: {
                  input: data,
                  options: { upsert: true },
                },
                args: undefined,
                info,
                context,
              })
            } else {
              return await hooks({
                obj: undefined,
                args: {
                  input: data,
                  options: args.options,
                },
                info,
                context,
                fn,
              })
            }

            return
          } catch (error) {
            console.error(error)
            return error
          }
        })
      )
    } else {
      mutationResults = [
        await hooks({
          obj: null,
          args,
          info,
          context,
          fn: await getModule({
            context,
            moduleName,
            methodName,
          }),
        }),
      ]
    }

    return {
      RESULTS: mutationResults.map(item => item.RESULTS),
    }
  } catch (error) {
    console.error(error)
    return error
  }
}

async function runQuery({ obj, args, context, hooks, info, methodName, moduleName }) {
  return await hooks({
    obj,
    args,
    info,
    context,
    fn: await getModule({ context, moduleName, methodName }),
  })
}

export const reduceResolverModulesAsync = loadedResolverModules => {
  const resolver = {
    Mutation: {},
    Query: {},
  }
  const hookListAsync = hooksLoaderAsync()

  return Bluebird.reduce(
    loadedResolverModules,
    (previous, current) => {
      return getMethods(current.module.prototype).reduce((prev, methodName) => {
        const moduleName = current.name
        const moduleMethodName = `${moduleName}${capitalize(methodName)}`
        const type = snake(methodName).toUpperCase()

        let mutation = { ...prev.Mutation }
        let query = { ...prev.Query }

        const method = async (obj, args, context, info) => {
          try {
            const hooks = await hooksAsync({
              hookListAsync,
              locals: context.locals,
              moduleName,
              moduleType: current.type,
              type,
            })
            if (current.type === 'mutation') {
              return await runMutation({
                args,
                context,
                hooks,
                info,
                methodName,
                moduleName,
              })
            } else if (current.type === 'query') {
              return await runQuery({
                obj,
                args,
                context,
                hooks,
                info,
                methodName,
                moduleName,
              })
            }
          } catch (error) {
            console.error(error)
          }
        }

        if (current.type === 'mutation') {
          mutation = {
            ...mutation,
            [moduleMethodName]: method,
          }
        } else if (current.type === 'query') {
          query = {
            ...query,
            [moduleMethodName]: method,
          }
        }

        return {
          Mutation: {
            ...prev.Mutation,
            ...mutation,
          },
          Query: {
            ...prev.Query,
            ...query,
          },
        }
      }, previous)
    },
    resolver
  )
    .then(resolvers => {
      if (Object.keys(resolvers.Mutation).length === 0) {
        delete resolvers.Mutation
        return resolvers
      } else {
        return resolvers
      }
    })
    .catch(error => console.error(error))
}

export default (async function resolverLoaderAsync({ schema, validation }) {
  try {
    const loadedResolverModules = await pipeAsync([
      mergeGlobPathsAsync({ fileName: 'resolverMutation' }),
      mergeGlobPathsAsync({ fileName: 'resolverQuery' }),
      mergePluginsPathsAsync({ fileName: 'resolverMutation', plugins }),
      mergePluginsPathsAsync({ fileName: 'resolverQuery', plugins }),
      loadResolverAsync,
    ])([])

    const resolvers = await reduceResolverModulesAsync(loadedResolverModules)
    const connectors = await connectorReducerAsync(loadedResolverModules)
    const schemaOperations = await schemaOperationsAsync(loadedResolverModules)

    return {
      connectors,
      resolvers: {
        ...resolvers,
        ...extendedResolverQuery,
        ...schema.definition.context,
      },
      schemaOperations,
    }
  } catch (error) {
    throw new Error(error)
  }
})
