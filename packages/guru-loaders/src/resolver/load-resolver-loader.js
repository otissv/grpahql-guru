import Bluebird from 'bluebird'
import camel from 'to-camel-case'

export default (async function loadResolverAsync(paths) {
  try {
    return await Bluebird.map(paths, async p => {
      try {
        const mutationPath = p.match('Mutation')
        const mutationHookPath = p.match('HookMutation')

        const queryPath = p.match('Query')
        const queryHookPath = p.match('HookQuery')

        const mutation = mutationHookPath || mutationPath
        const query = queryHookPath || queryPath

        const Resolver = await require(p)
        const moduleName = camel(Resolver.default.name)

        return {
          name: moduleName,
          type: (mutation && camel(mutation[0])) || (query && camel(query[0])),
          module: Resolver.default,
        }
      } catch (error) {
        console.error(error)
      }
    })
  } catch (error) {
    console.error(error)
  }
})
