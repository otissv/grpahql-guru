import { globPathsAsync, globPluginsPathsAsync } from 'guru-server-utils'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

export default (async function middlewareLoaderAsync() {
  try {
    const configPath = await globPathsAsync({
      fileName: 'core/**/middleware-index.js',
    })
    const pluginsPath = await globPluginsPathsAsync({
      fileName: 'middleware-index.js',
      plugins,
    })

    const configObj = [...configPath, ...pluginsPath].map(middleware => require(middleware).default)

    return configObj
  } catch (error) {
    console.error(error)
  }
})
