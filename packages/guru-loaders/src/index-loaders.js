/*
* Index loader
*/

import { mergeSchemaAsync, schemaLoaderAsync } from './schema/schema-loader'

import databasesLoaderAsync from './databases/databases-loader'
import graphqlconfig from './graphql/graphqlconfig'
import jsonLoaderAsync from './json/json-loader'
import middlewareLoaderAsync from './middleware/middleware-loader'
import modelLoaderAsync from './model/model-loader'
import resolverLoaderAsync from './resolver/resolvers-loader'
import routesLoaderAsync from './routes/routes-loader'
import seedAsync from './seed/seed'
import { tests } from './tests'

export default (async function loaders() {
  try {
    // Seed database
    if (process.env.SEED) {
      seedAsync()
    }

    const schema = await schemaLoaderAsync()
    const json = await jsonLoaderAsync(schema)

    const { resolvers, connectors, schemaOperations } = await resolverLoaderAsync({ schema, json })

    const schemaMerged = await mergeSchemaAsync({
      schema,
      operations: schemaOperations,
    }).catch(error => {
      throw error
    })

    // tests({
    //   definition: schemaMerged.definition,
    //   resolver: { resolvers, connectors }
    // })
    graphqlconfig({ schema: schemaMerged })

    return {
      databaseLoader: await databasesLoaderAsync(),
      jsonLoader: json,
      middlewareLoader: await middlewareLoaderAsync(),
      modelLoader: await modelLoaderAsync(),
      resolverLoader: { resolvers, connectors },
      routeLoader: await routesLoaderAsync(),
      schemaLoader: schemaMerged,
    }
  } catch (error) {
    console.error(error)
  }
})
