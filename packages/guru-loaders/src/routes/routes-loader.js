import { globPathsAsync, globPluginsPathsAsync } from 'guru-server-utils'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

export default (async function routesLoaderAsync() {
  try {
    const configPath = await globPathsAsync({
      fileName: 'core/**/routes-index.js',
    })
    const pluginsPath = await globPluginsPathsAsync({
      fileName: 'routes-index.js',
      plugins,
    })

    const configObj = [...configPath, ...pluginsPath].map(route => require(route).default)

    return configObj
  } catch (error) {
    console.error(error)
  }
})
