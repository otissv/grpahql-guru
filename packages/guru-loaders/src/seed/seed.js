import { MongoDBMutation, connect } from 'guru-database-mongodb'
import { globPathsAsync, globPluginsPathsAsync } from 'guru-server-utils'

import chalk from 'chalk'

const config = require(`${process.cwd()}/.guru.config`)
const plugins = config.plugins

async function seed({ collectionName, data, database }) {
  try {
    if (!data || data.length < 1) return
    if (!config.databases[database]) {
      throw new Error(`${database} database is not defined in .guru.config.js`)
    }

    const client = connect(config.databases[database])
    const db = await client.then(db => db)

    await db
      .collections()
      .then(collections => !collections.map(c => c.s.name).includes(collectionName))
      .then(bool => process.env.SEED !== 'insert' && !bool && db.collection(collectionName).drop())
      .then(() => process.env.SEED !== 'insert' && db.createCollection(collectionName))
      .then(() => db.collection(collectionName).insertMany(data))
      .then(() => console.log(`${chalk.blue.bold(`Inserting ${collectionName}`)} into ${database}`))
  } catch (error) {
    console.error(error)
  }
}

export default (async function databasesLoaderAsync() {
  try {
    const configPath = await globPathsAsync({
      fileName: 'seed/**/*.js',
    })
    const pluginsPath = await globPluginsPathsAsync({
      fileName: 'seed/**/*.js',
      plugins,
    })

    configPath.concat(pluginsPath).forEach(file => {
      const { data, database } = require(file).default
      const collectionName = file.substr(file.lastIndexOf('/') + 1, file.length - 1).split('.')[0]

      if (collectionName !== 'users' && collectionName !== 'roles') {
        // console.log(collectionName, data)
      }
      seed({ collectionName, data, database }).catch(error => console.log(error))
    })
  } catch (error) {
    console.error(error)
  }
})
