import { globPathsAsync, globPluginsPathsAsync } from 'guru-server-utils'

import deepMerge from 'deepmerge'

const plugins = require(`${process.cwd()}/.guru.config`).plugins

export default (async function databasesLoaderAsync() {
  try {
    const configPath = await globPathsAsync({
      fileName: 'core/**/databases-index.js',
    })
    const pluginsPath = await globPluginsPathsAsync({
      fileName: 'databases-index.js',
      plugins,
    })

    const config = (configPath[0] && require(configPath[0])) || {}
    const extensions = (pluginsPath[0] && require(pluginsPath[0])) || {}
    const configObj = deepMerge.all([config, extensions])

    return configObj.default
  } catch (error) {
    console.error(error)
  }
})
