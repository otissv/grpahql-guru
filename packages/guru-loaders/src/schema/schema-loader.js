/*
* Schema loader
*/

'use strict'

import {
  extendSchemaMutation,
  extendSchemaQuery,
  extendTypes,
} from '../graphql-extend/graphql-extend-schema'
import {
  globGQLModulePathsAsync,
  globGQLPluginsPathsAsync,
  pipeAsync,
  readFile,
} from 'guru-server-utils'
import { parse, print } from 'graphql'

import Bluebird from 'bluebird'
import schemaDefinition from './schema-definition'
import { schemaInputs } from './schema-inputs'

const _plugins = require(`${process.cwd()}/.guru.config`).plugins

async function getTypeDefinitionList(typeDefinition) {
  try {
    const definitions = parse(typeDefinition).definitions

    return definitions.reduce((previousTypes, def) => {
      if (def.kind !== 'ObjectTypeDefinition') return previousTypes
      return [...previousTypes, def.name.value]
    }, [])
  } catch (error) {
    throw error
  }
}

export async function pathsAsync(fileName) {
  try {
    const plugins = await globGQLPluginsPathsAsync({
      fileName: `${fileName}`,
      plugins: _plugins,
    })
    const modules = await globGQLModulePathsAsync({
      fileName: `${fileName}`,
    })

    return Promise.resolve([...plugins, ...modules])
  } catch (error) {
    throw error
  }
}

export function loadFilesAsync(paths) {
  return Bluebird.reduce(
    paths,
    async (previous, current) => {
      try {
        const currentFile = await readFile(current)

        return `${previous}\n${currentFile}`
      } catch (error) {
        throw error
      }
    },
    ''
  ).catch(error => console.error(error))
}

export async function mergeDefinitionAsync({ definition, operations }) {
  try {
    const mutation = parse(`type Mutation {${operations.mutation}}`).definitions[0].fields
    const query = parse(`type Query {${operations.query}}`).definitions[0].fields

    return {
      ...definition,
      operations: {
        Mutation: [...definition.operations.Mutation, ...mutation],
        Query: [...definition.operations.Query, ...query],
      },
    }
  } catch (error) {
    throw error
  }
}

export async function mergeSchemaAsync({ schema, operations }) {
  try {
    const { dsl } = schema

    const schemaDsl = `
${dsl.extendTypes}
${dsl.typeDefinition}
${dsl.inputs}
${dsl.schemaSubscriptionDefinition}

type Mutation {
${dsl.schemaMutationDefinition}
${operations.mutation}
}

type Query {
${dsl.schemaQueryDefinition}
${operations.query}
}

schema {
  mutation: Mutation
  query: Query
}
`

    const astDocument = parse(schemaDsl)

    return {
      ...schema,
      ast: astDocument,
      printAst: print(astDocument),
      definition: await mergeDefinitionAsync({
        definition: schema.definition,
        operations,
      }),
    }
  } catch (error) {
    throw error
  }
}

export async function schemaLoaderAsync() {
  try {
    const mutation = await pipeAsync([pathsAsync, loadFilesAsync])('schemaMutation')
    const query = await pipeAsync([pathsAsync, loadFilesAsync])('schemaQuery')
    const subscription = await pipeAsync([pathsAsync, loadFilesAsync])('schemaSubscription')
    const typeDefinition = await pipeAsync([pathsAsync, loadFilesAsync])('schemaType')

    const typeDefinitionList = await getTypeDefinitionList(typeDefinition)

    const inputs = await schemaInputs({ typeDefinition, typeDefinitionList })

    let schemaMutation = ''
    let schemaQuery = ''
    let schemaMutationDefinition = ''
    let schemaQueryDefinition = ''
    let schemaSubscription = ''
    let schemaSubscriptionDefinition = ''

    const hasMutation = mutation && mutation.length > 0
    if (mutation && mutation.length > 0) {
      schemaMutation = 'mutation: Mutation'
      schemaMutationDefinition = `
type Mutation {
  ${extendSchemaMutation}
  ${mutation}
}
`
    }

    const hasQuery = query && query.length > 0
    if (hasQuery) {
      schemaQuery = 'query: Query'
      schemaQueryDefinition = `type Query {
  ${extendSchemaQuery}
  ${query}
}`
    }

    const hasSubscription = subscription && subscription.length > 0
    if (hasSubscription) {
      schemaSubscription = 'subscription: Subscription'
      schemaSubscriptionDefinition = `type Subscription {
  ${extendSchemaSubscription}
  ${subscription}
}`
    }

    const cleanedTypeDefinition = typeDefinition.replace(' Nested', '')

    const schema = `
${extendTypes}
${cleanedTypeDefinition}
${inputs}
${schemaMutationDefinition}
${schemaQueryDefinition}
${schemaSubscriptionDefinition}
`

    const astDocument = parse(schema)
    const typesDocument = parse(`
      ${extendTypes}
      ${cleanedTypeDefinition}
    `)

    return await Bluebird.resolve({
      dsl: {
        extendTypes,
        typeDefinition: cleanedTypeDefinition,
        inputs,
        schemaMutationDefinition,
        schemaQueryDefinition,
        schemaSubscriptionDefinition,
        schema: {
          schemaMutation,
          schemaQuery,
          schemaSubscription,
        },
      },
      ast: astDocument,
      printAst: print(astDocument),
      types: typesDocument,
      printTypes: print(typesDocument),
      typeList: typeDefinitionList,
      definition: schemaDefinition({ astDocument, typeDefinitionList }),
    })
  } catch (error) {
    throw error
  }
}
