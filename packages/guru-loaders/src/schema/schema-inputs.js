import { parse, print } from 'graphql'

import chalk from 'chalk'

const queryOpertions = `
  # Matches arrays that contain all elements specified in the query.
  ALL: [String]
  # Matches numeric or binary values in which a set of bit positions all have a value of 0.
  BITS_ALL_CLEAR: JSON
  # Matches numeric or binary values in which a set of bit positions all have a value of 1.
  BITS_ALL_SET: JSON
  #	Matches numeric or binary values in which any bit from a set of bit positions has a value of 0.
  BITS_ANY_CLEAR: JSON
  # Matches numeric or binary values in which any bit from a set of bit positions has a value of 1.
  BITS_ANY_SET: JSON
  #Selects documents if element in the array field matches all the specified $elemMatch conditions.
  ELEMNET_MATCH: JSON
  # Matches values that are equal to a specified value.
  EQ: JSON
  # Allows use of aggregation expressions within the query language.
  EXPR: JSON
  # Matches documents that have the specified field.
  EXISTS: Boolean
  # Selects geometries that intersect with a GeoJSON geometry. The 2dsphere index supports GEO_INTERSECTS.
  GEO_INTERSECTS: InputMongodbGeometry
  # Selects geometries within a bounding GeoJSON geometry. The 2dsphere index supports GEO_WITHIN.
  GEO_WITHIN: InputMongodbGeometry
  # Matches values that are equal to a specified value.
  GT: Int
  # Matches values that are greater than or equal to a specified value.
  GTE: Int
  # Matches any of the values specified in an array.
  IN: [JSON]
  # Validate documents against the given JSON Schema.
  JSON_SCHEMA: InputMongodbJsonSchema
  # Matches values that are less than a specified value.
  LT: Int
  # Matches values that are less than a specified value.
  LTE: Int
  # 	Performs a modulo operation on the value of a field and selects documents with a specified result.
  MOD: [Int]
  # Matches values that are less than a specified value.
  NE: JSON
  #Returns geospatial objects in proximity to a point. Requires a geospatial index. The 2dsphere and 2d indexes support NEAR.
  NEAR: InputMongodbDistance
  # Returns geospatial objects in proximity to a point on a sphere. Requires a geospatial index. The 2dsphere and 2d indexes support NEAR_SPHERE.
  NEAR_SPHERE: InputMongodbDistance
  # Matches none of the values specified in an array.
  NIN: [JSON]
  # Selects documents if a field is of the specified type.
  REGEX: RegExp
  # Used with REGEX to provide options
  REGEX_OPTIONS: String
  # Selects documents if the array field is a specified size.
  SIZE: Int
  # Performs text search on the content of the fields indexed with a text index.
  TEXT: InputMongodbText
  # Selects documents if a field is of the specified type.
  TYPE: JSON
`

const logicalQueryOprations = input => `
  # Joins query clauses with a logical AND returns all documents that match the conditions of both clauses.
  AND: [${input}]
  # Joins query clauses with a logical NOR returns all documents that fail to match both clauses.
  NOR: [${input}]
  NOT: ${input}
  # Joins query clauses with a logical OR returns all documents that match the conditions of either clause.
  OR: [${input}]
  # Returns all documents that match the conditions of the query
`

export async function schemaInputs({ typeDefinition, typeDefinitionList }) {
  try {
    const definitions = parse(typeDefinition).definitions

    return definitions.reduce((previousInputs, def) => {
      if (def.kind !== 'ObjectTypeDefinition') {
        return previousInputs
      }

      const fields = def.fields.reduce(
        (previous, field) => {
          if (field.name.value === field.name.value.toUpperCase()) {
            return previous
          }

          const fieldType = field.type.kind === 'ListType' ? field.type.type : field.type

          const fieldTypeName = fieldType.name.value
          const inputType =
            fieldTypeName.substr(0, 6) === 'Nested'
              ? fieldTypeName.substr(6, fieldTypeName.length - 1)
              : fieldTypeName

          const typeName = typeDefinitionList.includes(inputType)
            ? `Input${fieldType.name.value}`
            : fieldTypeName

          return {
            input: `${previous.input ? previous.input + '\n' : ''} ${
              field.name.value
            }: ${typeName}`,
            query: `${previous.query ? previous.query + '\n' : ''} ${
              field.name.value
            }: InputMongodb${def.name.value}Operations`,
          }
        },
        { inpit: '', previous: '' }
      )

      console.log(`${chalk.blue.bold('Creating')} Input${def.name.value}
${chalk.blue.bold('Creating')} InputNested${def.name.value} 
${chalk.blue.bold('Creating')} InputMongodb${def.name.value}
${chalk.blue.bold('Creating')} InputMongodb${def.name.value}Options`)

      return `${previousInputs}
# MongoDB input ${def.name.value} 
input Input${def.name.value} {
  ${fields.input}
}

# MongoDB input nested ${def.name.value} 
input InputNested${def.name.value} {
  ${fields.input}
}

input InputMongodb${def.name.value}Operations {
  ${queryOpertions}
}

# MongoDB input ${def.name.value} query fields 
input InputMongodb${def.name.value}Fields {
  ${fields.query}
  ${logicalQueryOprations(`InputMongodb${def.name.value}Fields`)}
}

# MongoDB input ${def.name.value} query
input InputMongodb${def.name.value}  {
  ${fields.input}
  ${logicalQueryOprations(`InputMongodb${def.name.value}Fields`)}
  # Returns all documents that match the conditions of the query
  SELECT: InputMongodb${def.name.value}Fields 
}

# MongoDB input {def.name.value} query options
input InputMongodb${def.name.value}Options {
  # Modifies the way that the underlying query is executed.
  cursor: InputMongodbCursor
  # A subset of fields to return.
  projection: InputMongodbProjection
}
`
    }, '')
  } catch (error) {
    throw error
  }
}
