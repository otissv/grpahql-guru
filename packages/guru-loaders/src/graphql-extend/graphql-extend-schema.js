export const extendTypes = `

type NODE {
  id: String
  COUNT: COUNT
  RESULTS: RESULTS
  ERROR: ERROR
  META: META
}

input InputPair {
  key: String
  value: JSON
}
type ERROR {
  type:    String
  message: String
}

type RESULTS {
  id:     String
  module: String
  result: String
  error:  ERROR
  n:      Int
}

type COUNT {
  n: Int
}

type META {
  createdAt: String
  createdBy: DateTime
  updatedAt: String
  updatedBy: DateTime
}

scalar DateTime
scalar Email
scalar JSON
scalar LimitedString
scalar Password
scalar RegExp
scalar URL
scalar UUID
`

export const extendSchemaQuery = ``

export const extendSchemaMutation = ``
