import GraphQLJSON from 'graphql-type-json'
import GraphQLRegExp from './GraphQLRegExp'

import {
  GraphQLEmail,
  GraphQLURL,
  GraphQLLimitedString,
  GraphQLPassword,
  GraphQLDateTime,
  GraphQLUUID,
} from 'graphql-custom-types'

export const extendedResolverQuery = {
  JSON: {
    serialize(value) {
      return GraphQLJSON.parseValue(value)
    },
    parseValue(value) {
      return GraphQLJSON.parseValue(value)
    },
    parseLiteral(ast) {
      return GraphQLJSON.parseLiteral(ast)
    },
  },
  DateTime: GraphQLDateTime,
  Email: GraphQLEmail,
  LimitedString: GraphQLLimitedString,
  Password: GraphQLPassword,
  RegExp: GraphQLRegExp,
  URL: GraphQLURL,
  UUID: GraphQLUUID,
}
