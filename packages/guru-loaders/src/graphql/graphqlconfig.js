import Bluebird from 'bluebird'
import { dirExists, writeFiles } from 'guru-server-utils'

const fs = Bluebird.promisifyAll(require('fs'))
const GENERATED_PATH = `${process.cwd()}/__generated__`
const config = require(`${process.cwd()}/.guru.config`)

function createPrintSchema(schema) {
  writeFiles({
    dir: GENERATED_PATH,
    files: [
      {
        data: schema,
        name: 'schema.graphql',
        parser: 'stringify',
      },
    ],
  }).catch(error => {
    throw error
  })
}

function createPrintSchemaTypes(schema) {
  writeFiles({
    dir: GENERATED_PATH,
    files: [
      {
        data: schema,
        name: 'schemaTypes.graphql',
        parser: 'stringify',
      },
    ],
  }).catch(error => {
    throw error
  })
}

function createAstSchema(schema) {
  writeFiles({
    dir: GENERATED_PATH,
    files: [
      {
        data: schema,
        name: 'schema.json',
        parser: 'stringify',
      },
    ],
  }).catch(error => {
    throw error
  })
}

function createAstSchemaTypes(schema) {
  writeFiles({
    dir: GENERATED_PATH,
    files: [
      {
        data: schema,
        name: 'schemaTypes.json',
        parser: 'stringify',
      },
    ],
  }).catch(error => {
    throw error
  })
}

function createGraphqlconfig() {
  const endpoints = Object.keys(config.endpoints).reduce((previous, env) => {
    return {
      ...previous,
      [env]: config.endpoints[env],
    }
  }, {})

  const data = {
    schemaPath: 'schema.graphql',
    extensions: {
      endpoints: endpoints,
    },
  }

  writeFiles({
    dir: GENERATED_PATH,
    files: [
      {
        data,
        name: '.graphqlconfig',
        parser: 'stringify',
      },
    ],
  }).catch(error => {
    throw error
  })
}

export default function graphqlconfig({ schema, config }) {
  // create schema.graphql
  createPrintSchema(schema.printAst)

  // create schemaTypes.graphql
  createPrintSchemaTypes(schema.printTypes)

  // create schema.json
  createAstSchema(schema.ast)

  // create schemaTypes.json
  createAstSchemaTypes(schema.types)

  // create .graphqlconfig
  createGraphqlconfig(config)
}
