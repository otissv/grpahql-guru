import Bluebird from 'bluebird'
import glob from 'glob-promise'
import deepMerge from 'deepmerge'
import toSource from 'tosource'
import JSONfn from 'json-fn'
import utilErrors from './errors-utils'
import queryString from 'querystring'
import chalk from 'chalk'

const serverDir = `${process.cwd()}/lib`
const extensionsDir = `${process.cwd()}/extensions`
const node_modules = `${process.cwd()}/node_modules`
const fs = Bluebird.promisifyAll(require('fs'))

export function box(x) {
  return {
    map: f => box(f(x)),
    fold: f => f(x),
    inspect: () => `box(${x})`,
  }
}

export function capitalize(str) {
  const firstCharUpperCase = str[0].toUpperCase()
  return `${firstCharUpperCase}${str.substr(1, str.length - 1)}`
}

export function classMethods({ currentName, modulePrototype, Obj }) {
  const objInstance = new Obj()

  return modulePrototype.reduce((prev, curr) => {
    if (
      curr === 'undefined' ||
      (objInstance[curr].name === currentName && objInstance[curr].name == null)
    ) {
      return prev
    } else {
      return {
        ...prev,
        [objInstance[curr].name]: objInstance[curr],
      }
    }
  }, {})
}

export function cleanObj(obj) {
  Object.keys(obj).forEach(
    key =>
      (obj[key] && typeof obj[key] === 'object' && cleanObj(obj[key])) ||
      ((obj[key] === undefined || obj[key] === null) && delete obj[key])
  )
  return obj
}

export function createClass() {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i]
      descriptor.enumerable = descriptor.enumerable || false
      descriptor.configurable = true
      if ('value' in descriptor) descriptor.writable = true
      Object.defineProperty(target, descriptor.key, descriptor)
    }
  }

  return function(Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps)
    if (staticProps) defineProperties(Constructor, staticProps)
    return Constructor
  }
}

export function dirExists(dirPath, write) {
  return new Promise((resolve, reject) => {
    fs.exists(dirPath, resolve)
  })
    .then(exists => {
      if (!exists && write) {
        return fs.mkdir(dirPath, error => {
          if (error) return error
        })
      }
    })
    .catch(error => {
      console.error(error)
      return error
    })
}

export const errors = utilErrors

export function fileExists(filePath, write) {
  return new Promise((resolve, reject) => {
    fs.exists(filePath, resolve)
  })
    .then(exists => {
      if (!exists && write) {
        const index = filePath.lastIndexOf('/')
        const dir = filePath.substr(0, index)

        if (dir !== '.')
          return fs.mkdir(dir, error => {
            if (error) return error
          })
      } else {
        return exists
      }
    })
    .then(exists => {
      if (!exists && write) return fs.writeFileAsync(filePath, '')
    })
    .catch(error => {
      console.error(error)
      return error
    })
}

export function getAllMethods(obj) {
  let props = []

  do {
    const l = Object.getOwnPropertyNames(obj)
      .concat(Object.getOwnPropertySymbols(obj).map(s => s.toString()))
      .sort()
      .filter(
        (p, i, arr) =>
          typeof obj[p] === 'function' && (i == 0 || p !== arr[i - 1]) && props.indexOf(p) === -1
      )
    props = props.concat(l)
  } while ((obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj))

  return props
}

export function getMethods(obj) {
  let props = []

  do {
    const l = Object.getOwnPropertyNames(obj)
      .concat(Object.getOwnPropertySymbols(obj).map(s => s.toString()))
      .sort()
      .filter(
        (p, i, arr) =>
          typeof obj[p] === 'function' &&
          p !== 'constructor' &&
          (i == 0 || p !== arr[i - 1]) &&
          props.indexOf(p) === -1
      )
    props = props.concat(l)
  } while ((obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj))

  return props
}

export function globPluginsAsync({ ext, fileName, plugins }) {
  const _ext = ext ? `*.${ext}` : ''

  return Bluebird.reduce(
    plugins,
    async (previous, plugin) => {
      const resolvePlugin = require.resolve(plugin)
      const pluginPath = resolvePlugin.substr(0, resolvePlugin.lastIndexOf('/'))

      const paths = await glob(`${pluginPath}/**/${fileName}${_ext}`)
      try {
        return [...previous, ...paths]
      } catch (error) {
        console.error(error)
      }
    },
    []
  )
}

export function globGQLModulePathsAsync({ fileName }) {
  return glob(`${serverDir}/modules/**/${fileName}*.graphql`).catch(error =>
    process.stdout.write(error)
  )
}

export function globJsModulePathsAsync({ fileName }) {
  return glob(`${serverDir}/modules/**/${fileName}*.js`).catch(error => process.stdout.write(error))
}

export function globPathsAsync({ fileName }) {
  return glob(`${serverDir}/**/${fileName}`).catch(error => process.stdout.write(error))
}

export function globPluginsPathsAsync({ fileName, plugins }) {
  return globPluginsAsync({ fileName, plugins })
}

export function globJsPluginsPathsAsync({ fileName, plugins }) {
  return globPluginsAsync({ ext: 'js', fileName, plugins })
}

export function globGQLPluginsPathsAsync({ fileName, plugins }) {
  return globPluginsAsync({ ext: 'graphql', fileName, plugins })
}

export function env() {
  return Object.keys(process.env).reduce((previous, key) => {
    if (key.substr(0, 4) !== 'GURU') return previous
    const prop = JSON.parse(process.env[key])

    return {
      ...previous,
      [key]: prop.value,
    }
  }, {})
}

export function isObjectEmpty(obj) {
  function check(val) {
    // checks if string is truthy or falsey
    if (!val || val.trim === '') return true

    // checks objects length property (array)
    if (val.length && val.length === 0) return true
    if (Object.keys(val).length === 0) return true
  }

  // checks all object properties are empty
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) {
      return check(obj[key])
    }
  }

  return check(obj)
}

export async function logAsync(value) {
  try {
    return await value
  } catch (error) {
    process.stdout.write(error)
  }
}

export function meta({ userId, create }) {
  let data = {}
  const now = new Date().toISOString()

  if (create) {
    data.createdAt = now
    data.createdBy = userId
  }

  data.updatedAt = now
  data.updatedBy = userId

  return data
}

export function merge(objects) {
  return objects.reduce((previous, current) => {
    return deepMerge(previous, current)
  }, {})
}

export async function mergeGlobPathsAsync(fileName) {
  return async function() {
    try {
      return globJsModulePathsAsync(fileName).then(result => [...arguments[0], ...result])
    } catch (error) {
      console.error(error)
    }
  }
}

export async function mergePluginsPathsAsync(fileName) {
  return async function() {
    try {
      return globJsPluginsPathsAsync(fileName).then(result => [...arguments[0], ...result])
    } catch (error) {
      console.error(error)
    }
  }
}

export async function parseQueryString(str) {
  if (str.indexOf('?') >= 0) return queryString.parse(str.replace(/^.*\?/, ''))
}

export async function readFile(filePath) {
  try {
    return await fs.readFileAsync(filePath, 'utf8')
  } catch (error) {
    process.stdout.write(error)
  }
}

export async function readModuleFile(moduleFilePath) {
  try {
    const filePath = await glob(`${serverDir}/modules/${moduleFilePath}`)
    return await fs.readFileAsync(filePath[0], 'utf8')
  } catch (error) {
    process.stdout.write(error)
  }
}

export function pipeAsync(sequence) {
  return async initialValue => {
    try {
      return await Bluebird.reduce(
        sequence,
        async (previous, current) => {
          try {
            return await current(previous)
          } catch (error) {
            process.stdout.write(error)
          }
        },
        initialValue
      ).catch(error => process.stdout.write(error))
    } catch (error) {
      process.stdout.write(error)
    }
  }
}

export function writeFiles({ dir, files }) {
  function parser(data, parser) {
    const type = {
      source: data => toSource(data),
      stringify: data => JSONfn.stringify(data),
      // stringify: data => JSON.stringify(data)
    }
    return type[parser](data)
  }

  return dirExists(dir, true)
    .then(() => {
      files.forEach(file => {
        let data = file.data

        // parse data
        if (file.parser) {
          data = parser(file.data, file.parser)
        }

        // if module add export
        if (file.module) {
          data = `export default ${data} \n`
        }

        return fs.writeFile(`${dir}/${file.name}`, data, { flag: 'w' }, error => {
          if (error) return error
          process.stdout.write(`${chalk.blue.bold('Generated')} ${file.name}` + '\n')
        })
      })
    })
    .catch(error => console.error(error))
}
