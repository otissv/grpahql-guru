// guru errors
import chalk from 'chalk'

export function log(message) {
  if (process.env.DEBUG) {
    console.error(`Error: ${message}`)
  }
}

export default {
  accessDenied: ({ error, moduleName }) => {
    const _message = 'Unauthorized access. You do not have permission to access this resource'
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'ACCESS_DENIED',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  collectionName: moduleName => {
    const _message = `${moduleName} must have a database table or a collection name. 
Check that table or a collection or table has been added to the ${moduleName} decorator.
`
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'COLLECTION_NAME_ERROR',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  databaseConfiguration: async ({ dbName, moduleName }) => {
    const _message = `graphql-guru-${dbName} is not not setup correctly.
Check that graphql-guru-jsondb is installed and is configured correctly.
`

    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'DATABASE_CONFIGURATION_ERROR',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  databaseConnection: ({ error, message, moduleName }) => {
    const _message = message || (error && error.message) || 'Database connection error'
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'DATABASE_CONNECTION_ERROR',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  fileDoesNotExit: ({ file, moduleName }) => {
    const _message = `Request file ${file} does not exit`
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'FILE_DOES_NOT_EXIST',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  lifecycleHook: ({ error, moduleName }) => {
    log(error.message)

    return [
      {
        RESULTS: {
          error: {
            type: 'LIFECYCLE_HOOK',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  noInput: ({ methodName, moduleName }) => {
    const _message = `${moduleName} ${methodName} arguments must be in an query object.`
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'NO_QUERY',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  noInputArray: ({ methodName, moduleName }) => {
    const _message = `${moduleName} arguments must be in an query array of one or more objects.`
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'NO_QUERY_ARRAY',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  resolverQuery: ({ error, id, message, moduleName }) => {
    const _message = message || (error && error.message) || `${moduleName} query resolver error.`
    log(_message)

    return [
      {
        RESULTS: {
          id,
          error: {
            type: 'RESOLVER_QUERY_ERROR',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },

  resolverMutation: ({ error, id, message, n = 0, moduleName }) => {
    const _message = message || (error && error.message) || `${moduleName} mutaion resolver error.`
    log(_message)

    return [
      {
        RESULTS: {
          id,
          error: {
            type: 'RESOLVER_MUTATION_ERROR',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
          n,
        },
      },
    ]
  },

  userExists: moduleName => {
    const _message = 'User already exits'
    log(_message)

    return [
      {
        RESULTS: {
          error: {
            type: 'USER_EXITS',
            message: _message,
          },
          module: moduleName,
          result: 'failed',
        },
      },
    ]
  },
}
