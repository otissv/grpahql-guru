import { middleware as auth } from 'guru-accounts'

export default function middleware({ app, context }) {
  const { locals } = context

  auth.auth0({ app, context })
  auth.azure({ app, context })
  auth.bearer({ app, context })
  auth.bitbucket({ app, context })
  auth.facebook({ app, context })
  auth.github({ app, context })
  auth.gitlab({ app, context })
  auth.google({ app, context })
  auth.instagram({ app, context })
  auth.linkedin({ app, context })
  auth.local({ app, context })
  auth.twitter({ app, context })
  auth.windowslive({ app, context })
  auth.init({ app, context })
}
