import { routes as auth } from 'guru-accounts'

export default function routes({ app, context }) {
  const locals = context.locals
  auth.logout({ app, context, redirect: '/logout', failedRedirect: '/logout' })
  auth.auth0({ app, context, redirect: '/secure' })
  auth.azure({ app, context, redirect: '/secure' })
  auth.bearer({ app, context, redirect: '/secure' })
  auth.bitbucket({ app, context, redirect: '/secure' })
  auth.facebook({ app, context, redirect: '/secure' })
  auth.github({ app, context, redirect: '/secure' })
  auth.gitlab({ app, context, redirect: '/secure' })
  auth.google({
    app,
    context,
    options: { scope: locals.googleScope },
    redirect: '/secure',
  })
  auth.instagram({ app, context, redirect: '/secure' })
  auth.linkedin({ app, context, redirect: '/secure' })
  auth.local({ app, context, redirect: '/secure' })
  auth.twitter({ app, context, redirect: '/secure' })
  auth.windowslive({ app, context, redirect: '/secure' })

  app.use('/secure', (request, response, next) => {
    if (request.isAuthenticated()) {
      next()
    } else {
      response.send('Access Denied')
    }
  })
}
