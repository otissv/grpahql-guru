import times from 'lodash/fp/times'

export default {
  database: 'mongodb',
  data: times(i => ({
    _id: i,
    name: `category_${i}`,
    description: `category_${i} description`,
  }))(100),
}
