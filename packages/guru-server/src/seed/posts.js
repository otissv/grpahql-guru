import categories from './categories'
import comments from './comments'
import times from 'lodash/fp/times'

const randomIntToFrom = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

export default {
  database: 'mongodb',
  data: times(i => ({
    _id: i,
    category: categories.data[randomIntToFrom(0, categories.data.length - 1)],
    comments: comments.data[randomIntToFrom(0, comments.data.length - 1)],
    content: `Content for post ${i}`,
    hideComments: Boolean(randomIntToFrom(0, 1)),
    summary: `Summary description for post ${i}`,
    tile: `Post ${i}`,
  }))(100),
}
