import times from 'lodash/fp/times'

const randomIntToFrom = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
const randomUser = () => `user_${randomIntToFrom(0, 99)}`
const comment = i => ({
  _id: i,
  commenter: randomUser(),
  content: `content for comment ${i}`,
  replies:
    i < 50
      ? []
      : [
          {
            _id: 50 + i,
            commenter: randomUser(),
            content: `rely ${50 + i} for comment ${i}`,
          },
        ],
})

export default {
  database: 'mongodb',
  data: times(i => comment(i))(100),
}
