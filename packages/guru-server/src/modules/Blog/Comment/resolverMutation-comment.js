import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'comments' })
export default class Comment {}
