import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({
  collection: 'comments',
})
export default class Comment {}
