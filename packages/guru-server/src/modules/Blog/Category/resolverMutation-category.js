import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'categories' })
export default class Category {}
