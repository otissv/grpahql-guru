import { MongoDBQuery } from 'guru-database-mongodb'

@MongoDBQuery({ collection: 'categories' })
export default class Category {}
