import { MongoDBMutation } from 'guru-database-mongodb'

@MongoDBMutation({ collection: 'posts' })
export default class Post {}
