import { authorize } from 'guru-accounts'

export default class Global {
  async access({ context, info }) {
    // return await authorize({ context, info });
    return true
  }

  async beforeEachQuery(params) {
    return params
  }

  async beforeEachMutation(params) {
    return params
  }

  async afterEachQuery({ data, params }) {
    return data
  }

  async afterEachMutation({ data, params }) {
    return data
  }
}
