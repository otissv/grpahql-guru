export default class User {
  async access() {
    return true
  }

  async beforeEach(params) {
    return params
  }

  async beforeFindMany(params) {
    return params
  }

  async afterEach({ data, params }) {
    return data
  }

  async afterFindMany({ data, params }) {
    return data
  }
}
