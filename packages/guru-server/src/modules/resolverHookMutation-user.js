export default class User {
  async beforeEach(params) {
    return params
  }

  async beforeCreate(params) {
    return params
  }

  async afterEach({ params, data }) {
    return data
  }

  async afterCreate({ params, data }) {
    return data
  }
}
