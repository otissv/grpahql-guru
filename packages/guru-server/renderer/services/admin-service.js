import gql from 'graphql-tag'
import userProfileFindMany from 'userProfileFindMany.graphql'

const withQuery = () => {}
const withMutation = () => {}

export const fetchUserProfiles = withQuery(
  gql`
    query userProfileFindManyQuery {
      userProfileFindMany {
        id
        displayName
        gender
        user {
          id
          lastLoggedInWith
        }
        name {
          familyName
          givenName
        }
      }
    }
  `
)

export const fetchUserDetail = withQuery(
  gql`
    query userProfileFindByIdQuery($input: InputUserProfile) {
      userProfileFindById(input: $input) {
        id
        displayName
        name {
          familyName
          givenName
        }
        user {
          id
          lastLoggedInWith
          roles {
            role
            description
          }
          username
        }
        bio
        dataOfBirth
        photos
        telephoneNumbers
        gender
        website
        organization
        location
        emails {
          value
          type
        }
      }
    }
  `,
  {
    options: props => {
      return {
        variables: {
          input: {
            id: '5a47a080b7e4bf667325cc29', //props.store.selectedId.userDetail
          },
        },
      }
    },
  }
)

export const fetchRoles = withQuery(
  gql`
    query roleFindManyQuery {
      roleFindMany {
        id
        description
        role
      }
    }
  `
)

export const fetchRole = withQuery(
  gql`
    query roleFindByIdQuery($input: InputRole) {
      roleFindById(input: $input) {
        id
        role
        description
      }
    }
  `,
  {
    options: props => {
      return {
        variables: {
          input: {
            id: props.url.query.role,
          },
        },
      }
    },
  }
)

export const mutateUserProfile = withMutation(
  gql`
    mutation userProfileMutation($input: InputUserProfile) {
      userProfileUpdate(input: $input) {
        id
        RESULTS {
          result
          error {
            message
          }
        }
      }
    }
  `
)
