import styled from 'styled-components'
import media from '../../styled/media'

const Footer = styled.footer`
  margin-top: ${props => props.theme.spacing.large};
  ${media.greaterThan('medium1')`  
    margin-top: 0;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
  `};
`

export default Footer
