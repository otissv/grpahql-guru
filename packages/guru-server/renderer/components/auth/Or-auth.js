import styled from 'styled-components'

const Divider = styled.main`
  margin: ${props => props.theme.spacing.medium};
`
export default Divider
