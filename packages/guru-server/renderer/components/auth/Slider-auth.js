import styled from 'styled-components'
import media from '../../styled/media'

export const slideStyle = theme => `
  position: absolute;
  top: 0;
  bottom: 0;
  width: 100%;  
`

const Slider = styled.div`
  position: relative;
  flex: 1;
  overflow-x: hidden;

  ${media.greaterThan('medium1')`overflow: hidden;`};
`

export default Slider
