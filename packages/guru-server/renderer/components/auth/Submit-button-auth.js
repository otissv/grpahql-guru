import styled from 'styled-components'
import media from '../../styled/media'
import Button from '../../styled/components/Button'

const SubmitButton = styled(Button)`
  ${media.greaterThan('medium1')`
    border-bottom-left-radius: ${props => props.theme.border.rounded};
    border-bottom-right-radius: ${props => props.theme.border.rounded};
  `};
`

const Submit = ({ children, onClick }) => (
  <SubmitButton context="primary" stretch type="submit">
    {children}
  </SubmitButton>
)

export default Submit
