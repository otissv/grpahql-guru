import { PureComponent } from 'react'
import styled from 'styled-components'
import ButtonSquared from '../../styled/components/ButtonSquared'
import FacebookIcon from '../../images/icons/social/facebook.svg'
import GoogleIcon from '../../images/icons/social/google.svg'
import TwitterIcon from '../../images/icons/social/twitter-box.svg'
import GitHubIcon from '../../images/icons/social/github.svg'

const ProviderButton = styled(ButtonSquared)`
  margin: 0 5px;
`

class Provider extends PureComponent {
  handleOnClick = event => {
    event.preventDefault()
    window.location.replace(this.props.href)
  }

  render() {
    return (
      <ProviderButton onClick={this.handleOnClick} {...this.props}>
        {this.props.children}
      </ProviderButton>
    )
  }
}

export default class Providers extends PureComponent {
  render() {
    return (
      <div>
        <Provider href="https://localhost:8443/auth/facebook" background="#2e519f">
          <FacebookIcon style={{ height: '25px', width: '25px', fill: '#fff' }} />
        </Provider>

        <Provider href="https://localhost:8443/auth/google" background="#dc4031">
          <GoogleIcon style={{ height: '25px', width: '25px', fill: '#fff' }} />
        </Provider>
        <Provider href="https://localhost:8443/auth/twitter" background="#1da1f2">
          <TwitterIcon style={{ height: '25px', width: '25px', fill: '#fff' }} />
        </Provider>
        <Provider href="https://localhost:8443/auth/github" background="#24292e">
          <GitHubIcon style={{ height: '25px', width: '25px', fill: '#fff' }} />
        </Provider>
      </div>
    )
  }
}
