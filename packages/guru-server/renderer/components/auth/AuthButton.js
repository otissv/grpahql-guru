import styled from 'styled-components'
import Button from '../../styled/components/Button'

const AuthButton = styled(Button)`
  width: 50%;
  border-bottom: ${props =>
    props.active ? props.theme.border.thin : props.theme.border.thinSecondary};
  &:hover {
    border: ${props => props.theme.border.thinTransparent};
    border-bottom: ${props =>
      props.active ? props.theme.border.thin : props.theme.border.thinSecondary};
  }

  &:active {
    background: none;
    border: ${props => props.theme.border.thinTransparent};
    border-bottom: ${props =>
      props.active ? props.theme.border.thin : props.theme.border.thinSecondary};
  }

  &:focus {
    background: none;
    border: ${props => props.theme.border.thinTransparent};
    border-bottom: ${props =>
      props.active ? props.theme.border.thin : props.theme.border.thinSecondary};
  }
`

export default AuthButton
