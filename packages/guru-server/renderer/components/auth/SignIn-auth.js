import { Component } from 'react'
import styled from 'styled-components'
import media from '../../styled/media'
import Button from '../../styled/components/Button'
import ButtonSquared from '../../styled/components/ButtonSquared'
import Input from '../../styled/components/Input'
import Providers from './Provider-auth'
import Divider from './Or-auth'
import Main from './Main-auth'
import Footer from './Footer-auth'
import { slideStyle } from './Slider-auth'
import Submit from './Submit-button-auth'
import Form from '../../styled/components/form/Form'
import FormInput from '../../styled/components/form/FormInput'
import { signInForm } from '../../store/auth-form-store'

const SignInStyled = styled.div`
  ${slideStyle};
  transition: ${props => props.theme.animation.easeMedium('transform')};
  transform: ${props => (props.active ? 'translateX(0)' : 'translateX(100%)')};
`

const Forgot = styled.a`
  margin: ${props => props.theme.spacing.xlarge};
  display: block;
  color: #fff;
`

export default class SignIn extends Component {
  render() {
    const { active, store: { forms } } = this.props
    const fields = forms.signIn

    return (
      <SignInStyled active={active}>
        <Form name="signIn" {...this.props}>
          <Main>
            <Providers />
            <Divider>or</Divider>
            <FormInput aligned="left" {...fields.email} styledInput="margin-top: 10px;" />
            <FormInput aligned="left" {...fields.password} styledInput="margin-top: 10px;" />
          </Main>
          <Footer>
            <Submit>Login</Submit>
          </Footer>
        </Form>
      </SignInStyled>
    )
  }
}
