export default {
  signIn: [
    {
      name: 'email',
      type: 'email',
      placeholder: 'your email',
      rules: { presence: true, email: true },
      value: 's.jobs@apple.com',
      message: 'Email is required',
      help: ' this is help text',
    },
    {
      name: 'password',
      type: 'password',
      placeholder: 'your Password',
      rules: { presence: true, email: true },
      message: 'Password is required',
    },
  ],
}
