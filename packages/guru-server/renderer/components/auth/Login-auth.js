import { Component } from 'react'
import styled from 'styled-components'
import media from '../../styled/media'
import LogoIcon from '../../images/guru-logo.svg'
import SignIn from './SignIn-auth'
import SignUp from './SignUp-auth'
import Slider from './Slider-auth'
import AuthButton from './AuthButton'

const Logo = styled(LogoIcon)`
  height: 100px;
`

const Container = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  position: absolute;
  width: 100%;
  height: 100vh;

  ${media.greaterThan('medium1')`
    box-shadow: ${props => props.theme.depth.d5};
    border-radius: ${props => props.theme.border.rounded};
    left: 50%;
    top: 50%;
    height: 500px;
    margin-top: -250px;
    width: 360px;
    margin-left: -180px;
  `};
`

const Header = styled.div`
  text-align: center;
`

export default class LoginPage extends Component {
  state = {
    signIn: true,
    signUp: false,
  }

  toggleActive = () => {
    this.setState({
      signIn: !this.state.signIn,
      signUp: !this.state.signUp,
    })
  }

  handelOnLoginClick = () => {}

  handelOnSignUpClick = () => {}

  render() {
    const { providers, submit } = this.props

    return (
      <Container>
        <Header>
          <Logo />
        </Header>
        <div>
          <AuthButton active={this.state.signIn} onClick={this.toggleActive}>
            Log In
          </AuthButton>
          <AuthButton active={this.state.signUp} onClick={this.toggleActive}>
            Sign In
          </AuthButton>
        </div>
        <Slider>
          <SignUp {...this.props} active={this.state.signUp} />
          <SignIn {...this.props} active={this.state.signIn} />
        </Slider>
      </Container>
    )
  }
}
