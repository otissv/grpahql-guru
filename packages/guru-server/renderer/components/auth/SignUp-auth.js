import { Component } from 'react'
import styled from 'styled-components'
import media from '../../styled/media'
import Button from '../../styled/components/Button'
import ButtonSquared from '../../styled/components/ButtonSquared'
import Input from '../../styled/components/Input'
import Providers from './Provider-auth'
import Divider from './Or-auth'
import Main from './Main-auth'
import Footer from './Footer-auth'
import { slideStyle } from './Slider-auth'
import Submit from './Submit-button-auth'
import Form from '../../styled/components/form/Form'
import forms from './forms-auth'

const SignUpStyled = styled.div`
  ${slideStyle};
  transition: ${props => props.theme.animation.easeMedium('transform')};
  transform: ${props => (props.active ? 'translateX(0)' : 'translateX(-100%)')};
`

export default class SignUp extends Component {
  handleSubmit = values => {}

  render() {
    const { active, form } = this.props

    return (
      <SignUpStyled active={active}>
        <Main>
          <Form name="signOut" {...this.props} onSubmit={this.onSubmit}>
            <Providers />
            <Divider>or</Divider>
            <Input placeholder="username" styledInput="margin-top: 10px;" />
            <Input type="email" placeholder="email" styledInput="margin-top: 10px;" />
            <Input type="password" placeholder="password" styledInput="margin-top: 10px;" />
            <Input placeholder="telephone number" styledInput="margin-top: 10px;" />
            <Footer>
              <Submit>Sign UP</Submit>
            </Footer>
          </Form>
        </Main>
      </SignUpStyled>
    )
  }
}
