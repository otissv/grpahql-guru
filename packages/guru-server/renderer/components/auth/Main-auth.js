import styled from 'styled-components'

const Main = styled.div`
  padding: ${props => props.theme.spacing.medium};
  padding-top: 0;
  margin-top: ${props => props.theme.spacing.medium};
`

export default Main
