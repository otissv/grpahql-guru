import Link from 'next/link'
import Head from 'next/head'
import { ServerStyleSheet, ThemeProvider } from 'styled-components'
import global from '../styles/global.scss'
import theme from '../styled/index-theme'
import Row from '../styled/components/Row'

export default args => ComposedComponent => {
  return class Layout extends React.Component {
    static async getInitialProps({ req }) {
      const sheet = new ServerStyleSheet()
      const styleTags = sheet.getStyleElement()

      return { styleTags }
    }

    render() {
      const { css, graphql, heading, styleTags, title = 'Page title' } = args

      return (
        <div>
          <Head>
            <title>{title}</title>
            <link href="https://fonts.googleapis.com/css?family=Biryani" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet" />
            <style dangerouslySetInnerHTML={{ __html: global }} />
            {css ? css.map(s => <style key="s" dangerouslySetInnerHTML={{ __html: s }} />) : null}
          </Head>

          <ThemeProvider theme={theme('dark')}>
            <div>
              <Row>
                <h1>{heading}</h1>
              </Row>
              <ComposedComponent />
            </div>
          </ThemeProvider>
        </div>
      )
    }
  }
}
