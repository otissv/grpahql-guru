import { Component } from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import Table from '../../../styled/components/table/Table'

class Roles extends Component {
  getTableData = () => {
    const { Role } = this.props.store.graphql
    const roles = Role || []

    return Object.keys(roles).map(key => {
      const roles = Role || []
      const { id, role, description } = roles[key]
      return {
        'display name': (
          <Link prefetch href={`/admin-role?role=${id}`}>
            <a>{role}</a>
          </Link>
        ),
        description,
      }
    })
  }

  render() {
    const data = this.getTableData()

    return (
      <Table caption="User list" sort={['name', '-height']} head="*" body={data} divider striped />
    )
  }
}

export default Roles
