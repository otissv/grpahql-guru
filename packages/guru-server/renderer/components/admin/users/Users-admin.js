import { Component } from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import Table from '../../../styled/components/table/Table'

class Users extends Component {
  getTableData = () => {
    const { UserProfile } = this.props.store.graphql
    const profiles = UserProfile || []

    return Object.keys(profiles).map(key => {
      const { id, displayName, user } = profiles[key]
      return {
        'display name': (
          <Link prefetch href={`/admin/users/${id}`}>
            <a>{displayName}</a>
          </Link>
        ),
        id,
        'last Logged In With': user.lastLoggedInWith,
      }
    })
  }

  render() {
    const data = this.getTableData()

    return (
      <Table caption="User list" sort={['name', '-height']} head="*" body={data} divider striped />
    )
  }
}

export default Users
