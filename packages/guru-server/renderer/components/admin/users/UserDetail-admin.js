import { Component } from 'react'
import Link from 'next/link'
import styled from 'styled-components'

class UserDetail extends Component {
  getSelectedItem = () => {
    const { selectedId, store: { graphql: { UserProfile } } } = this.props

    return (UserProfile && UserProfile[selectedId]) || {}
  }

  render() {
    const { selectedId } = this.props
    const { displayName, gender, name, user } = this.getSelectedItem()
    const { id, lastLoggedInWith } = user || {}
    const { familyName, givenName } = name || {}

    return (
      <div>
        <Link prefetch href={`/admin/users/${selectedId}/edit`}>
          <a>Edit</a>
        </Link>
        <p>ID: {id}</p>
        <p>Display name: {displayName}</p>
        <p>Family name: {familyName}</p>
        <p>Given name: {givenName}</p>
        <p>Gender: {gender || 'N/A'}</p>
        <p>Last logged in with: {lastLoggedInWith}</p>
      </div>
    )
  }
}

export default UserDetail
