import { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import styled from 'styled-components'
import { generateForm } from '../../../utils/generate-form'
import Row from '../../../styled/components/Row'
import camel2Title from '../../../utils/camel-2-title'

class EditUserProfile extends Component {
  formName = 'adminUserProfile'

  componentWillUnmount() {
    const { store: { removeForm } } = this.props
    removeForm({ formName: this.formName })
  }

  onChange = ({ field, value }) => {}

  onSubmit = ({ values }) => {
    const { store: { removeForm }, url, userProfileMutation } = this.props

    userProfileMutation(values)
    Router.push({
      pathname: url.pathname.replace('edit-', ''),
      query: url.query,
    })
  }

  handelOnLoginClick = () => {}

  handelOnSignUpClick = () => {}

  getForm() {
    const { selectedId, store: { graphql: { UserProfile } } } = this.props
    return generateForm({
      name: this.formName,
      prop: UserProfile && UserProfile[this.props.selectedId],
      props: {
        ...this.props,
        // onChange: this.onChange,
        onSubmit: this.onSubmit,
      },
      schema: {
        submit: {
          value: 'Send',
          styled: { context: 'ghost' },
        },
        defaults: {
          label: {
            value: value => camel2Title(value),
          },
          input: { widths: 'xlarge' },
          error: { widths: 'xlarge' },
        },
        bio: {
          type: 'textarea',
          validate: ({ value, validator }) => validator.isAlpha(value),
          error: { message: 'Field must only contain letters' },
        },
        dataOfBirth: {
          name: 'dataOfBirth',
          type: 'date',
        },
        name: {
          name: 'name',
          fields: {
            defaults: {
              presence: true,
            },
          },
        },
        user: {
          fields: {
            lastLoggedInWith: {
              ignore: true,
            },
            username: {
              presence: true,
            },
          },
        },
        emails: {
          name: 'emails',
          type: 'email',
          list: true,
          addButton: {
            context: 'ghost',
          },
        },
        telephoneNumbers: {
          name: 'telephoneNumbers',
          type: 'tel',
          list: true,
          addButton: {
            context: 'ghost',
          },
        },
        photos: {
          name: 'photos',
          type: 'photo',
          list: true,
          addButton: {
            context: 'ghost',
          },
        },
        gender: {
          name: 'gender',
          type: 'select',
          options: [{ label: 'Male', value: 'male' }, { label: 'Female', value: 'female ' }],
        },
        // roles: {
        //   name: 'roles',
        //   type: 'select',
        //   options: []
        // }
      },
    })
  }

  render() {
    return <Row container>{this.getForm()}</Row>
  }
}

export default EditUserProfile
