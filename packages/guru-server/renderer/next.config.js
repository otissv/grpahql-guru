const path = require('path')
const glob = require('glob')

module.exports = {
  distDir: 'build',
  webpack: config => {
    const webpack = require('webpack')
    config.plugins = config.plugins || []

    // const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    // config.plugins.push(new BundleAnalyzerPlugin());

    config.module.rules.push(
      {
        test: /\.(css|scss)/,
        loader: 'emit-file-loader',
        options: {
          name: 'dist/[path][name].[ext]',
        },
      },
      {
        test: /\.css$/,
        use: ['babel-loader', 'raw-loader', 'postcss-loader'],
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          'babel-loader',
          'raw-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['styles', 'node_modules']
                .map(d => path.join(__dirname, d))
                .map(g => glob.sync(g))
                .reduce((a, c) => a.concat(c), []),
            },
          },
        ],
      }
    )

    return config
  },
  webpackDevMiddleware: config => {
    // Perform customizations to webpack dev middleware config

    // Important: return the modified config
    return config
  },
}

// Warning: Adding loaders to support new file types (css, less, svg, etc.) is not recommended
// because only the client code gets bundled via webpack and thus it won't work on the initial
// server rendering. Babel plugins are a good alternative because they're applied consistently
// between server/client rendering (e.g. babel-plugin-inline-react-svg).
