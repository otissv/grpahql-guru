import { Component } from 'react'
import Layout from '../../../components/Layout'
import { fetchRole } from '../../../services/admin-service'
import RolesPage from '../../../components/admin/roles/Roles-admin'

@fetchRole
class Roles extends Component {
  render() {
    return (
      <Layout title="Roles | Guru Server" heading="Roles" store={this.props.store}>
        <RolesPage />
      </Layout>
    )
  }
}

export default Roles
