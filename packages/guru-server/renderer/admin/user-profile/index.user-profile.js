import { Component } from 'react'
import Layout from '../../../components/Layout'
import { fetchUserDetail } from '../../../service/admin-service'
import UsersPage from '../../../components/admin/users/UserProfile-admin'

function setSelected(query) {
  const selectedId = query.id
  if (selectedId) {
    const selectedId = query && query.id
    return { selectedId }
  }
}

@fetchUserDetail
class UserProfile extends Component {
  static getInitialProps({ req, query, renderPage }) {
    const selectedId = (req && req.query.id) || query.id
    return { selectedId }
  }

  render() {
    return (
      <Layout title="User Profile | Guru Server" heading="User Profile" store={this.props.store}>
        <UsersPage {...this.props} />
      </Layout>
    )
  }
}

export default UserProfile
