import { Component } from 'react'
import Layout from '../../components/Layout'
import Login from '../../components/auth/Login-auth'
import copyMethodsIntoProps from '../utils/copyMethodsIntoProps'

export default class AdminIndex extends Component {
  onChange = ({ formName, fields }) => {}
  onSubmit = ({ formName, values }) => {}
  validate = ({ formName, fields }) => {}

  handelOnLoginClick = () => {}

  handelOnSignUpClick = () => {}

  render() {
    return (
      <Layout title="Guru Server" store={this.props.store}>
        <Login providers="" {...copyMethodsIntoProps(this)} />
      </Layout>
    )
  }
}
