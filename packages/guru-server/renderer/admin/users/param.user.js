import { Component } from 'react'
import Layout from '../../../components/Layout'
import { fetchUserDetail } from '../../../services/admin-service'
import UserDetail from '../../../components/admin/users/UserDetail-admin'

@fetchUserDetail
class UserProfile extends Component {
  static getInitialProps({ req, query, renderPage }) {
    const selectedId = req && req.params.param
    return { selectedId }
  }

  render() {
    return (
      <Layout title="User Profile | Guru Server" heading="User Profile" store={this.props.store}>
        <UserDetail {...this.props} />
      </Layout>
    )
  }
}

export default UserProfile
