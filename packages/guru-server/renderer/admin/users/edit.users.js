import { Component } from 'react'
import Layout from '../../../components/Layout'
import EditUserProfilePage from '../../../components/admin/users/Edit-userProfile-admin'
import { fetchUserDetail, mutateUserProfile } from '../../../services/admin-service'

@fetchUserDetail
// @mutateUserProfile
class EditUserProfile extends Component {
  static getInitialProps({ req, query, renderPage }) {
    const selectedId = (req && req.query.id) || query.id
    return { selectedId }
  }

  render() {
    return (
      <Layout title="User Profile | Guru Server" heading="User Profile" store={this.props.store}>
        <EditUserProfilePage {...this.props} />
      </Layout>
    )
  }
}

export default EditUserProfile
