import { Component } from 'react'
import Layout from '../../../components/Layout'
import { fetchUserProfiles } from '../../../services/admin-service'
import UsersPage from '../../../components/admin/users/Users-admin'

@fetchUserProfiles
class Users extends Component {
  render() {
    return (
      <Layout title="Users | Guru Server" heading="Users" store={this.props.store}>
        <UsersPage />
      </Layout>
    )
  }
}

export default Users
