export const signInForm = {
  signIn: {
    email: {
      type: 'email',
      placeholder: 'your email',
      rules: { presence: true, email: true, optional: true },
      value: 's.jobs@apple.com',
      message: 'Email is required',
      help: ' this is help text',
    },
    password: {
      type: 'password',
      placeholder: 'your Password',
      rules: { presence: true, email: true, optional: true },
      message: 'Password is required',
    },
  },
}

export const signUpForm = {
  signUp: {
    username: {
      placeholder: 'username',
      rules: { presence: true },
      message: 'Username is required',
    },
    email: {
      type: 'email',
      placeholder: 'email',
      rules: { presence: true, email: true },
      message: 'Email is required',
    },
    password: {
      type: 'password',
      placeholder: 'your Password',
      rules: { presence: true, email: true },
      message: 'Password is required',
    },
  },
}
