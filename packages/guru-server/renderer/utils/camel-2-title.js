export default function camel2title(camelCase) {
  return camelCase
    ? camelCase
        .replace(/([A-Z])/g, match => ` ${match}`)
        .toLowerCase()
        .replace(/^./, match => match.toUpperCase())
    : ''
}
