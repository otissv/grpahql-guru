import { Component } from 'react'
import { observeStore, injectForm } from '../lib/store'
import merge from 'deepmerge'
import Form from '../styled/components/form/Form'
import FormRow from '../styled/components/form/FormRow'
import FormInput from '../styled/components/form/FormInput'
import FormErrorList from '../styled/components/form/FormErrorList'
import Button from '../styled/components/Button'
import camel2Title from './camel-2-title'
import isIgnoredField from './is-ignored-field'

export const generateForm = function GenerateForm({ name, prop = {}, schema = {}, props }) {
  const buildForm = field => {
    return field
      ? Object.keys(field).reduce((previous, key) => {
          const value = field[key] || ''

          if (isIgnoredField({ key })) {
            return previous
          } else if (value && !Array.isArray(value) && typeof value === 'object') {
            return {
              ...previous,
              [key]: { name: key, fields: buildForm(value) },
            }
          } else {
            return { ...previous, [key]: { name: key, label: key, value } }
          }
        }, {})
      : {}
  }
  // TODO: use isIgnoredField on merge
  @injectForm({ [name]: merge(buildForm(prop), schema) })
  @observeStore
  class FormComponent extends Component {
    componentDidMount() {
      this.fields = this.props.store.forms[name]
    }

    getField = ({ fieldName, item, defaults }) => {
      if (item.component) {
        return item.component({ fieldName, item, defaults })
      } else {
        return (
          <FormInput
            defaults={defaults}
            input={{ ...defaults.input, ...item.input }}
            key={fieldName}
            {...item}
            error={{ ...defaults.error, ...item.error }}
            name={fieldName}
            label={{
              ...defaults.label,
              ...item.label,
              value:
                (defaults.label && defaults.label.value && defaults.label.value(item.name)) ||
                item.name,
            }}
            value={item.value}
          />
        )
      }
    }

    addFieldItem = event => {
      event.preventDefault()
    }

    getFormFields = ({ fields, prefix = '', defaults = {} }) => {
      const _fields = { ...fields }
      delete _fields.touched
      delete _fields.rules
      delete _fields.isSubmitting
      delete _fields.errors

      return Object.keys(_fields)
        .filter(key => fields[key].name)
        .map(key => {
          const item = fields[key]

          if (isIgnoredField({ key, items: [item.ignore] })) return null

          const _defaults = { ...defaults, ...fields.defaults }
          const fieldName = prefix ? `${prefix}.${item.name}` : item.name

          if (item.fields) {
            return this.getFormFields({
              fields: item.fields,
              prefix: fieldName,
              defaults: _defaults,
            })
          } else if (item.list) {
            if (Array.isArray(item.value)) {
              return this.getField({ fieldName, item, defaults: _defaults })
            } else {
              return (
                <div key={key}>
                  {this.getField({ fieldName, item, defaults: _defaults })}
                  <Button
                    {...item.addButton}
                    data-target="fieldName"
                    onClick={this.addFieldItem}
                  >{`Add new ${item.name}`}</Button>
                </div>
              )
            }
          } else {
            return this.getField({ fieldName, item, defaults: _defaults })
          }
        })
    }

    render() {
      const fields = { ...this.props.store.forms[name] }
      const { value, styled } = fields.submit
      return (
        <div>
          <FormErrorList errors={fields.errors} />
          <Form name={name} {...this.props}>
            {this.getFormFields({ fields })}
            <FormRow styledFormRow="margin-top: 30px">
              <Button value={value || 'submit'} {...styled} context="primary" />
            </FormRow>
          </Form>
        </div>
      )
    }
  }

  return <FormComponent {...props} />
}
