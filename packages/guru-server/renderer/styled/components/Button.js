import React, { PureComponent } from 'react'
import styled from 'styled-components'
import { getThemeProp } from '../utils-styled'
import kebab from 'kebab-case'
import { reduce } from 'async'

function getButtonContext(style, props) {
  return getThemeProp({
    theme: 'button',
    prop: 'context',
    props,
    style,
  })
}

function getButtonSize(style, props) {
  return getThemeProp({
    theme: 'button',
    prop: 'size',
    props,
    style,
  })
}

function getButtonStretch(style, props) {
  return getThemeProp({
    theme: 'button',
    prop: 'stretch',
    props,
    style,
  })
}

const ButtonStyled = styled.button`
  ${props => {
    const theme = props.theme.button

    function getStyle(key) {
      switch (key) {
        case 'background':
          return `background: ${getButtonContext('background', props)};`
        case 'border':
          return `border: ${getButtonContext('border', props)};`
        case 'color':
          return `color: ${getButtonContext('color', props)};`
        case 'cursor':
          return `cursor: ${theme.cursor};`
        case 'lineHeight':
          return `line-height: ${getButtonSize('lineHeight', props)};`
        case 'padding':
          return `padding: ${getButtonSize('padding', props)};`
        case 'stretch':
          return `width: ${props.stretch &&
            getButtonStretch('stretch', { ...props, stretch: null })};`
        case 'hover':
          return `&:${key} {${Object.keys(theme[key]).reduce(
            (previous, subKey) =>
              previous +
              ' ' +
              kebab(subKey) +
              ': ' +
              getButtonContext(key + '.' + subKey, props) +
              ';',
            ''
          )}}`
        case 'active':
          return `&:${key} {${Object.keys(theme[key]).reduce(
            (previous, subKey) =>
              previous +
              ' ' +
              kebab(subKey) +
              ': ' +
              getButtonContext(key + '.' + subKey, props) +
              ';',
            ''
          )}}`
        case 'focus':
          return `&:${key} {${Object.keys(theme[key]).reduce(
            (previous, subKey) =>
              previous +
              ' ' +
              kebab(subKey) +
              ': ' +
              getButtonContext(key + '.' + subKey, props) +
              ';',
            ''
          )}}`
        default:
          return `${kebab(key)}: ${theme[key]};`
      }
    }

    return Object.keys(theme).reduce(
      (previous, key) => `${previous} 
${getStyle(key)}`,
      ``
    )
  }} ${props => props.styledButton};
`

export default class Button extends PureComponent {
  render() {
    const { value, children } = this.props
    return (
      <ButtonStyled className="Button" {...this.props}>
        {value || children}
      </ButtonStyled>
    )
  }
}
