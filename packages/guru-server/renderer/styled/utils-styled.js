export function getThemeProp({ prop, props, style, theme }) {
  const _style = style.split('.')
  const _theme = props.theme[theme]
  const attr = _style[0]
  const value = _style[1]
  const key = props[prop]

  if (key && value) return _theme[key][attr][value]
  if (key) return _theme[key][attr]
  return _theme[attr]
}
