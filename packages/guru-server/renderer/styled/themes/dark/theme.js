import Accordion from './accordion-theme'
import Border from './border-theme'
import Button from './button-theme'
import ButtonSquared from './button-squared-theme'
import Checkbox from './checkbox-theme'
import Close from './close-theme'
import Colors from './colors-theme'
import Font from './font-theme'
import Form from './form-theme'
import Input from './input-theme'
import List from './list-theme'
import Logo from './logo-theme'
import Modal from './modal-theme'
import Nav from './nav-theme'
import Scrollbar from './scrollbar-theme'
import Select from './select-theme'
import Sidebar from './sidebar-theme'
import Statusbar from './statusbar-theme'
import Svg from './svg-theme'
import Tabs from './tabs-theme'
import Textarea from './textarea-theme'
import Toolbar from './toolbar-theme'
import Inject from './injectGloab-theme'

// shared
import Animation from '../shared/animation-theme'
import Clearfix from './../shared/clearfix-theme'
import Global from './../shared/global-theme'
import Spacing from '../shared/spacing-theme'
import media from '../../media'

// base
export const colors = Colors()
export const border = Border({ colors })
export const clearfix = Clearfix()
export const font = Font({ colors })
export const spacing = Spacing()
export const scrollbar = Scrollbar({ colors, spacing })
export const animation = Animation()

const base = {
  animation,
  border,
  clearfix,
  colors,
  font,
  media,
  scrollbar,
  spacing,
}

export const global = Global({ ...base, inject: Inject({ colors, spacing }) })

// modules
export const accordion = Accordion(base)
export const button = Button(base)
export const buttonSquared = ButtonSquared(base)
export const checkbox = Checkbox(base)
export const close = Close(base)
export const form = Form(base)
export const input = Input(base)
export const list = List(base)
export const logo = Logo(base)
export const modal = Modal(base)
export const nav = Nav(base)
export const select = Select(base)
export const sidebar = Sidebar(base)
export const statusbar = Statusbar(base)
export const svg = Svg(base)
export const tabs = Tabs(base)
export const textarea = Textarea(base)
export const toolbar = Toolbar(base)

export default {
  accordion,
  animation,
  border,
  button,
  buttonSquared,
  checkbox,
  clearfix,
  close,
  colors,
  font,
  form,
  input,
  list,
  logo,
  modal,
  nav,
  scrollbar,
  select,
  sidebar,
  spacing,
  statusbar,
  svg,
  tabs,
  textarea,
  toolbar,
}
