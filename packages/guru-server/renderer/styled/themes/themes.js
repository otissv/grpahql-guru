import Accordion from './components/accordion-theme'
import Border from './components/border-theme'
import Button from './components/button-theme'
import ButtonSquared from './components/button-squared-theme'
import Checkbox from './components/checkbox-theme'
import Close from './components/close-theme'
import Font from './components/font-theme'
import Form from './components/form-theme'
import Input from './components/input-theme'
import List from './components/list-theme'
import Logo from './components/logo-theme'
import Modal from './components/modal-theme'
import Nav from './components/nav-theme'
import Scrollbar from './components/scrollbar-theme'
import Select from './components/select-theme'
import Sidebar from './components/sidebar-theme'
import Statusbar from './components/statusbar-theme'
import Row from './components/row-theme'
import Tabs from './components/tabs-theme'
import Textarea from './components/textarea-theme'
import Toolbar from './components/toolbar-theme'
import Inject from './components/injectGloab-theme'

// shared
import Animation from './base/animation-theme'
import Clearfix from './base/clearfix-theme'
import Depth from './base/depth-theme'
import Global from './base/global-theme'
import Spacing from './base/spacing-theme'
import media from '../media'

export default function base(colors) {
  const spacing = Spacing()
  const animation = Animation()
  const border = Border({ colors })
  const clearfix = Clearfix()
  const depth = Depth()
  const font = Font({ colors })
  const scrollbar = Scrollbar({ colors, spacing })

  const base = {
    animation,
    border,
    clearfix,
    colors,
    depth,
    font,
    media,
    scrollbar,
    spacing,
  }

  Global({
    ...base,
    inject: Inject(base),
  })

  return {
    ...base,
    accordion: Accordion(base),
    button: Button(base),
    buttonSquared: ButtonSquared(base),
    checkbox: Checkbox(base),
    close: Close(base),
    form: Form(base),
    input: Input(base),
    list: List(base),
    logo: Logo(base),
    modal: Modal(base),
    nav: Nav(base),
    select: Select(base),
    sidebar: Sidebar(base),
    statusbar: Statusbar(base),
    row: Row(base),
    tabs: Tabs(base),
    textarea: Textarea(base),
    toolbar: Toolbar(base),
  }
}
