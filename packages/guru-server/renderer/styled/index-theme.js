import dark from './dark-theme'
import light from './light-theme'
import themes from './themes/themes'

export default function theme(name) {
  const theme = {
    default: light,
    dark,
    light,
  }

  return (themes[name] && themes(theme[name])) || themes(theme.default)
}
