import styled from 'styled-components'
import { generateMedia } from 'styled-media-query'

export default generateMedia({
  mega: '2560px',
  huge: '1920px',
  xlarge: '1440px',
  large: '1170px',
  medium: '768px',
  small: '450px',
  xsmall: '320px',

  mega1: '2561px',
  huge1: '1920px',
  xlarge1: '1441px',
  large1: '1171px',
  medium1: '766px',
  small1: '451px',
  xsmall1: '321px',
})
