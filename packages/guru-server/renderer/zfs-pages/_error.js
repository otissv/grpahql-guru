import React from 'react'

function errorMessage(statusCode) {
  switch (statusCode) {
    case 401:
      return 'Access denied'
    case 404:
      return 'Page could not be found'
    case 500:
      return 'Internal server error'
    default:
      return 'An error occurred on server'
  }
}

export default class Error extends React.Component {
  static async getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null
    return { message: errorMessage(statusCode), statusCode }
  }

  render() {
    const { message, statusCode } = this.props
    return (
      <p>{statusCode ? `${this.props.statusCode} - ${message}` : 'An error occurred on client'}</p>
    )
  }
}
