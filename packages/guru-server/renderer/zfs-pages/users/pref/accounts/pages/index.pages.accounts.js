import React from 'react'

export default props => (
  <div>
    <h1>Accounts Pages</h1>
    {props.children}
  </div>
)
