import React from 'react'

export default props => (
  <div>
    <h1>Users</h1>
    {props.children}
  </div>
)
