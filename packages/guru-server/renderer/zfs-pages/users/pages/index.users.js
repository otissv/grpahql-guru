import React from 'react'

export default props => (
  <div>
    <h1>Users Page</h1>
    {props.children}
  </div>
)
