const env = require('get-env')

module.exports = {
  generate: true,
  databases: {
    jsondb: {
      storage: 'jsondb/db.json',
      defaults: { notes: [] },
    },
    mongodb: {
      uri: 'mongodb://127.0.0.1:27017/guru',
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
    // redis: {
    //   uri: '127.0.0.1',
    //   port: 6379
    // },
    // rethinkdb: {
    //   port: 28015,
    //   host: 'localhost',
    //   db: 'test'
    // }
  },
  endpoints: {
    dev: {
      url: process.env.GRAPHQL_DEV,
      headers: {
        Authorization: 'Bearer ${process.envAUTH_TOKEN_ENV}',
      },
      subscription: {
        url: process.env.SUBSCRIPTION_DEV,
        connectionParams: {
          Token: '${process.env.SUBSCRIPTION_TOKEN}',
        },
      },
    },
  },
  apollo: {
    debug: process.env.DEBUG,
    tracing: process.env.DEBUG,
  },
  middleware: {
    logger: {
      accessFile: 'logs/access.log',
      errorFile: 'logs/error.log',
    },
    staticFiles: {
      compression: {
        filter: (req, res) => /json|text|javascript|css/.test(res.getHeader('Content-Type')),
        level: 9,
      },
    },
    security: {
      cors: {
        whitelist: [],
      },
      trustProxy: false,
      helmet: {
        contentSecurityPolicy: {
          directives: {
            defaultSrc: ["'self'"],
            scriptSrc: ["'self'", "'unsafe-inline'", '*.google-analytics.com'],
            styleSrc: ["'self'", "'unsafe-inline'", 'fonts.googleapis.com'],
            imgSrc: ["'self'", '*.google-analytics.com', 'data:'],
            connectSrc: ["'self'"],
            fontSrc: ["'self'", 'data:', 'googleapis.com', 'fonts.gstatic.com'],
            objectSrc: ["'self'"],
            mediaSrc: ["'self'"],
            frameSrc: ["'self'"],
          },
        },
        frameguard: { action: 'sameorigin' },
        dnsPrefetchControl: { allow: false },
        referrerPolicy: { policy: 'same-origin' },
        hsts: {
          maxAge: 7776000000,
          includeSubdomains: true,
        },
      },
      sessions: {
        session: {
          resave: false,
          saveUninitialized: true,
          cookie: { secure: true },
        },
        csrf: {
          cookie: {
            key: '_csrf',
            path: '/',
          },
        },
      },
    },
  },
  plugins: ['guru-database-mongodb', 'guru-accounts'],
}
