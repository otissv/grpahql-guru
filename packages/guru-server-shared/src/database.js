/*
* Database connections
*/
const config = require(`${process.cwd()}/.guru.config`)

export default function databaseConnections({ databases }) {
  if (databases == null) return null
  const db = databases

  return Object.keys(db).reduce((previousObj, currentKey) => {
    if (currentKey === 'default') return previousObj
    const options = config.databases[currentKey]

    return {
      ...previousObj,
      [currentKey]: db[currentKey].connect(options),
    }
  }, {})
}
