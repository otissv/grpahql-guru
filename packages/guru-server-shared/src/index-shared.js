import * as ide from './ide/index-ide'
import errors from './errors'
import main from './main'

export default {
  errors,
  ide,
  main,
}
