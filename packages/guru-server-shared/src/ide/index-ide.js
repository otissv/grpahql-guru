import idePersistedResolverMutation from './ide-persisted/resolver/resolverMutation-ide-persisted'
import idePersistedResolverQuery from './ide-persisted/resolver/resolverQuery-ide-persisted'
import idePersistedSchemaMutation from './ide-persisted/schema/schemaMutation-persisted'
import idePersistedSchemaQuery from './ide-persisted/schema/schemaQuery-ide-persisted'
import idePersistedSchemaType from './ide-persisted/schema/schemaType-ide-persisted'

import ideRequestResolverMutation from './ide-request/resolver/resolverMutation-ide-request'
import ideRequestResolverQuery from './ide-request/resolver/resolverQuery-ide-request'
import ideRequestSchemaMutation from './ide-request/schema/schemaMutation-ide-request'
import ideRequestSchemaQuery from './ide-request/schema/schemaQuery-ide-request'
import ideRequestSchemaType from './ide-request/schema/schemaType-ide-request'

import ideProjectResolverMutation from './ide-project/resolver/resolverMutation-ide-project'
import ideProjectResolverQuery from './ide-project/resolver/resolverQuery-ide-project'
import ideProjectSchemaMutation from './ide-project/schema/schemaMutation-project'
import ideProjectSchemaQuery from './ide-project/schema/schemaQuery-ide-project'
import ideProjectSchemaType from './ide-project/schema/schemaType-ide-project'

export const resolvers = {
  Query: {
    ...idePersistedResolverQuery,
    ...ideProjectResolverQuery,
    ...ideRequestResolverQuery,
  },

  Mutation: {
    ...idePersistedResolverMutation,
    ...ideProjectResolverMutation,
    ...ideRequestResolverMutation,
  },
}

export const schema = `
type Query {
  ${idePersistedSchemaQuery}
  ${ideProjectSchemaQuery}
  ${ideRequestSchemaQuery}
}

type Mutation {
  ${idePersistedSchemaMutation}
  ${ideProjectSchemaMutation}
  ${ideRequestSchemaMutation}
}


${idePersistedSchemaType}
${ideProjectSchemaType}
${ideRequestSchemaType}

type ERROR_ {
  type:    String
  message: String
}

type RESULTS_ {
  result: String
  error:  ERROR_
}

type META {
  UpdatedBy: String
  createdAt: String
  createdBy: String
  updatedAt: String
  count: Int
}
`
