export const QUERY_DIRECTORY = `${process.cwd()}/src/queries`
export const PERSISTED_DIRECTORY = `${process.cwd()}/src/persisted`
export const QUERY_HISTORY_FILE = `${process.cwd()}/src/__history__/query-history.json`
export const PERSISTED_HISTORY_FILE = `${process.cwd()}/src/__history__/persisted-history.json`
export const PROJECT_FILE = `${process.cwd()}/__generated__/projects.json`
