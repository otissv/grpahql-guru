'use strict'
import Bluebird from 'bluebird'
import shell from 'shelljs'
import low from '../../../low'
import { QUERY_DIRECTORY, QUERY_HISTORY_FILE } from '../../constants'

const fs = Bluebird.promisifyAll(require('fs'))
const { mkdir, ls, error } = shell

export default {
  ideRequestFindMany: async (obj, args, context) => {
    const id = (obj && obj.projectId) || args.projectId

    try {
      ls(QUERY_DIRECTORY)
      if (error()) {
        mkdir(QUERY_DIRECTORY)
        process.stdout.write('Created queries directory')
        return Bluebird.resolve([])
      } else {
        const files = await fs.readdirAsync(QUERY_DIRECTORY)

        const queries =
          files.length > 0
            ? await Bluebird.all(
                files.map(async file => {
                  try {
                    if (file.split('-')[0] === id) {
                      const content = await fs.readFileAsync(
                        `${QUERY_DIRECTORY}/${file}`,
                        'utf8'
                      )

                      const contentParse = JSON.parse(content)
                      return contentParse.results
                        ? {
                            ...contentParse,
                            results: JSON.stringify(contentParse.results),
                          }
                        : contentParse
                    } else {
                      return null
                    }
                  } catch (error) {
                    console.error(error)
                  }
                })
              )
            : Bluebird.resolve([])

        return queries.filter(q => q !== null)
      }
    } catch (error) {
      return error
    }
  },

  ideRequestHistoryFind(obj, args, context) {
    const db = low(QUERY_HISTORY_FILE)
    const id = (obj && obj.projectId) || args.projectId

    db.defaults({ history: [] }).write()

    const data = db
      .get('history')
      .find({ id })
      .value()

    return (data && data.requests) || []
  },
}
