'use strict'

import Bluebird from 'bluebird'
import shell from 'shelljs'
import { QUERY_DIRECTORY, QUERY_HISTORY_FILE } from '../../constants'
import { fileExists } from 'guru-server-utils'
import low from '../../../low'

const { rm } = shell
const fs = Bluebird.promisifyAll(require('fs'))

export default {
  ideRequestCollectionClear(obj, args, context) {
    rm('-r', QUERY_DIRECTORY)
  },

  ideRequestCreate: async (obj, args, context) => {
    try {
      const data = {
        ...args,
        created: new Date().toISOString(),
      }

      const id = (obj && obj.projectId) || args.projectId

      return await fileExists(PROJECT_FILE, true)
        .then(exist => {
          if (!exist) {
            low(QUERY_HISTORY_FILE)
              .defaults({ history: [] })
              .write()
          }
        })
        .then(() => {
          return fs
            .writeFileAsync(
              `${id}-${QUERY_DIRECTORY}/${args.id}-query.json`,
              JSON.stringify(data)
            )
            .then(() => {
              return {
                RESULTS_: {
                  result: 'ok',
                },
              }
            })
        })
    } catch (error) {
      console.error(error)
      return error
    }
  },

  ideRequestHistoryRemove(obj, args, context) {
    const db = low(QUERY_HISTORY_FILE)

    db.defaults({ history: [] }).write()
    const { id, projectId } = args

    const project = low
      .get('history')
      .find({ id: args.projectId })
      .value()

    const requests = project.requests.filter(item => item.id !== id)

    db
      .get('history')
      .find({ id: projectId })
      .assign({
        id: projectId,
        requests,
      })
      .write()
  },

  ideRequestHistoryClear(obj, args, context) {
    db.set({ history: [] }).write()

    fs.writeFileAsync(QUERY_HISTORY_FILE, '{\n  "history": [] \n}')
    return []
  },

  ideRequestHistorySave: async (obj, args, context) => {
    const db = low(QUERY_HISTORY_FILE)

    try {
      const data = {
        ...args,
        created: new Date().toISOString(),
      }

      const id = (obj && obj.projectId) || args.projectId

      return await fileExists(PROJECT_FILE, true)
        .then(exist => {
          if (!exist) {
            db.defaults({ history: [] }).write()
          }
        })
        .then(() => {
          db.defaults({ history: [] }).write()

          const project = db
            .get('history')
            .find({ id: args.projectId })
            .value()

          project.requests.push(args)

          db
            .get('history')
            .find({ id: args.projectId })
            .assign(project)
            .write()
        })
    } catch (error) {
      console.error(error)
      return error
    }
  },
}
