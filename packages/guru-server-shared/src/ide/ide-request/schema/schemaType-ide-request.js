export default `
type IdeRequest {
  id:          String
  projectId:   String
  collection:  String
  description: String
  endpoint:    String
  name:        String
  query:       String
  created:     String
  updated:     String
  variables:   String
  results:     String
  RESULTS_:    RESULTS_
}
`
