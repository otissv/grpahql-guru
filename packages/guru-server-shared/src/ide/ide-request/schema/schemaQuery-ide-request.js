export default `
  ideRequestFindMany (projectId: String): [IdeRequest]
  ideRequestHistoryFind (projectId: String): [IdeRequest]
`
