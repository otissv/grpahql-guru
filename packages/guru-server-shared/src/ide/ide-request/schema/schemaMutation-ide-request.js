export default `
  ideRequestCollectionClear: IdeRequest

  ideRequestCreate (
    id:          String
    collection:  String
    created:     String
    description: String
    endpoint:    String
    name:        String
    projectId:   String
    query:       String
    results:     String
    updated:     String
    variables:   String
  ): IdeRequest

  ideRequestHistoryClear: [IdeRequest]
  
  ideRequestHistorySave (
    id:        String
    projectId: String
    endpoint:  String
    name:      String
    query:     String
    results:   String
    variables: String
  ): IdeRequest

  ideRequestHistoryRemove(
    id:        String
    projectId: String
  ): IdeRequest`
