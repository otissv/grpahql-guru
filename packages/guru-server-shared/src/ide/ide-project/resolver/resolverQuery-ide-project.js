'use strict'

import low from '../../../low'
import { PROJECT_FILE } from '../../constants'
import { fileExists } from 'guru-server-utils'

export default {
  ideProjectFindMany: async (obj, args, context) => {
    try {
      return await fileExists(PROJECT_FILE, true)
        .then(exist => {
          if (!exist) {
            low(PROJECT_FILE)
              .defaults({ projects: [] })
              .write()
          }
        })
        .then(() => {
          const result = low(PROJECT_FILE)
            .get('projects')
            .value()

          return result
        })
    } catch (error) {
      return Promise.reject(error)
    }
  },

  ideProjectById: async (obj, args, context) => {
    try {
      const id = (obj && obj.id) || args.id

      return await fileExists(PROJECT_FILE, true).then(() => {
        return low(PROJECT_FILE)
          .get('projects')
          .find({ id })
          .value()
      })
    } catch (error) {
      return Promise.reject(error)
    }
  },
}
