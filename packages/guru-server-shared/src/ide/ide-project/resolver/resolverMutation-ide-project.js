'use strict'

import low from '../../../low'
import { PROJECT_FILE } from '../../constants'
import { fileExists } from 'guru-server-utils'

export default {
  ideProjectCreate: async (obj, args, context) => {
    try {
      const tmpObj = obj || args
      const id = (obj && obj.id) || args.id

      const data = {
        ...args,
        created: new Date().toISOString(),
      }

      return await fileExists(PROJECT_FILE, true)
        .then(exist => {
          if (!exist) {
            low(PROJECT_FILE)
              .defaults({ projects: [] })
              .write()
          }
        })
        .then(() => {
          return low(PROJECT_FILE)
            .get('projects')
            .push(tmpObj)
            .write()
        })
    } catch (error) {
      console.error(error)
      return error
    }
  },

  ideProjectRemove: async (obj, args, context) => {},

  ideProjectClear: async (obj, args, context) => {
    try {
      return await fileExists(PROJECT_FILE, true).then(() => {
        low(PROJECT_FILE)
          .set({ projects: [] })
          .write()
        return []
      })
    } catch (error) {
      console.error(error)
      return error
    }
  },
}
