export default `
  ideProjectCreate (
    id:       String
    name:     String
    headers:  String
    endpoint:  String
  ): IdeProject

  ideProjectRemove: IdeProject

  ideProjectClear: [IdeProject]
`
