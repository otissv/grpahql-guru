'use strict'
import Bluebird from 'bluebird'
import shell from 'shelljs'
import low from '../../../low'
import { fileExists } from 'guru-server-utils'
import { PERSISTED_DIRECTORY, PERSISTED_HISTORY_FILE } from '../../constants'

const fs = Bluebird.promisifyAll(require('fs'))
const { mkdir, ls, error } = shell

export default {
  idePersistedFindMany: async (obj, args, context) => {
    try {
      ls(PERSISTED_DIRECTORY)

      if (error()) {
        mkdir(PERSISTED_DIRECTORY)
        process.stdout.write('Created queries directory')
        return Bluebird.resolve([])
      } else {
        const files = await fs.readdirAsync(PERSISTED_DIRECTORY)

        const queries =
          files.length > 0
            ? await Bluebird.all(
                files.map(async file => {
                  try {
                    const content = await fs.readFileAsync(
                      `${PERSISTED_DIRECTORY}/${file}`,
                      'utf8'
                    )

                    const contentParse = JSON.parse(content)
                    return contentParse.results
                      ? {
                          ...contentParse,
                          results: JSON.stringify(contentParse.results),
                        }
                      : contentParse
                  } catch (error) {
                    console.error(error)
                  }
                })
              )
            : Bluebird.resolve([])
        return queries
      }
    } catch (error) {
      return Promise.reject(error)
    }
  },

  idePersistedHistoryFindMany: async (obj, args, context) => {
    try {
      return await fileExists(PERSISTED_HISTORY_FILE, true)
        .then(exist => {
          if (!exist) {
            return low(PERSISTED_HISTORY_FILE)
              .defaults({ history: [] })
              .write()
          }
        })
        .then(() => {
          return low(PERSISTED_HISTORY_FILE)
            .get('projects')
            .value()
        })
    } catch (error) {
      return Promise.reject(error)
    }
  },
}
