'use strict'

import Bluebird from 'bluebird'
import shell from 'shelljs'
import low from '../../../low'
import { PERSISTED_DIRECTORY, PERSISTED_HISTORY_FILE } from '../../constants'

const { rm } = shell
const fs = Bluebird.promisifyAll(require('fs'))

export default {
  idePersistedCollectionClear(obj, args, context) {
    rm('-r', PERSISTED_DIRECTORY)
  },

  idePersistedCreate(obj, args, context) {
    const data = {
      ...args,
      created: new Date().toISOString(),
    }

    return fs
      .writeFileAsync(
        `${PERSISTED_DIRECTORY}/${args.id}-persisted.json`,
        JSON.stringify(data)
      )
      .then(() => {
        return {
          RESULTS_: {
            result: 'ok',
          },
        }
      })
      .catch(error => {
        console.error(error)

        return {
          RESULTS_: {
            result: 'ok',
            error: {
              type: 'QUERY_SAVE_ERROR',
              message: 'Persisted did not save',
            },
          },
        }
      })
  },

  idePersistedRemove(obj, args, context) {},

  idePersistedHistoryClear(obj, args, context) {
    low(PERSISTED_HISTORY_FILE)
      .set({ history: [] })
      .write()

    fs.writeFileAsync(PERSISTED_HISTORY_FILE, '{\n  "history": [] \n}')
    return []
  },

  idePersistedHistorySave(obj, args, context) {
    low(PERSISTED_HISTORY_FILE)
      .defaults({ history: [] })
      .write()

    low
      .get('history')
      .push(args)
      .write()
  },
}
