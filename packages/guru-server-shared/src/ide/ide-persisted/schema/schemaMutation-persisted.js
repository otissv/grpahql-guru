export default `
  idePersistedCollectionClear: IdePersisted

  idePersistedCreate (
    id:          String
    collection:  String
    created:     String
    description: String
    endpoint:    String
    name:        String
    projectId:   String
    query:       String
    results:     String
    updated:     String
    variables:   String
  ): IdePersisted

  idePersistedRemove: IdePersisted

  idePersistedHistoryClear: [IdePersisted]
  
  idePersistedHistorySave (
    id:        String
    projectId: String
    endpoint:  String
    name:      String
    query:     String
    results:   String
    variables: String
  ): IdePersisted  `
