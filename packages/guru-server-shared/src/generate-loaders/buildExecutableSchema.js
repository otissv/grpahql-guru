import { makeExecutableSchema } from 'graphql-tools'
import Bluebird from 'bluebird'
const fs = Bluebird.promisifyAll(require('fs'))

function buildExecutableSchema({ schema, resolvers }) {
  try {
    return new Promise(async (resolve, reject) => {
      const executableSchema = makeExecutableSchema({
        typeDefs: schema.ast,
        resolvers,
      })
      await fs
        .writeFileAsync(
          `${process.cwd()}/__generated__/executableSchema`,
          JSON.stringify(executableSchema, null, 2)
        )
        .then(() => process.stdout.write('Created executableSchema' + '\n'))
        .catch(error => console.error(error))
    })
  } catch (error) {
    console.error(error)
  }
}

export default buildExecutableSchema
