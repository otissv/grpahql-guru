/*
* Main app 
*/
import next from 'next'
import * as mobx from 'mobx-react'
import loader from 'guru-loaders'
import databaseConnections from './database'
import buildExecutableSchema from './generate-loaders/buildExecutableSchema'

const dev = process.env.NODE_ENV !== 'prod'
const config = require(`${process.cwd()}/.guru.config`)

if (dev) {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
}

export default (async function main({ app, routes, middleware, options }) {
  try {
    const {
      databaseLoader,
      modelLoader,
      middlewareLoader,
      jsonLoader,
      resolverLoader,
      routeLoader,
      schemaLoader,
    } = await loader()

    // Next.js
    const nextApp = next({
      dev,
      dir: './renderer',
    })

    const handle = nextApp.getRequestHandler()

    mobx.useStaticRendering(true)
    nextApp.prepare()

    await nextApp.prepare().then(() => {
      const render = nextApp.render

      // build executable schema
      buildExecutableSchema({
        schema: schemaLoader,
        resolvers: resolverLoader.resolvers,
      }).catch(error => console.error(error))

      // load resolvers
      const resolvers = resolverLoader.resolvers
      const connectors = resolverLoader.connectors

      const apollo = config.apollo || {}
      const configContext = config.context && config.context(app)

      // create context
      const _context = {
        connectors,
        databases: databaseConnections({
          databases: databaseLoader,
          config: app.locals.database,
        }),
        json: jsonLoader,
        locals: app.locals,
        models: modelLoader,
        schema: schemaLoader,
        ...apollo,
      }

      const context = {
        ..._context,
        ...configContext,
      }

      // load middleware
      middleware({
        app,
        context,
        middlewareLoader: () => middlewareLoader.forEach(m => m({ app, context })),
      })

      // load routes
      const opts = apollo
      delete opts.context

      const routeParams = {
        app,
        context,
        resolvers,
        handle,
        render,
        schema: schemaLoader,
        options: opts,
      }

      routes({
        ...routeParams,
        routes: () => routeLoader.forEach(route => route(routeParams)),
      })
    })
  } catch (error) {
    console.error(error)
  }
})
