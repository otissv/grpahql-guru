import lowDB from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'

export default function low(filePath) {
  const adapter = new FileSync(filePath)
  return lowDB(adapter)
}
