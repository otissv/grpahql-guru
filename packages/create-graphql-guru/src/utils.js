import Bluebird from 'bluebird'
import fs from 'fs'
import shell from 'shelljs'
import glob from 'glob-promise'

Bluebird.promisifyAll(fs)
const { mkdir } = shell
const serverDir = `${process.cwd()}/server`
const extensionsDir = `${process.cwd()}/extensions`
const genRoot = `${process.cwd()}/__generated__`

export function capitalize(str) {
  const firstCharUpperCase = str.charAt(0).toUpperCase()
  return `${firstCharUpperCase}${str.substr(1, str.length - 1)}`
}

export async function createGeneratedText(dbFiles) {
  const data = dbFiles.reduce(
    (previous, file, index) => ({
      imports: `${previous.imports}
import module${index} from '${file}';`,
      exports: [...previous.exports, `module${index}`],
    }),
    { imports: '', exports: '' }
  )

  return `${data.imports}

export default [${data.exports.toString().replace(/,/g, ', ')}];
`.replace('\n', '')
}

export function dirExists(dirPath, write) {
  return new Promise((resolve, reject) => {
    fs.exists(dirPath, resolve)
  })
    .then(exists => {
      if (!exists && write) {
        return fs.mkdir(dirPath, error => {
          if (error) return error
        })
      }
    })
    .catch(error => {
      console.error(error)
      return error
    })
}

export async function generateText(dbFiles) {
  try {
    return await createGeneratedText(dbFiles)
  } catch (error) {
    return error
  }
}

export async function getFilePaths({ main, extension }) {
  try {
    const configPath = await globPathsAsync(main)
    const pluginsPath = await globPluginsPathsAsync(extension)

    return [...configPath, ...pluginsPath]
  } catch (error) {
    console.error(error)
  }
}

export const globJsModulePathsAsync = fileNamePrefix => {
  return glob(`${serverDir}/modules/**/${fileNamePrefix}*.mjs`).catch(error =>
    process.stdout.write(error)
  )
}

export const globPluginsPathsAsync = fileName => {
  return glob(`${extensionsDir}/**/${fileName}`).catch(error =>
    process.stdout.write(error)
  )
}

export const globGQLPluginsPathsAsync = fileNamePrefix => {
  return glob(`${extensionsDir}/**/${fileNamePrefix}*.graphql`).catch(error =>
    process.stdout.write(error)
  )
}

export const globGQLModulePathsAsync = fileNamePrefix => {
  return glob(`${serverDir}/modules/**/${fileNamePrefix}*.graphql`).catch(
    error => process.stdout.write(error)
  )
}

export const globPathsAsync = fileName => {
  return glob(`${serverDir}/**/${fileName}`).catch(error =>
    process.stdout.write(error)
  )
}

export function loadFilesAsync(paths) {
  return Bluebird.reduce(
    paths,
    async (previous, current) => {
      try {
        const currentFile = await readFile(current)

        return `${previous}\n${currentFile}`
      } catch (error) {
        console.error(error)
      }
    },
    ''
  ).catch(error => console.error(error))
}

export const readFile = async filePath => {
  try {
    return await fs.readFileAsync(filePath, 'utf8')
  } catch (error) {
    process.stdout.write(error)
  }
}

export async function writeGeneratedFile({ data, file, ext }) {
  const filePath = `${genRoot}/${file}.${ext || 'mjs'}`

  return fs
    .writeFileAsync(filePath, data, { flag: 'w' })
    .then(() => {
      process.stdout.write(`Generated ${file} file\n`)
    })
    .catch(error => {
      throw new Error(error)
    })
}
