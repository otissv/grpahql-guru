import program from 'commander'
import {
  createGeneratedText,
  dirExists,
  getFilePaths,
  generateText,
  globGQLPluginsPathsAsync,
  globGQLModulePathsAsync,
  globJsModulePathsAsync,
  loadFilesAsync,
  readFile,
  writeGeneratedFile,
} from './utils'

const genRoot = `${process.cwd()}/__generated__`

async function schemaGenerate(fileName) {
  try {
    // const mutation = await pipeAsync([pathsAsync, loadFilesAsync])('schemaMutation');
    // const query = await pipeAsync([pathsAsync, loadFilesAsync])('schemaQuery');
    // const typeDefinition = await pipeAsync([pathsAsync, loadFilesAsync])('schemaType');

    const configPath = await globGQLModulePathsAsync(`**/${fileName}`)
    const extensions = await globGQLPluginsPathsAsync(`${fileName}`)

    return [...configPath, ...extensions]
  } catch (error) {
    console.error(error)
  }
}

dirExists(genRoot, true)
  .then(() => {
    // Env
    getFilePaths({
      main: '**/.env.mjs',
      extension: '.env.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'env' }))
      .catch(error => console.error(error))

    // Databases
    getFilePaths({
      main: 'core/**/databases-index.mjs',
      extension: 'databases-index.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'databases' }))
      .catch(error => console.error(error))

    // Middleware
    getFilePaths({
      main: 'core/**/middleware-index.mjs',
      extension: 'middleware-index.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'middleware' }))
      .catch(error => console.error(error))

    // Model
    getFilePaths({
      main: '**/model.mjs',
      extension: 'model.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'model' }))
      .catch(error => console.error(error))

    // Routes
    getFilePaths({
      main: 'core/**/routes-index.mjs',
      extension: 'routes-index.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'routes' }))
      .catch(error => console.error(error))

    // Validation
    getFilePaths({
      main: '**/validation.mjs',
      extension: 'validation.mjs',
    })
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'validation' }))
      .catch(error => console.error(error))

    // Resolver Hooks
    globJsModulePathsAsync('resolverHook')
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'resolverHook' }))
      .catch(error => console.error(error))

    // Resolver Hook Mutation
    globJsModulePathsAsync('resolverHookMutation')
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'resolverHookMutation' }))
      .catch(error => console.error(error))

    // Resolver Hook Query
    globJsModulePathsAsync('resolverHookQuery')
      .then(dbFiles => generateText(dbFiles))
      .then(data => writeGeneratedFile({ data, file: 'resolverHookQuery' }))
      .catch(error => console.error(error))

    // // Schema Mutation
    // schemaGenerate('schemaMutation')
    //   .then(async dbFiles => loadFilesAsync(dbFiles))
    //   .then(data => writeGeneratedFile({ data, file: 'schemaMutation' }))
    //   .catch(error => console.error(error));

    // // Schema Query
    // schemaGenerate('schemaQuery')
    //   .then(async dbFiles => loadFilesAsync(dbFiles))
    //   .then(data => writeGeneratedFile({ data, file: 'schemaQuery' }))
    //   .catch(error => console.error(error));

    // // Schema Type
    // schemaGenerate('schemaType')
    //   .then(async dbFiles => loadFilesAsync(dbFiles))
    //   .then(data => writeGeneratedFile({ data, file: 'schemaType', ext: 'graphql' }))
    //   .catch(error => console.error(error));
  })
  .catch(error => console.error(error))
