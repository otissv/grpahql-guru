/*
* Schema routes
*/
'use strict'

export default function schemaRoute({ app, schema }) {
  app.route('/schema').get((request, response) => {
    return response.json({ schema })
  })
}
