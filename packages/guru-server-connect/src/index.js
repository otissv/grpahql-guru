/*
* Application Server
*/

import express from 'express'
import shared from 'guru-server-shared'
import routes from './routes'
import middleware from './middleware/index-middleware'

const main = shared.main

export default (async function server() {
  try {
    const app = express()
    const os = require('os')
    const networkInterfaces = os.networkInterfaces()

    await main({ app, routes, middleware })

    app.set('port', process.env.PORT || app.locals.port)

    let externalAddress

    if (networkInterfaces.en0 === true) {
      externalAddress = networkInterfaces.en0[0].address
    } else if (networkInterfaces.wlp4s0) {
      externalAddress = networkInterfaces.wlp4s0[0].address
    }

    const server = app.listen(app.get('port'), () => {
      const PORT = server.address().port

      process.stdout.write(`
Guru Express server started in ${app.get('env')} mode.
 - IDE address           = http://localhost:${PORT}
 - GraphQL endpoint:     = http://localhost:${PORT}/graphql
${
        externalAddress
          ? ' - External IDE address  = ' +
            'http://' +
            externalAddress +
            ':' +
            PORT
          : ''
      }
${
        externalAddress
          ? ' - External IDE endpoint = ' +
            'http://' +
            externalAddress +
            ':' +
            PORT +
            '/graphql'
          : ''
      }

`)
    })
  } catch (error) {
    console.error(error)
  }
})
