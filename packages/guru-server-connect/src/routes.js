/*
* Application routes
*/

import coreRoutes from './routes/index-route'

function routes({ app, context, schema, resolvers, routes }) {
  const params = {
    app,
    context,
    resolvers,
    schema,
  }

  routes(params)
  coreRoutes(params)
  // 404 page
}

export default routes
