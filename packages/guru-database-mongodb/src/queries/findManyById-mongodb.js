import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'

import { errors } from 'guru-server-utils'
import mongod from 'mongodb'
import { query } from '../execute-mongodb'

const { ObjectId } = mongod

export default function findManyById() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findManyById'
  const beforeMethod = initializeArgs.beforeFindManyById
  const afterMethod = initializeArgs.afterFindById

  return function(target) {
    const _target = new target()

    async function findManyByIdFn(obj, args, context, info) {
      try {
        const _target = new target()

        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases, options } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return Promise.reject(errors.noInputArray({ methodName: name, moduleName: target.name }))
        }

        const mongodb = databases[dbName]
        const { cursor, projection, filter } = options || {}
        const _obj = args.query || obj

        if (_obj == null) return null

        return mongodb.then(
          db =>
            new Promise((resolve, reject) =>
              query({
                db,
                col: table,
                cursor,
                method: 'find',
                projection,
                args: {
                  _id: {
                    $in: _obj.id.map(id => ObjectId(id)),
                  },
                },
              }).toArray((error, docs) => {
                if (error) {
                  return Promise.reject(
                    errors.resolverQuery({
                      args: _obj,
                      error,
                      moduleName: target.name,
                    })
                  )
                }

                const result = docs.map(doc => {
                  if (doc._id) {
                    const _doc = { ...doc }
                    delete _doc._id
                    return { ..._doc, id: doc._id }
                  }
                  return doc
                })

                return resolve(result)
              })
            )
        )
      } catch (error) {
        return Promise.reject(
          errors.resolverQuery({
            args: _obj,
            error,
            moduleName: target.name,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return findManyByIdFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
