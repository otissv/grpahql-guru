import { after, before, resolverPromise } from '../utils-mongodb'

export default function resolve() {
  const name = 'resolve'

  return function(target) {
    const _target = new target()

    Object.defineProperty(target.prototype, name, {
      value: function _resolve() {
        // return Array.isArray(obj) ? loaderMany.load(obj) : loader.load(obj);
        return _target.findById(...arguments)
      },
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
