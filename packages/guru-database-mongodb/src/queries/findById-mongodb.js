import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'

import { errors } from 'guru-server-utils'
import { query } from '../execute-mongodb'

export default function findById() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findById'
  const beforeMethod = initializeArgs.beforeFindById
  const afterMethod = initializeArgs.afterFindById

  return function(target) {
    const _target = new target()

    async function findByIdFn(obj, args, context, info) {
      try {
        const _target = new target()

        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases, options } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return Promise.reject(errors.noInput({ methodName: name, moduleName: target.name }))
        }

        const mongodb = databases[dbName]
        const _obj = args.query || obj
        const { cursor, projection, filter } = options || {}
        const tmpArgs = { ..._obj }
        delete tmpArgs.options

        if (_obj == null || _obj.id == null) return null
        return mongodb
          .then(db =>
            query({
              db,
              col: table,
              cursor,
              method: 'findOne',
              projection,
              args: tmpArgs,
            })
          )
          .then(doc => {
            if (doc == null) return doc
            const _doc = { ...doc, id: doc._id }
            delete _doc._id
            return _doc
          })
      } catch (error) {
        return Promise.reject(
          errors.resolverQuery({
            args: _obj,
            error,
            moduleName: target.name,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return findByIdFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
