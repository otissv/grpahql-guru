import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'

import { errors } from 'guru-server-utils'
import { query } from '../execute-mongodb'

export default function findOne() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findOne'
  const beforeMethod = initializeArgs.beforeFindOne
  const afterMethod = initializeArgs.afterFindOne

  return function(target) {
    const _target = new target()

    async function findOneFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases, options } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return Promise.reject(errors.noInput({ methodName: name, moduleName: target.name }))
        }

        const mongodb = databases[dbName]
        const _obj = args.query || obj
        const { cursor, projection, filter } = options || {}
        const tmpArgs = { ..._obj }
        delete tmpArgs.options

        return await mongodb.then(async db => {
          try {
            const doc = await query({
              args: tmpArgs,
              col: table,
              cursor,
              db,
              method: 'findOne',
              projection,
            })

            if (!doc) return doc

            const _doc = {
              ...doc,
              id: doc._id,
            }

            delete _doc._id
            return _doc
          } catch (error) {
            return Promise.reject(
              errors.resolverQuery({
                args: _obj,
                error,
                moduleName: target.name,
              })
            )
          }
        })
      } catch (error) {
        return Promise.reject(
          errors.resolverQuery({
            args: _obj,
            error,
            moduleName: target.name,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            return findOneFn(...arguments[0])
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
