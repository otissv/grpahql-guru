import create from './mutations/create-mongodb'
import createMany from './mutations/createMany-mongodb'
import drop from './mutations/drop-mongodb'
import findAndModify from './mutations/findAndModify-mongodb'
import findOneAndDelete from './mutations/findOneAndDelete-mongodb'
import findOneAndReplace from './mutations/findOneAndReplace-mongodb'
import findOneAndUpdate from './mutations/findOneAndUpdate-mongodb'
import nested from './mutations/nested-mongodb'
import remove from './mutations/remove-mongodb'
import removeById from './mutations/removeById-mongodb'
import save from './mutations/save-mongodb'
import saveMany from './mutations/saveMany-mongodb'
import update from './mutations/update-mongodb'
import updateMany from './mutations/updateMany-mongodb'
import updateManyById from './mutations/updateManyById-mongodb'

export default function mutation() {
  const _arguments = arguments

  return function(target) {
    create(..._arguments)(target)
    createMany(..._arguments)(target)
    drop(..._arguments)(target)
    findAndModify(..._arguments)(target)
    nested(..._arguments)(target)
    remove(..._arguments)(target)
    removeById(..._arguments)(target)
    save(..._arguments)(target)
    saveMany(..._arguments)(target)
    update(..._arguments)(target)
    updateMany(..._arguments)(target)
    updateManyById(..._arguments)(target)
    findOneAndDelete(..._arguments)(target)
    findOneAndReplace(..._arguments)(target)
    findOneAndUpdate(..._arguments)(target)
  }
}
