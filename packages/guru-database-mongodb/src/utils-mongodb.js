import { errors, isObjectEmpty } from 'guru-server-utils'

import camelCase from 'lodash/fp/camelCase'
import mongodb from 'mongodb'

const { ObjectId } = mongodb

export function after() {
  const ownArgs = arguments[0][0]

  return async function() {
    const afterFn = arguments[0].after

    try {
      if (afterFn) {
        return await afterFn({ data: ownArgs, params: arguments[0] })
      } else {
        return ownArgs
      }
    } catch (error) {
      return error
    }
  }
}

export function before() {
  const ownArgs = arguments[0][0]

  return async function() {
    const beforeFn = arguments[0].before

    try {
      if (beforeFn) {
        return await beforeFn(...ownArgs)
      } else {
        return ownArgs
      }
    } catch (error) {
      return error
    }
  }
}

export function gqlQueryToMongoQuery(query) {
  function transformObj(obj) {
    return Object.keys(obj).reduce((previous, key) => {
      let _key = key === key.toUpperCase() ? `$${camelCase(key)}` : key

      return {
        ...previous,
        [_key]: typeof obj[key] === 'object' ? gqlToMongo(obj[key]) : obj[key],
      }
    }, {})
  }

  if (Array.isArray(query)) {
    return query.map(obj => transformObj(obj))
  } else {
    return transformObj(query)
  }
}

export function queryObjectId(args = {}) {
  if (isObjectEmpty(args)) return {}

  let obj = Object.keys(args).reduce((previous, key) => {
    if (key[0] === '_') {
      return { ...previous, [key]: args[key] }
    }

    if (args.id && args.id['$in']) {
      return { ...previous, _id: { $in: args.id['$in' || 'IN'].map(item => ObjectId(item)) } }
    }

    if (Array.isArray(args.id)) {
      return { ...previous, _id: { $in: args.id.map(item => ObjectId(item)) } }
    }

    if (Array.isArray(args[key])) {
      return { ...previous, [key]: args[key].map(item => queryObjectId(item)) }
    }

    let tmpArg = key === 'id' ? { _id: ObjectId(args[key]) } : { [key]: args[key] }

    return {
      ...previous,
      ...tmpArg,
    }
  }, {})

  return obj
}

export async function resolverPromise(obj, args, context, info) {
  return Promise.resolve(arguments)
}

export async function resolverQueryArgs({ args, dbName, table, collection }) {
  const _args = Object.keys(args).length < 1 ? {} : args

  return {
    dbName: _args.dbName || dbName || 'mongodb',
    table: _args.table || _args.collection || table || collection,
  }
}
