import { after, before, queryObjectId, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

export default function findOneAndReplace() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findOneAndReplace'
  const beforeMethod = initializeArgs.beforeFindOneAndReplace
  const afterMethod = initializeArgs.afterFindOneAndReplace

  return function(target) {
    const _target = new target()

    async function findOneAndReplaceFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return errors.noInput({ methodName: name, moduleName: target.name })
        }

        const mongodb = databases[dbName]
        let _obj = args || obj
        const { query, options } = _obj
        const tmpArgs = { ...query }
        delete tmpArgs.id

        return new Promise((resolve, reject) => {})
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return findOneAndReplaceFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
