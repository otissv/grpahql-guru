import { after, before, resolverPromise } from '../utils-mongodb'

export default function save() {
  const initializeArgs = arguments[0] || {}
  const name = 'save'

  return function(target) {
    const _target = new target()

    Object.defineProperty(target.prototype, name, {
      value: function _save(obj, args, context, info) {
        return this.create(obj, args, context, info)
      },
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
