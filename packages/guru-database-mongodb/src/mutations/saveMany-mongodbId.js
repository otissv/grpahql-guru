import { after, before, resolverPromise } from '../utils-mongodb'

export default function saveMany() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'saveMany'

  return function(target) {
    const _target = new target()

    Object.defineProperty(target.prototype, name, {
      value: function _saveMany() {
        return this.updateMany(obj, args, context, info)
      },
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
