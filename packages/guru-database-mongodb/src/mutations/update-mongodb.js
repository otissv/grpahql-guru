import { after, before, queryObjectId, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

import { mutation } from '../execute-mongodb'

export default function update() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'update'
  const beforeMethod = initializeArgs.beforeUpdate
  const afterMethod = initializeArgs.afterUpdate

  return function(target) {
    const _target = new target()

    async function updateFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return errors.noInput({ methodName: name, moduleName: target.name })
        }

        const mongodb = databases[dbName]
        let _obj = (args && { ...args }) || (obj && { ...obj })
        const { update, options, query } = _obj
        const _update = { ...update }
        _update.meta = meta({ userId: context.userId })

        return mongodb
          .then(db =>
            mutation({
              args: query,
              col: table,
              db,
              options,
              update: { $set: _update },
              method: 'update',
            }).then(response => {
              if (response.result.n === 0)
                return Promise.reject(
                  errors.resolverMutation({
                    id: query && query.id,
                    moduleName: target.name,
                    message: `${target.name}Update resolver failed to update.`,
                    n: 0,
                  })
                )

              return [
                {
                  RESULTS: {
                    id: query && query.id,
                    module: target.name,
                    result: response.result.n > 0 ? 'ok' : 'failed.',
                    n: response.result.n,
                    error: response.error,
                  },
                },
              ]
            })
          )
          .catch(error =>
            Promise.reject(
              errors.resolverMutation({
                id: query && query.id,
                error,
                moduleName: target.name,
                n: 0,
              })
            )
          )
      } catch (error) {
        const _obj = (args && { ...args }) || (obj && { ...obj })
        const id = _obj.query.id

        return Promise.reject(
          errors.resolverMutation({
            id: query && query.id,
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return updateFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
