import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

export default function drop() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'drop'
  const beforeMethod = initializeArgs.beforeDrop
  const afterMethod = initializeArgs.afterDrop

  return function(target) {
    const _target = new target()

    async function dropFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        const mongodb = databases[dbName]

        return mongodb.then(db =>
          db
            .collection(table)
            .drop()
            .then(response => [
              {
                RESULTS: {
                  module: target.name,
                  result: response ? 'ok' : 'failed',
                },
              },
            ])
        )
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return dropFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
