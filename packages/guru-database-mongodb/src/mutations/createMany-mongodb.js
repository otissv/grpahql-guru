import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

export default function createMany() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'createMany'
  const beforeMethod = initializeArgs.beforeCreateMany
  const afterMethod = initializeArgs.afterCreateMany

  return function(target) {
    const _target = new target()

    async function createManyFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return errors.noInputArray({
            methodName: name,
            moduleName: target.name,
          })
        }

        const mongodb = databases[dbName]
        let _obj = args || obj
        const { query, options } = _obj

        const list = _obj.query

        const tmpArgs = query.map(item => {
          const data = {
            ...item,
            _id: item.id,
            meta: meta({ userId: context.userId, create: true }),
          }

          delete item.id
          return data
        })

        return new Promise((resolve, reject) =>
          mongodb.then(db =>
            db
              .collection(table)
              .insertMany(tmpArgs, options)
              .then(response => {
                resolve(
                  response.insertedIds.map(id => ({
                    RESULTS: {
                      id,
                      module: _target.constructor.name,
                      result: response.result.n > 0 ? 'ok' : 'failed.',
                      n: response.result.n,
                      error: response.error,
                    },
                  }))
                )
              })
          )
        )
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return createManyFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
