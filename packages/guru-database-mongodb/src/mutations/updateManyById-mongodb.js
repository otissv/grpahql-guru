import { after, before, queryObjectId, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

import { mutation } from '../execute-mongodb'

export default function updateManyById() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'updateManyById'
  const beforeMethod = initializeArgs.beforeUpDateMany
  const afterMethod = initializeArgs.afterUpDateMany

  return function(target) {
    const _target = new target()

    async function updateManyByIdFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases } = context

        if (!databases || !databases[dbName])
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return Promise.reject(errors.noInputArray({ methodName: name, moduleName: target.name }))
        }

        const mongodb = databases[dbName]
        let _obj = (args && { ...args }) || (obj && { ...obj })
        const { query, update, options } = _obj
        const _update = { ...update }
        _update.meta = meta({ userId: context.userId })

        return mongodb.then(db =>
          mutation({
            args: query,
            col: table,
            db,
            options,
            update: { $set: _update },
            method: 'updateMany',
          }).then(response => {
            if (response.result.n === 0) return Promise.reject()

            return [
              {
                RESULTS: {
                  module: target.name,
                  result: response.result.n > 0 ? 'ok' : 'failed.',
                  n: response.result.n,
                  error: response.error,
                },
              },
            ]
          })
        )
      } catch (error) {
        const _obj = (args && { ...args }) || (obj && { ...obj })
        const id = _obj.query.id

        return Promise.reject(
          errors.resolverMutation({
            id,
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return updateManyByIdFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
