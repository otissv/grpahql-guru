import { after, before, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

import mongod from 'mongodb'
import { mutation } from '../execute-mongodb'

const { ObjectId } = mongod

export default function removeById() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'removeById'
  const beforeMethod = initializeArgs.beforeRemoveMany
  const afterMethod = initializeArgs.afterRemoveMany

  return function(target) {
    const _target = new target()

    async function removeByIdFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        if (!table) {
          return errors.collectionNameError({ module: target.name })
        }

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return errors.noInputArray({
            methodName: name,
            moduleName: target.name,
          })
        }

        const mongodb = databases[dbName]
        let _obj = (args && { ...args }) || (obj && { ...obj })
        const { options, query } = _obj

        return mongodb.then(db =>
          mutation({
            args: {
              id: {
                $in: query.map(item => item.id),
              },
            },
            db,
            col: table,
            method: 'remove',
            options,
          }).then(response => [
            {
              RESULTS: {
                module: _target.constructor.name,
                result: response.result.n > 0 ? 'ok' : 'failed.',
                n: response.result.n,
                error: response.error,
              },
            },
          ])
        )
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return removeByIdFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
