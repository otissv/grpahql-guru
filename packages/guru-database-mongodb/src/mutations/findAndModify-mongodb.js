import { after, before, queryObjectId, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

import mongod from 'mongodb'

const { ObjectId } = mongod

export default function findAndModify() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findAndModify'
  const beforeMethod = initializeArgs.beforeFindAndModify
  const afterMethod = initializeArgs.afterFindAndModify

  return function(target) {
    const _target = new target()

    async function findAndModifyFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        const mongodb = databases[dbName]

        const {
          query,
          sort,
          update,
          remove,
          fields,
          upsert,
          bypassDocumentValidation,
          writeConcern,
          maxTimeMS,
          collation,
          unset,
        } = args.query

        const _query = { ...query }

        if (_query.id) {
          _query._id = ObjectId(query.id)
          delete _query.id
        }

        const _sort = { ...sort }

        if (_sort && _sort.id) {
          _sort._id = sort.id
          delete _sort.id
        } else {
          _sort._id = 1
        }

        const _update = unset
          ? {
              $unset: {
                ...update,
                meta: meta({ userId: context.userId, create: true }),
              },
            }
          : { $set: update }

        return mongodb.then(db =>
          db
            .collection(table)
            .findAndModify(_query, _sort, update ? _update : null, {
              remove,
              new: remove ? null : true,
              fields,
              upsert,
              bypassDocumentValidation,
              writeConcern,
              maxTimeMS,
              collation,
            })
            .then(response => {
              if (response.value === null) {
                return Promise.reject(response)
              }

              // const value = { ...response.value, id: response.value._id };
              // delete value._id;

              // return value;

              resolve({
                RESULTS: {
                  id: value._id,
                  module: _target.constructor.name,
                  result: response.result.n > 0 ? 'ok' : 'failed.',
                  n: response.result.n,
                  error: response.error,
                },
              })
            })
        )
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            id: value._id,
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return findAndModifyFn(...arguments[0])
          })
          .then(function() {
            if (afterMethod) {
              return after(arguments)({
                ...initializeArgs,
                after: afterMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
