import { after, before, resolverPromise } from '../utils-mongodb'

export default function nested() {
  const name = 'nested'

  return function(target) {
    const _target = new target()

    Object.defineProperty(target.prototype, name, {
      value: function _nested() {
        return Array.isArray(arguments[0])
          ? _target.updateMany(...arguments)
          : _target.update(...arguments)
      },
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
