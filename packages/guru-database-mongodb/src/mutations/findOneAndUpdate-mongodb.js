import { after, before, queryObjectId, resolverPromise, resolverQueryArgs } from '../utils-mongodb'
import { errors, meta } from 'guru-server-utils'

export default function findOneAndUpdate() {
  const initializeArgs = arguments[0] || {}
  const name = initializeArgs.name || 'findOneAndUpdate'
  const beforeMethod = initializeArgs.beforeFindOneAndUpdate
  const afterMethod = initializeArgs.afterFindOneAndUpdate

  return function(target) {
    const _target = new target()

    async function findOneAndUpdateFn(obj, args, context, info) {
      try {
        const { dbName, table } = await resolverQueryArgs({
          args: initializeArgs,
          dbName: _target.dbName,
          table: _target.table,
          collection: _target.collection,
        })

        const { databases } = context

        if (!databases || !databases[dbName]) {
          return Promise.reject(errors.databaseConfiguration({ dbName, moduleName: target.name }))
        }

        if (Object.keys(args).length > 0 && !args.options && !args.query) {
          return errors.noInput({ methodName: name, moduleName: target.name })
        }

        const mongodb = databases[dbName]
        let _obj = args || obj
        const { query, options } = _obj
        const tmpArgs = { ...query }
        delete tmpArgs.id

        if (Object.keys(query).length === 0) {
          return errors.noInput({ methodName: name, moduleName: target.name })
        }

        return new Promise((resolve, reject) => {})
      } catch (error) {
        return Promise.reject(
          errors.resolverMutation({
            error,
            moduleName: target.name,
            n: 0,
          })
        )
      }
    }

    Object.defineProperty(target.prototype, name, {
      value: (obj, args, context, info) =>
        resolverPromise(obj, args, context, info)
          .then(function() {
            return before(arguments)(initializeArgs)
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return findOneAndUpdateFn(...arguments[0])
          })
          .then(function() {
            if (beforeMethod) {
              return before(arguments)({
                ...initializeArgs,
                before: beforeMethod,
              })
            } else {
              return arguments[0]
            }
          })
          .then(function() {
            return after(arguments)(initializeArgs)
          }),
    })

    Object.defineProperty(target.prototype[name], 'name', {
      writable: false,
      enumerable: false,
      value: name,
    })
  }
}
