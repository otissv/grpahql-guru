import camelCase from 'lodash/fp/camelCase'
import { queryObjectId } from './utils-mongodb'

function objToMongodbQueryObj(value) {
  if (typeof value === 'object') {
    return Object.keys(value).reduce((previous, current) => {
      const key = current === current.toUpperCase() ? `$${camelCase(current)}` : current
      return {
        ...previous,
        [key]:
          typeof value[current] === 'object'
            ? objToMongodbQueryObj(value[current])
            : value[current],
      }
    }, {})
  } else {
    return value
  }
}

export function mutation({ args, col, db, method, options, update }) {
  //TODO:  add gqlQueryToMongoQuery
  return db.collection(col)[method](queryObjectId(args), update, options)
}

export function query({ args, col, cursor, db, method, projection }) {
  //TODO:  add gqlQueryToMongoQuery
  const query = db.collection(col)[method](queryObjectId(args), projection)

  let _cursor
  if (!cursor) {
    return query
  } else {
    _cursor = objToMongodbQueryObj(cursor)
  }

  return Object.keys(_cursor).reduce((previous, key) => {
    const value = _cursor[key]

    // close: () => previous[key](),
    // count: () => previous[key](),
    // explain: () => previous[key](),
    // hasNext: () => previous[key](),
    // isClosed
    // next
    // nextObject
    // read: value => previous[key](value),

    // alias methods
    const type = {
      last: value => {
        if (isNaN(value)) {
          const k = Object.keys(value)
          return previous.limit(3).sort({ [k]: -1 })
        } else {
          return previous.limit(value).sort({ $natural: -1 })
        }
      },
      first: value => previous.limit(value),
    }

    return (previous[key] && previous[key](value)) || (type[key] && type[key](value)) || previous
  }, query)
}
