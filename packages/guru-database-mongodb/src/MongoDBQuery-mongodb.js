import aggregate from './queries/aggregate-mongodb'
import connect from './connect-mongodb'
import count from './queries/count-mongodb'
import distinct from './queries/distinct-mongodb'
import findById from './queries/findById-mongodb'
import findMany from './queries/findMany-mongodb'
import findManyById from './queries/findManyById-mongodb'
import findOne from './queries/findOne-mongodb'
import resolve from './queries/resolve-mongodb'

export default function query() {
  const _arguments = arguments.length > 0 ? [...arguments] : [{}]

  return function(target) {
    aggregate(..._arguments)(target)
    count(..._arguments)(target)
    distinct(..._arguments)(target)
    findById(..._arguments)(target)
    findMany(..._arguments)(target)
    findManyById(..._arguments)(target)
    findOne(..._arguments)(target)
    resolve(..._arguments)(target)

    return target
  }
}
