import connect from './connect-mongodb'

export default function connection(options) {
  return function(target) {
    Object.defineProperty(target, 'databases', {
      value: Object.keys(options).reduce(async (previous, key) => {
        try {
          return {
            ...previous,
            [key]: connect(options[key]),
          }
        } catch (error) {
          console.error(error)
        }
      }, {}),
    })
  }
}