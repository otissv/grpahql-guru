import mongodb from 'mongodb'

export default function connect(options) {
  return mongodb.MongoClient.connect(options.uri, { native_parser: true })
}
