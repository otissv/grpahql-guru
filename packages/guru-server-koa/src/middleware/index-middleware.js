import compress from 'koa-compress'
import logger from 'koa-logger'
import koaBody from 'koa-bodyparser'
import cors from 'koa-cors'

export default function middleware({ router }) {
  return function({ app, middlewareLoader }) {
    app.use(cors({ origin: '*' }))
    app.use(logger())
    app.use(koaBody())
    app.use(router.routes())
    app.use(router.allowedMethods())
    app.use(compress())

    middlewareLoader(app)
  }
}
