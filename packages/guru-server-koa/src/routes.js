/*
* Application routes
*/

import coreRoutes from './routes/index-route'

function routes({ router }) {
  return function({ app, context, schema, resolvers, routes }) {
    const params = {
      app,
      context,
      resolvers,
      schema,
      router,
    }

    routes(params)
    coreRoutes(params)
    // 404 page
  }
}

export default routes
