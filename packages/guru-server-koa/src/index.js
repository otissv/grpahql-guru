import Koa from 'koa'
import Router from 'koa-router'
import shared from 'guru-server-shared'
import routes from './routes'
import middleware from './middleware/index-middleware'

export default (async function server() {
  try {
    const app = new Koa()
    const os = require('os')
    const networkInterfaces = os.networkInterfaces()
    const main = shared.main
    const router = new Router()

    await main({
      app,
      middleware: middleware({ router }),
      routes: routes({ router }),
    })
    // routes

    let externalAddress

    if (networkInterfaces.en0 === true) {
      externalAddress = networkInterfaces.en0[0].address
    } else if (networkInterfaces.wlp4s0) {
      externalAddress = networkInterfaces.wlp4s0[0].address
    }

    const PORT = 8000
    app.listen(PORT)

    process.stdout.write(`
Guru Koa server started in ${app.env} mode.
 - IDE address           = http://localhost:${PORT}
 - GraphQL endpoint:     = http://localhost:${PORT}/graphql
${
      externalAddress
        ? ' - External IDE address  = ' +
          'http://' +
          externalAddress +
          ':' +
          PORT
        : ''
    }
${
      externalAddress
        ? ' - External IDE endpoint = ' +
          'http://' +
          externalAddress +
          ':' +
          PORT +
          '/graphql'
        : ''
    }

`)
  } catch (error) {
    console.error(error)
  }
})
