/*
* GraphQL rourte
*/

'use strict'
import { graphqlKoa } from 'graphql-server-koa'
import { makeExecutableSchema } from 'graphql-tools'
import Cache from './cache-route'
import fs from 'fs'
import { mergeStrings } from 'gql-merge'
import chalk from 'chalk'

export default function graphqlRoute({
  app,
  context,
  schema,
  resolvers,
  router,
}) {
  const logger = { log: e => console.error(e) }

  app.use(
    '/graphql',
    graphqlKoa((ctx, next) => {
      return {
        schema: makeExecutableSchema({
          typeDefs: schema.ast,
          resolvers,
          logger,
        }),
        context: {
          ...context,
          cache: new Cache(),
          request: ctx,
        },
        debug: true,
      }
    })
  )

  // app.use(
  //   '/graphql',
  //  z
  //   graphqlExpress(req => {
  //     return {
  // schema: makeExecutableSchema({
  //   typeDefs: schema.ast,
  //   resolvers,
  //   logger
  // }),
  //       context: {
  //         ...context,
  //         cache: new Cache(),
  //         req
  //       }
  //     };
  //   })
  // );
}
