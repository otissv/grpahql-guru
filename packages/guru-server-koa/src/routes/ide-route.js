/*
* GraphQL rourte
*/

'use strict'

import { graphqlKoa } from 'graphql-server-koa'
import { makeExecutableSchema } from 'graphql-tools'
import { parse } from 'graphql'
import shared from 'guru-server-shared'

const { schema, resolvers } = shared.ide

export default function ideRoute({ app, context, router }) {
  const logger = { log: e => console.error(e) }

  app.use('/ide/schema'), (ctx, next) => (ctx.body = parse(schema))

  app.use(
    '/ide',
    graphqlKoa((ctx, next) => {
      return {
        schema: makeExecutableSchema({
          typeDefs: schema,
          resolvers,
          logger,
        }),
        context: {
          request: ctx,
        },
        debug: true,
      }
    })
  )
}
