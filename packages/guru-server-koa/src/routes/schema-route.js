/*
* Schema routes
*/

'use strict'

export default function schemaRoute({ app, schema, router }) {
  app.use('/schema', async (ctx, next) => {
    ctx.body = { schema }
  })
}
