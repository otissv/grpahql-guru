const { pipe, tasks, runPackageTasks } = require('./packages/tasks/tasks')
const glob = require('glob-promise')
const shell = require('shelljs')

glob('packages/*')
  .then(files => files.map(file => file.split('/')[1]))
  .then(files => {
    // Remove non task packages
    const ignore = (file, name) => file !== name
    return files.filter(
      file =>
        ignore(file, 'ide') &&
        ignore(file, 'guru-server') &&
        ignore(file, 'tasks')
    )
  })
  .then(files => {
    const packages = files.reduce(
      (previous, key) => ({
        ...previous,
        [key]: {
          src: `packages/${key}/src`,
          dist: `packages/${key}/lib`,
          silent: true,
        },
      }),
      {}
    )

    const jobs = files.reduce(
      (previous, key) => ({
        ...previous,
        [key]: () => tasks(key, packages[key]),
      }),
      {}
    )

    runPackageTasks({ jobs, packages })

    return files
  })

  .catch(error => {
    throw new Error(error)
  })
