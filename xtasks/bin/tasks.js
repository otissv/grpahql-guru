const tasks = require('../tasks')
const argv = require('minimist')(process.argv)
const help = argv.h || argv.help
const silent = argv.s || argv.silent
const watching = argv.w || argv.watch
const input = argv.i || argv.input
const output = argv.o || argv.output

tasks({ help, silent, watching, input, output })
