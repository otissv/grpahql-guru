'use strict'

const shell = require('shelljs')
const chalk = require('chalk')

module.exports = function eslint({ task, silent }) {
  return function({ root }) {
    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} starting ESLint
`)
      )
    }

    shell.config.silent = silent
    shell.exec(`eslint --quiet ${root} && echo 'Completed:' && date || true`)
    shell.config.silent = false

    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} esLint completed
      
`)
      )
    }
  }
}
