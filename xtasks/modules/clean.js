'use strict'

const shell = require('shelljs')
const chalk = require('chalk')

module.exports = function clean({ task, silent, dist }) {
  return function() {
    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} starting clean
`)
      )
    }

    shell.config.silent = silent
    shell.rm('-r', dist)
    shell.config.silent = false

    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} clean completed
      
`)
      )
    }
  }
}
