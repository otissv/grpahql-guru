const chalk = require('chalk')
const watch = require('watch')
const { buildBabelFile } = require('./babel-build')
const shell = require('shelljs')
const { ls, rm, error } = shell

module.exports = function watchFiles({ dist, src, packages }) {
  watch.watchTree(`${process.cwd()}/packages`, (file, curr, prev) => {
    let isTask

    const runTasks = file => {
      const split = file
        .substr(`${process.cwd()}/packages/`.length, file.length)
        .split('/')
      const task = split[0]
      let isTask =
        packages &&
        packages[task] &&
        !file.match('node_modules') &&
        !file.match('__') &&
        !file.match('package.json')

      if (isTask) {
        const _dist = `${process.cwd()}/${packages[task].dist}` || dist
        const _src = `${process.cwd()}/${packages[task].src}` || src

        // match('build');
        if (_dist && _src && !file.match(_dist)) {
          const distFile = `${_dist}${file.substr(_src.length, file.length)}`
          buildBabelFile({ task, dist: distFile, silent: false, src: file })
        }
      }

      if (isTask) {
        const date = new Date()
        process.stdout.write(chalk.green(`Watching ${process.cwd()}\n`))
        process.stdout.write(chalk.yellow(`${date}\n\n`))
      }
    }

    if (typeof file === 'object' && prev === null && curr === null) {
      // Finished walking the tree
    } else if (prev === null) {
      // file is a new
      runTasks(file)
    } else if (curr.nlink === 0) {
      // file was removed
    } else {
      // file was changed
      runTasks(file)
    }
  })
}
