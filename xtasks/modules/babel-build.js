'use strict'

const shell = require('shelljs')
const chalk = require('chalk')
const babel = require('babel-core')
const { buildBabelFile } = require('./babel-build')

exports.buildBabel = function buildBabel({ task, dist, silent, src }) {
  return function() {
    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} starting  babel build

`)
      )
    }

    shell.config.silent = silent
    shell.exec(`babel -d ${dist} ${src} -s --ignore node_modules,public`)
    shell.config.silent = false

    if (!silent) {
      process.stdout.write(
        chalk.yellow(`${task || ''} babel build completed

`)
      )
    }
  }
}

exports.buildBabelFile = function buildBabelFile({ task, dist, silent, src }) {
  shell.config.silent = silent

  if (!silent) {
    process.stdout.write(
      chalk.yellow(`${task || ''} starting  babel build for ${src}
    `)
    )
  }

  shell.exec(`babel ${src} --out-file ${dist}`)
  if (shell.error()) console.error(shell.error())
  // shell.config.silent = false;

  if (!silent) {
    process.stdout.write(
      chalk.yellow(`${task || ''} babel build completed for ${dist}
  
    `)
    )
  }
}
