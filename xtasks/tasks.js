#!/usr/bin/env node

/*
* guru scripts
*/
'use strict'

const shell = require('shelljs')
const path = require('path')
const argv = require('minimist')(process.argv)
const chalk = require('chalk')

// tasks
const { buildBabel, buildBabelFile } = require('./modules/babel-build')
const copy = require('./modules/copy')
const clean = require('./modules/clean')
const eslint = require('./modules/eslint')
const watch = require('./modules/watch')

const root = process.cwd()
const packageJs = require(root + '/package')
const packagesArg = argv.p || argv.packages
const watching = argv.w || argv.watch

const pipe = sequence => {
  return sequence.reduce((previous, current) => {
    current()
  }, null)
}

const tasks = (task, { dist, silent, src = false }) => {
  const input = `${root}/${src}`
  const output = `${root}/${dist}`

  pipe([
    clean({ task, dist: output, silent }),
    copy({ packageJs, task, dist, silent, src }),
    buildBabel({ task, dist, silent, src }),
  ])
}

const runPackageTasks = ({ jobs, packages }) => {
  process.stdout.write('Building packages...\n')

  Object.keys(jobs).map(job => {
    jobs[job]()
  })

  process.stdout.write('Build complete\n')

  if (watching) {
    const date = new Date()
    process.stdout.write(chalk.green(`Watching ${process.cwd()}\n`))
    process.stdout.write(chalk.yellow(`${date}\n\n`))

    watch({ packages })
  }
}

function lib() {
  const src = argv.s || argv.src || 'src'
  const dist = argv.d || argv.dist || 'dist'
  const help = argv.h || argv.help
  const quiet = argv.s || argv.silent

  const run = () => pipe([clean, eslint, copy, buildBabelFile])

  if (help) {
    process.stdout.write('help')
  } else {
    shell.config.silent = quiet || false
    const options = {
      dist,
      src,
      silent: argv.silent,
    }

    if (watching) {
      watch({ dist, src })
    } else {
      run()
    }
  }

  shell.config.silent = false
}

if (!packagesArg) {
  lib()
}

module.exports = {
  pipe,
  runPackageTasks,
  tasks,
}
