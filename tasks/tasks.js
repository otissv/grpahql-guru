'use strict'

const argv = require('minimist')(process.argv)

const fs = require('fs')
const { cleanFolders } = require('./clean-folders.tasks')
const { copyFolders } = require('./copy-folders.tasks')
const { buildBabel } = require('./build-babel.tasks')
const { watchFolders } = require('./watch-folders.tasks')

const silent = argv.s || argv.silent
const watching = argv.w || argv.watch
const src = `${process.cwd()}/src`
const dest = `${process.cwd()}/lib`

const pipe = sequence => {
  return sequence.reduce((previous, current) => current(), null)
}

if (watching) {
  pipe([
    cleanFolders({ dest, silent, src }),
    copyFolders({ dest, silent, src }),
    buildBabel({ dest, silent, src }),
  ])

  watchFolders({ dest, silent, src, watching })
} else {
  pipe([
    cleanFolders({ dest, silent, src }),
    copyFolders({ dest, silent, src }),
    buildBabel({ dest, silent, src }),
  ])
}

// TODO: add error log
