const shell = require('shelljs')
exports.buildBabel = function buildBabel({ dest, silent, src }) {
  return function() {
    shell.config.silent = silent
    shell.exec(`babel -d ${dest} ${src} -s --ignore node_modules,lib/public
`)
    shell.config.silent = false

    if (!silent) {
      process.stdout.write(`Babel ${src} -> ${dest}

  `)
    }
  }
}
