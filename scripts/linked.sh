#!/bin/bash

cd packages/guru-server/node_modules && \
rm -r guru-accounts guru-database-jsondb guru-database-mongodb;
ln -s ../../guru-accounts  guru-accounts; 
ln -s ../../guru-database-jsondb  guru-database-jsondb;
ln -s ../../guru-database-mongodb  guru-database-mongodb;
ln -s ../../guru-server-utils  guru-server-utils;