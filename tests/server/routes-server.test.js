const request = require('request')
const util = require('util')
const get = util.promisify(request.get)
const post = util.promisify(request.post)
const fs = require('fs')
const path = require('path')

test('Server has index routes', async () => {
  const actual = await get({
    url: 'http://localhost:9200',
  })

  expect(actual.statusCode).toBe(200)
})

test('Get - Empty graphql endpoint is bad request', async () => {
  const actual = await get({
    url: 'http://localhost:9200/graphql',
  })
  expect(actual.statusCode).toBe(400)
})

test('Post - Empty body is internal server error', async () => {
  const actual = await post({
    url: 'http://localhost:9200',
  })
  expect(actual.statusCode).toBe(404)
})
