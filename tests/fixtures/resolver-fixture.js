const { ObjectId } = require('mongodb')

export const dataIds = [
  {
    // 0. remove data
    id: '57509b5f350a10fb44e4c2b5',
  },
  {
    // 1. remove intercept arguments
    id: '57509b5f350a10fb44e4c2b7',
  },
  {
    // 2. after remove intercept results
    id: '5758078ba2bb7ddb1444096e',
  },
  {
    // 3. removeById returns data
    id: '5758078ba2bb7ddb1444096f',
  },
  {
    // 4. removeById returns data
    id: '5758078ba2bb7ddb14440970',
  },
  {
    // 5. before removeById intercept arguments
    id: '5758078bf5048b981206a0e2',
  },
  {
    // 6. after removeById intercept results
    id: '5758078bf5048b981206a0e3',
  },
  {
    // 7. after removeById intercept results
    id: '57509b5f350a10fb44e4c2b3',
  },
  {
    // 8. update data
    id: '5758078ba2bb7ddb1444096a',
  },
  {
    // 9. before update intercept arguments
    id: '5758078ba2bb7ddb14440979',
  },
  {
    // 10. before update intercept arguments
    id: '5758078ba2bb7ddb14440971',
  },
  {
    // 11. before update intercept arguments
    id: '5758078ba2bb7ddb14440972',
  },
  {
    // 12. before update intercept arguments
    id: '5758078ba2bb7ddb14440973',
  },
  {
    // 13. before update intercept arguments
    id: '5758078ba2bb7ddb14440974',
  },
  {
    // 14. before update intercept arguments
    id: '5758078ba2bb7ddb14440975',
  },
  {
    // 15. before update intercept arguments
    id: '5758078ba2bb7ddb14440976',
  },
  {
    // 16. before update intercept arguments
    id: '5758078ba2bb7ddb14440977',
  },
  {
    // 17. remove data
    id: '5758078ba2bb7ddb14440961',
  },
  {
    // 18. remove data
    id: '5758078ba2bb7ddb14440962',
  },
  {
    // 19. remove data
    id: '5758078ba2bb7ddb14440963',
  },
  {
    // 20. remove data
    id: '5758078ba2bb7ddb14440964',
  },
  {
    // 21. remove data
    id: '5758078ba2bb7ddb14440965',
  },
  {
    // 22. remove data
    id: '5758078ba2bb7ddb14440966',
  },
]

export const data = dataIds.map((item, index) => ({
  _id: ObjectId(item.id),
  age: index + 1,
  username: `user_${index + 1}`,
  email: `user_${index + 1}@email.com`,
  roles: ['ADMIN'],
}))
