const { data } = require('../fixtures/accounts-fixture')
const { connect, utils } = require('guru-database-mongodb')
const { queryObjectId } = utils
const { ObjectId } = require('mongodb')

let client
const collectionName = 'test_query_utils'

beforeAll(async () => {
  const options = {
    uri: 'mongodb://127.0.0.1:27017/test',
    opts: {
      server: {
        socketOptions: { keepAlive: 1 },
      },
    },
  }

  client = connect(options)

  const db = await client.then(db => db)

  await db
    .collections()
    .then(collections => !collections.map(c => c.s.name).includes(collectionName))
    .then(bool => !bool && db.collection(collectionName).drop())
    .then(() => db.createCollection(collectionName))
    .then(() => db.collection(collectionName).createIndex({ age: -1 }))
    .then(() => db.collection(collectionName).insertMany(data))
})

/*
* Mongodb queryObjectId tests
*/
test('Mongodb queryObjectId returns empty object if args is empty.', async () => {
  const actual = queryObjectId({})
  expect(actual).toEqual({})
})

test('Mongodb queryObjectId returns empty object if no args.', async () => {
  const actual = queryObjectId()
  expect(actual).toEqual({})
})

test('Mongodb queryObjectId returns args with id', async () => {
  const id = '54495ad94c934721ede76d90'
  const actual = queryObjectId({ id, hello: 'world' })

  expect(actual).toEqual({
    _id: ObjectId(id),
    hello: 'world',
  })
})

test('Mongodb queryObjectId returns args with no id', async () => {
  const actual = queryObjectId({ hello: 'world' })

  expect(actual).toEqual({
    hello: 'world',
  })
})

test('Mongodb queryObjectId returns args with array', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'
  const actual = queryObjectId({ $in: [{ id: id_0 }, { id: id_1 }] })

  expect(actual).toEqual({
    $in: [{ _id: ObjectId(id_0) }, { _id: ObjectId(id_1) }],
  })
})

test('Mongodb queryObjectId ignores args with that start an  withunder score', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'

  const actual = queryObjectId({ _id: { $in: [ObjectId(id_0), ObjectId(id_1)] } })

  expect(actual).toEqual({
    _id: { $in: [ObjectId(id_0), ObjectId(id_1)] },
  })
})

test('Mongodb queryObjectId retrurs $in args with ObjectID', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'

  const actual = queryObjectId({ id: { $in: [id_0, id_1] } })

  expect(actual).toEqual({
    _id: { $in: [ObjectId(id_0), ObjectId(id_1)] },
  })
})

test('Mongodb queryObjectId returns may ids', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'
  const actual = queryObjectId({ id: [id_0, id_1] })

  expect(actual).toEqual({ _id: { $in: [ObjectId(id_0), ObjectId(id_1)] } })
})
