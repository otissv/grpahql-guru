const path = require('path')
require('dotenv').config({
  path: path.resolve(__dirname, '../../packages/guru-server/.env'),
})

const { connect, MongoDBMutation } = require('guru-database-mongodb')
const { data } = require('../fixtures/resolver-fixture')
const ObjectId = require('bson-objectid')
const uniqueId = require('lodash/fp/uniqueId')
const create = require('guru-database-mongodb/lib/mutations/create-mongodb').default
const createMany = require('guru-database-mongodb/lib/mutations/createMany-mongodb').default
const drop = require('guru-database-mongodb/lib/mutations/drop-mongodb').default
const findAndModify = require('guru-database-mongodb/lib/mutations/findAndModify-mongodb').default
const findOneAndDelete = require('guru-database-mongodb/lib/mutations/findOneAndDelete-mongodb')
  .default
const findOneAndReplace = require('guru-database-mongodb/lib/mutations/findOneAndReplace-mongodb')
  .default
const findOneAndUpdate = require('guru-database-mongodb/lib/mutations/findOneAndUpdate-mongodb')
  .default
const nested = require('guru-database-mongodb/lib/mutations/nested-mongodb').default
const remove = require('guru-database-mongodb/lib/mutations/remove-mongodb').default
const removeById = require('guru-database-mongodb/lib/mutations/removeById-mongodb').default
const save = require('guru-database-mongodb/lib/mutations/save-mongodb').default
const saveMany = require('guru-database-mongodb/lib/mutations/saveMany-mongodb').default
const update = require('guru-database-mongodb/lib/mutations/update-mongodb').default
const updateMany = require('guru-database-mongodb/lib/mutations/updateMany-mongodb').default
const updateManyById = require('guru-database-mongodb/lib/mutations/updateManyById-mongodb').default

let context = {
  databases: { mongodb: null },
  options: {},
}

const COLLECTION_NAME = 'test_mutation_mongodb'
const DEFAULT_OPTIONS = { collection: COLLECTION_NAME }

beforeAll(async () => {
  try {
    const options = {
      uri: 'mongodb://127.0.0.1:27017/test',
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    }

    context.databases.mongodb = connect(options)
    const db = await context.databases.mongodb.then(db => db)

    await db
      .collections()
      .then(collections => !collections.map(c => c.s.name).includes(COLLECTION_NAME))
      .then(bool => !bool && db.collection(COLLECTION_NAME).drop())
      .then(() => db.createCollection(COLLECTION_NAME))
      .then(() => db.collection(COLLECTION_NAME).insertMany(data))
  } catch (error) {
    console.error(error)
  }
})

/*
* Initiate MongoDB Mutation tests
*/
test.skip('MongoDBMutation collection has default database name', async () => {
  const collection = new MongoDBMutation()

  expect(collection.state.dbName).toBe('mongodb')
})

test.skip('MongoDBMutation collection has custom database name', async () => {
  const collection = new MongoDBMutation({
    dbName: COLLECTION_NAME,
  })

  expect(collection.state.dbName).toBe(COLLECTION_NAME)
})

test.skip('MongoDBMutation collection has collection name', async () => {
  const collection = new MongoDBMutation({
    collection: COLLECTION_NAME,
  })

  expect(collection.state.collection).toBe(COLLECTION_NAME)
})

test.skip('MongoDBMutation collection has table name', async () => {
  const collection = new MongoDBMutation({
    table: COLLECTION_NAME,
  })

  expect(collection.state.table).toBe(COLLECTION_NAME)
})

/*
* MongoDBMutation create query tests
*/
test('MongoDBMutation create returns data', async () => {
  @create(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('create_')

  const query = {
    id,
    age: id.split('_')[1],
    username: id,
    email: `${id}@email.com`,
    roles: ['ADMIN'],
  }

  const result = await collection.create(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery create returns query error', async () => {
  @create(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.create(null, { id: '1' }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection create arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before create intercept arguments', async () => {
  @create({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, query: { id: `${args.query.id}_guru` } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('create_')

  const query = {
    id,
    age: id.split('_')[1],
    username: id,
    email: `${id}@email.com`,
    roles: ['ADMIN'],
  }
  const result = await collection.create(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: `${id}_guru`,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after create intercept results', async () => {
  @create({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          id: `${i.RESULTS.id}_guru`,
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('create_')

  const query = {
    id,
    age: id.split('_')[1],
    username: id,
    email: `${id}@email.com`,
    roles: ['ADMIN'],
  }
  const result = await collection.create(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: `${id}_guru`,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

/*
* MongoDBMutation createMany query tests
*/
test('MongoDBMutation createMany returns data', async () => {
  @createMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const query = Array.apply(null, { length: 2 }).reduce((p, c) => {
    const id = uniqueId('createMany_')
    return [
      ...p,
      {
        id,
        age: id.split('_')[1],
        username: id,
        email: `${id}@email.com`,
        roles: ['ADMIN'],
      },
    ]
  }, [])

  const result = await collection.createMany(null, { query }, context)

  expect(result.map(i => i.RESULTS.result)).toEqual(['ok', 'ok'])
})

test('MongoDBQuery createMany returns query error', async () => {
  @createMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.createMany(null, { id: '1' }, context)
  } catch (error) {
    ;[
      {
        RESULTS: {
          error: {
            message: 'Resolver Mutation Error: No data supplied.',
            type: 'RESOLVER_MUTATION_ERROR',
          },
          module: undefined,
          n: undefined,
          result: 'failed',
        },
      },
    ]

    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection createMany arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before createMany intercept arguments', async () => {
  @createMany({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [
        obj,
        {
          ...args,
          query: args.query.map(i => ({ ...i, id: `${i.id}_guru` })),
        },
        context,
        info,
      ]
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('createMany_')

  const query = [
    {
      id,
      age: id.split('_')[1],
      username: id,
      email: `${id}@email.com`,
      roles: ['ADMIN'],
    },
  ]

  const result = await collection.createMany(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: `${id}_guru`,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after createMany intercept results', async () => {
  @createMany({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          id: `${i.RESULTS.id}_guru`,
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('createMany_')

  const query = [
    {
      id,
      age: id.split('_')[1],
      username: id,
      email: `${id}@email.com`,
      roles: ['ADMIN'],
    },
  ]

  const result = await collection.createMany(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: `${id}_guru`,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

/*
* MongoDBMutation drop query tests
*/
test('MongoDBMutation drop returns data', async () => {
  const collectionName = 'test_mutation_drop'

  @create({ collection: collectionName })
  class CreateCollection {}
  const creatCollection = new CreateCollection()

  await creatCollection.create(null, { query: { data: 'test' } }, context)

  @drop({ collection: collectionName })
  class Collection {}
  const collection = new Collection()

  const result = await collection.drop(null, {}, context)

  expect(result).toEqual([{ RESULTS: { module: 'Collection', result: 'ok' } }])
})

/*
* MongoDBMutation remove query tests
*/
test('MongoDBMutation remove data', async () => {
  @remove(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()
  const query = { id: data[0]._id }
  const result = await collection.remove(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery remove returns query error', async () => {
  @remove(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.remove(null, { id: '1' }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection remove arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before remove intercept arguments', async () => {
  @remove({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, query: { id: `${data[1]._id}` } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('remove_')

  const query = {
    id,
    age: id.split('_')[1],
    username: id,
    email: `${id}@email.com`,
    roles: ['ADMIN'],
  }
  const result = await collection.remove(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after remove intercept results', async () => {
  @remove({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          result: 'success',
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const query = {
    id: data[2]._id,
  }
  const result = await collection.remove(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        id: undefined,
        error: undefined,
        module: 'Collection',
        n: 1,
        result: 'success',
      },
    },
  ])
})

/*
* MongoDBMutation removeById query tests
*/
test('MongoDBMutation removeById returns data', async () => {
  @removeById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const query = [{ id: data[3]._id }, { id: data[4]._id }]
  const result = await collection.removeById(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        id: undefined,
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery removeById returns query error', async () => {
  @removeById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.removeById(null, { id: '1' }, context)
  } catch (error) {
    ;[
      {
        RESULTS: {
          error: {
            message: 'Resolver Mutation Error: No data supplied.',
            type: 'RESOLVER_MUTATION_ERROR',
          },
          module: undefined,
          n: undefined,
          result: 'failed',
        },
      },
    ]

    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection removeById arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before removeById intercept arguments', async () => {
  @removeById({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => [
      obj,
      { ...args, query: [{ id: `${data[5]._id}` }] },
      context,
      info,
    ],
  })
  class Collection {}
  const collection = new Collection()

  const id = uniqueId('removeById_')

  const query = [{ id: uniqueId('removeById_') }]

  const result = await collection.removeById(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after removeById intercept results', async () => {
  @removeById({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          result: `${i.RESULTS.result}_guru`,
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const query = [{ id: data[6]._id }, { id: data[7]._id }]

  const result = await collection.removeById(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok_guru',
      },
    },
  ])
})

/*
* MongoDBMutation update query tests
*/
test.skip('MongoDBMutation update data', async () => {
  @update(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const args = {
    query: { id: data[8]._id },
    update: { username: 'otis' },
  }

  const result = await collection.update(null, args, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: '5758078ba2bb7ddb1444096a',
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery update returns query error', async () => {
  @update(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.update(null, { query: { id: '1' } }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message:
              'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters',
            type: 'RESOLVER_MUTATION_ERROR',
          },
          id: '1',
          module: 'Collection',
          n: 0,
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before update intercept arguments', async () => {
  @update({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, update: { username: 'updateIntercept' } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const query = {
    id: data[9]._id,
  }

  const result = await collection.update(null, { query }, context)
  expect(result.map(i => ({ RESULTS: { ...i.RESULTS, id: i.RESULTS.id.toString() } }))).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: '5758078ba2bb7ddb14440979',
        module: 'Collection',
        n: 1,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after update intercept results', async () => {
  @update({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          result: 'success',
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const query = {
    id: data[10]._id,
  }

  const result = await collection.update(null, { query }, context)

  expect(result.map(i => ({ RESULTS: { ...i.RESULTS, id: i.RESULTS.id.toString() } }))).toEqual([
    {
      RESULTS: {
        error: undefined,
        id: '5758078ba2bb7ddb14440971',
        module: 'Collection',
        n: 1,
        result: 'success',
      },
    },
  ])
})

/*
* MongoDBMutation updateMany query tests
*/
test('MongoDBMutation updateMany returns data', async () => {
  @updateMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const args = {
    query: { age: { $gt: 21 } },
    udpate: { username: 'updateMany' },
  }

  const result = await collection.updateMany(null, args, context)

  expect(result).toEqual([
    {
      RESULTS: {
        id: undefined,
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery updateMany returns query error', async () => {
  @updateMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.updateMany(null, { id: '1' }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection updateMany arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before updateMany intercept arguments', async () => {
  @updateMany({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => [
      obj,
      { ...args, query: { id: [data[13]._id, data[14]._id] } },
      context,
      info,
    ],
  })
  class Collection {}
  const collection = new Collection()
  const query = { id: [data[13]._id] }

  const result = await collection.updateMany(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after updateMany intercept results', async () => {
  @updateMany({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          result: `${i.RESULTS.result}_guru`,
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const query = { id: [data[15]._id, data[16]._id] }

  const result = await collection.updateMany(null, { query }, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok_guru',
      },
    },
  ])
})

/*
* MongoDBMutation updateManyById query tests
*/
test('MongoDBMutation updateManyById returns data', async () => {
  @updateManyById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const args = {
    query: { id: [data[17]._id, data[18]._id] },
    update: { username: 'updateManyById' },
  }

  const result = await collection.updateManyById(null, args, context)

  expect(result).toEqual([
    {
      RESULTS: {
        id: undefined,
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBQuery updateManyById returns query error', async () => {
  @updateManyById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const result = await collection.updateManyById(null, { id: '1' }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection arguments must be in an query array of one or more objects.',
            type: 'NO_QUERY_ARRAY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBMutation before updateManyById intercept arguments', async () => {
  @updateManyById({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => [
      obj,
      { ...args, query: { id: [data[19]._id, data[20]._id] } },
      context,
      info,
    ],
  })
  class Collection {}
  const collection = new Collection()
  const args = {
    query: { id: [data[19]._id] },
    update: { username: data[19]._id },
  }

  const result = await collection.updateManyById(null, args, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok',
      },
    },
  ])
})

test('MongoDBMutation after updateManyById intercept results', async () => {
  @updateManyById({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) =>
      data.map(i => ({
        RESULTS: {
          ...i.RESULTS,
          result: `${i.RESULTS.result}_guru`,
        },
      })),
  })
  class Collection {}
  const collection = new Collection()

  const args = {
    query: { id: [data[21]._id, data[22]._id] },
    update: { username: 'updateManyById intercept ' },
  }

  const result = await collection.updateManyById(null, args, context)

  expect(result).toEqual([
    {
      RESULTS: {
        error: undefined,
        module: 'Collection',
        n: 2,
        result: 'ok_guru',
      },
    },
  ])
})
