const { connect } = require('guru-database-mongodb')

test.skip('Can connect to mongodb instance', async () => {
  const options = {
    uri: 'mongodb://127.0.0.1:27017/test',
    opts: {
      server: {
        socketOptions: { keepAlive: 1 },
      },
    },
  }

  const connection = await connect(options)

  expect(connection.s.databaseName).toBe('test')
})
