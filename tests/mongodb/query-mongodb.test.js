import { MongoDBMutation } from '../../packages/guru-database-mongodb/lib/index-mongodb'

const { dataIds } = require('../fixtures/accounts-fixture')
const { connect, connection, MongoDBQuery } = require('guru-database-mongodb')
const aggregate = require('guru-database-mongodb/lib/queries/aggregate-mongodb').default
const count = require('guru-database-mongodb/lib/queries/count-mongodb').default
const findById = require('guru-database-mongodb/lib/queries/findById-mongodb').default
const findManyById = require('guru-database-mongodb/lib/queries/findManyById-mongodb').default
const findMany = require('guru-database-mongodb/lib/queries/findMany-mongodb').default
const findOne = require('guru-database-mongodb/lib/queries/findOne-mongodb').default
const resolve = require('guru-database-mongodb/lib/queries/resolve-mongodb').default
const distinct = require('guru-database-mongodb/lib/queries/distinct-mongodb').default

let context = {
  databases: { mongodb: null },
  options: {},
}

const COLLECTION_NAME = 'test_query_mongodb'
const DEFAULT_OPTIONS = { collection: COLLECTION_NAME }
let collection

const data = dataIds

beforeAll(async () => {
  try {
    const options = {
      uri: 'mongodb://127.0.0.1:27017/test',
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    }

    context.databases.mongodb = connect(options)
    const db = await context.databases.mongodb.then(db => db)

    await db
      .collections()
      .then(collections => !collections.map(c => c.s.name).includes(COLLECTION_NAME))
      .then(bool => !bool && db.collection(COLLECTION_NAME).drop())
      .then(() => db.createCollection(COLLECTION_NAME))
      .then(() => db.collection(COLLECTION_NAME).insertMany(data))
  } catch (error) {
    console.error(error)
  }
})

/*
* Initiate MongoDBQuery  query tests
*/
test.skip('MongoDBQuery resolver has default database name', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  expect(collection.dbName).toBe('mongodb')
})

test.skip('MongoDBQuery resolver has custom database name', async () => {
  @MongoDBQuery({
    collection: COLLECTION_NAME,
    dbName: 'mydb',
  })
  class Collection {}
  const collection = new Collection()

  expect(collection.dbName).toBe('mydb')
})

test.skip('MongoDBQuery resolver has collection name', async () => {
  const collection = new MongoDBQuery(DEFAULT_OPTIONS)

  expect(collection.state.collection).toBe(COLLECTION_NAME)
})

test.skip('MongoDBQuery resolver has table name', async () => {
  const collection = new MongoDBQuery({
    table: COLLECTION_NAME,
  })

  expect(collection.state.table).toBe(COLLECTION_NAME)
})

/*
* MongoDBQuery count query tests
*/
test('MongoDBQuery count returns data', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Create {}

  const collection = new Create()
  const result = await collection.count(null, {}, context)

  expect(result.COUNT.n).toBe(6)
})

test('MongoDBQuery count returns data', async () => {
  @count(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const result = await collection.count(null, {}, context)
  expect(result.COUNT.n).toBe(6)
})

test.skip('MongoDBQuery before count intercept arguments', async () => {
  @count({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, options: { cursor: { limit: 3 } } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const result = await collection.count(null, {}, context)
  expect(result.COUNT.n).toBe(3)
})

test('MongoDBQuery after count intercept results', async () => {
  @count({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) => {
      return {
        COUNT: {
          n: data.COUNT.n + 1,
        },
      }
    },
  })
  class Collection {}
  const collection = new Collection()

  const result = await collection.count(null, {}, context)

  expect(result.COUNT.n).toBe(7)
})

/*
* MongoDBQuery findById query tests
*/
test('MongoDBQuery findById returns args with query', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b7'
  const result = await collection.findById(null, { query: { id } }, context)

  expect(result.id.toString()).toBe(id)
})

test('MongoDBQuery findById returns query error', async () => {
  @findById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const id = '57509b5f350a10fb44e4c2b7'
    await collection.findById(null, { id }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection findById arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBQuery before findById intercepts arguments', async () => {
  @findById({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, query: { id: '57509b5f350a10fb44e4c2b7' } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b7'
  const result = await collection.findById(null, { query: { id } }, context)

  expect(result.id.toString()).toBe('57509b5f350a10fb44e4c2b7')
})

test('MongoDBQuery after findById intercepts results', async () => {
  @findById({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) => {
      return { ...data, active: true }
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b7'
  const result = await collection.findById(null, { query: { id } }, context)

  expect(result.active).toEqual(true)
})

/*
* MongoDBQuery findManyById query tests
*/
test('MongoDBQuery findManyById returns args with query', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const query = { id: ['57509b5f350a10fb44e4c2b5', '57509b5f350a10fb44e4c2b7'] }
  const result = await collection.findManyById(null, { query }, context)

  expect(result.length).toBe(2)
})

test('MongoDBQuery findManyById returns query error', async () => {
  @findManyById(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const id = {}
    await collection.findManyById(null, { id }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection arguments must be in an query array of one or more objects.',
            type: 'NO_QUERY_ARRAY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBQuery before findManyById intercepts arguments', async () => {
  @findManyById({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, query: { id: ['57509b5f350a10fb44e4c2b7'] } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const query = { id: ['57509b5f350a10fb44e4c2b5'] }
  const result = await collection.findManyById(null, { query }, context)

  expect(result[0].id.toString()).toBe('57509b5f350a10fb44e4c2b7')
})

test('MongoDBQuery after findManyById intercepts results', async () => {
  @findManyById({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) => {
      return [...data, { active: true }]
    },
  })
  class Collection {}
  const collection = new Collection()

  const query = { id: ['57509b5f350a10fb44e4c2b5', '57509b5f350a10fb44e4c2b7'] } 
  const result = await collection.findManyById(null, { query }, context)

  expect(result[2].active).toEqual(true)
})

/*
* MongoDBQuery findMany query tests
*/
test('MongoDBQuery findMany returns data', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const result = await collection.findMany(null, {}, context)
  expect(result.length).toBe(6)
})

test('MongoDBQuery findMany returns data', async () => {
  @findMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const result = await collection.findMany(null, {}, context)
  expect(result.length).toBe(6)
})

test.skip('MongoDBQuery findMany returns args with query', async () => {
  @findMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b5'
  const result = await collection.findMany(null, { query: { id } }, context)

  expect(result.length).toBe(1)
  expect(result[0].id.toString()).toBe(id)
})

test('MongoDBQuery findMany returns query error', async () => {
  @findMany(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const id = '57509b5f350a10fb44e4c2b5'
    await collection.findMany(null, { id }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection findMany arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBQuery before findMany intercepts arguments', async () => {
  @findMany({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, options: { cursor: { limit: 3 } } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const result = await collection.findMany(null, {}, context)
  expect(result.length).toBe(3)
})

test('MongoDBQuery after findMany intercepts results', async () => {
  @findMany({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) => {
      const result = data.splice(1, data.length - 1)
      return result
    },
  })
  class Collection {}
  const collection = new Collection()

  const result = await collection.findMany(null, {}, context)
  expect(result.length).toBe(5)
})

/*
* MongoDBQuery findOne query tests
*/
test('MongoDBQuery findOne returns args with query', async () => {
  @MongoDBQuery(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b5'
  const result = await collection.findOne(null, { query: { id } }, context)

  expect(result.id.toString()).toBe(id)
})

test('MongoDBQuery findOne returns query error', async () => {
  @findOne(DEFAULT_OPTIONS)
  class Collection {}
  const collection = new Collection()

  try {
    const id = '57509b5f350a10fb44e4c2b5'
    await collection.findOne(null, { id }, context)
  } catch (error) {
    expect(error).toEqual([
      {
        RESULTS: {
          error: {
            message: 'Collection findOne arguments must be in an query object.',
            type: 'NO_QUERY',
          },
          module: 'Collection',
          result: 'failed',
        },
      },
    ])
  }
})

test('MongoDBQuery before findOne intercepts arguments', async () => {
  @findOne({
    ...DEFAULT_OPTIONS,
    before: async (obj, args, context, info) => {
      return [obj, { ...args, query: { id: '57509b5f350a10fb44e4c2b7' } }, context, info]
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b5'
  const result = await collection.findOne(null, { query: { id } }, context)

  expect(result.id.toString()).toBe('57509b5f350a10fb44e4c2b7')
})

test('MongoDBQuery after findOne intercepts results', async () => {
  @findOne({
    ...DEFAULT_OPTIONS,
    after: async ({ data, params }) => {
      return { ...data, active: true }
    },
  })
  class Collection {}
  const collection = new Collection()

  const id = '57509b5f350a10fb44e4c2b5'
  const result = await collection.findOne(null, { query: { id } }, context)

  expect(result.active).toEqual(true)
})
