exports.userFragment = ` 
  fragment userFields on User {
    id
    roles {
      id
      description
      role
    }
    lastLogin
    lastLoggedInWith
    password
    username
    token
    auth0Id
    azuerId
    bitbucketId
    auth0apiId
    facebookId
    githubId
    gitlabId
    googleId
    instagramId
    jwtId
    windowsliveId
    twitterId
    meta {
      createdAt
      createdBy
      updatedAt
      updatedBy
    }
  }
`
